(function($){
    $(function(){

        $('.sidenav').sidenav();
        $('.modal').modal();
        $('.tabs').tabs();
        $('.tooltipped').tooltip();
        $('select').formSelect();

        $("#input-register-password").on("focusout", function (e) {
            if ($(this).val() !== $("#input-register-password-confirm").val()) {
                $("#input-register-password-confirm").removeClass("valid").addClass("invalid");
            } else {
                $("#input-register-password-confirm").removeClass("invalid").addClass("valid");
            }
        });

        $("#input-register-password-confirm").on("keyup", function (e) {
            if ($("#input-register-password").val() !== $(this).val()) {
                $(this).removeClass("valid").addClass("invalid");
            } else {
                $(this).removeClass("invalid").addClass("valid");
            }
        });


        $( "#search-button" ).click(function() {

            $('#filter-search').css("display","block");
            setTimeout(function(){
                $('#filter-search').addClass("scale-in");
            }, 0);
            $.getJSON( "http://localhost:8080/api/sport", function( sports ) {
                $('#input-sports').empty();
                $.each( sports, function( key, sport ) {
                    $('#input-sports').append(
                        createCustomInput("radio","sports",sport.sport_name, sport.sport_id)
                    );
                });
                $("input[name='sports']").change(function() {
                    sport_id = $("input[type='radio'][name='sports']:checked").val();
                    console.log(sport_id);
                    $.getJSON( "http://localhost:8080/api/category/sport/"+sport_id, function( categories ) {
                        $('#input-categories').empty();
                        $.each( categories, function( key, category ) {
                            $('#input-categories').append(
                                createCustomInput("checkbox","categories[]",category.category_name, category.category_id)
                            );
                        });
                    });
                });
            })
        });

        fitty('.autofill-city', {
            minSize: 6,
            maxSize: 38,
            multiLine: false
        });
        fitty('.autofill-category', {
            minSize: 6,
            maxSize: 14,
            multiLine: false
        });
        fitty('.autofill-organizer', {
            minSize: 6,
            maxSize: 12,
            multiLine: false
        });
        fitty('.autofill-time', {
            minSize: 6,
            maxSize: 32,
            multiLine: false
        });

    }); // end of document ready
})(jQuery); // end of jQuery name space

function createCustomInput(type,name,id,value){

    var paragraph = document.createElement('p');

    var label = document.createElement('label');
    var span = document.createElement('span');
    span.setAttribute('class', "flow-text");
    var spanText = document.createTextNode(id);
    span.appendChild(spanText);

    var input = document.createElement('input');
    input.setAttribute('type', type);
    input.setAttribute('name', name);
    input.setAttribute('id', id);
    input.setAttribute('value', value);

    label.appendChild(input);
    label.appendChild(span);

    paragraph.appendChild(label);

    return paragraph;
}

document.addEventListener('DOMContentLoaded', function() {

    var selectedProvince = null;
    var selectedCity = null;

    var inputProvince = M.Autocomplete.init(document.querySelector('input#province-input'), {
        onAutocomplete: function(txt) {
            console.log(txt);
            $.getJSON( "http://localhost:8080/api/province/"+txt, function( provinces ){
                selectedProvince = provinces[0];
                $('input[name="province"]').val(selectedProvince.id);
            });
        },
        limit: 5,
        minLength: 1
    });

    $('#province-input').keyup(function () {
        selectedProvince = null;
        inputProvince = M.Autocomplete.getInstance(this);
        var search = slugify(this.value);
        var items = {};
        $.getJSON( "http://localhost:8080/api/province/"+search, function( provinces ) {
            $.each( provinces, function( key, province ) {
                items[province.provincia] = null;
            });
        });
        inputProvince.updateData(items);
    });

    var inputCity = M.Autocomplete.init(document.querySelector('input#city-input'), {
        onAutocomplete: function(txt) {
            console.log(txt);
            var search = slugify(txt);
            $.getJSON( "http://localhost:8080/api/city/"+search, function( cities ){
                selectedCity = cities[0];
                $('input[name="city"]').val(selectedCity.id);
                $('input[name="province"]').val(selectedCity.provincia_id);
                console.log(selectedCity.provincia_id);
                $.getJSON( "http://localhost:8080/api/province/"+selectedCity.provincia_id, function( provinces ){
                    selectedProvince = provinces;
                    console.log(selectedProvince);
                    document.querySelector('input#province-input').value = selectedProvince.provincia;
                    $('label[for="province-input"]').addClass("active");
                });
            });
        },
        limit: 5,
        minLength: 1
    });

    $('#city-input').keyup(function () {
        inputCity = M.Autocomplete.getInstance(this);

        if(selectedProvince){
            var url = "http://localhost:8080/api/city/province/"+selectedProvince.id;
        } else {
            var search = slugify(this.value);
            var url = "http://localhost:8080/api/city/"+search;
        }

        var items = {};
        $.getJSON( url, function( cities ) {
            $.each( cities, function( key, city ) {
                items[city.municipio] = null;
            });
            console.log(items);
            console.log(cities);
            inputCity.updateData(items);
        });
    });



});

function slugify(string){
    return string.toLowerCase().replace(/ /g,"-").replace(/[^A-Za-z0-9-]+/g,'');
}

