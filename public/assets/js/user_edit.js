function previewFile() {
    var preview = document.getElementById(this.imageId);
    var file    = this.files[0];
    var reader  = new FileReader();

    reader.onloadend = function () {
        preview.src = reader.result;
    }

    if (file) {
        reader.readAsDataURL(file);
    } else {
        preview.src = "";
    }
}

document.addEventListener('DOMContentLoaded', function() {
    let inputUserAvatar = document.querySelector("input[type=file][name='avatar']");
    let inputUserBackground = document.querySelector("input[type=file][name='background']");

    inputUserAvatar.addEventListener("change", previewFile);
    inputUserAvatar.imageId = "avatar";
    inputUserBackground.addEventListener("change", previewFile);
    inputUserBackground.imageId = "background";


    let inputDatePicker = document.getElementById("birthday");
    $('.datepicker').datepicker({
        defaultDate: new Date(inputDatePicker.value),
        setDefaultDate: true,
        maxDate: new Date(),
        firstDay: 1,
        format: "dd mmm, yyyy",
        onClose: function(){
            inputDatePicker.setAttribute("value", moment(this.date).format("YYYY-MM-DD"));
        },
        i18n: {
            months: [
                'Enero',
                'Febrero',
                'Marzo',
                'Abril',
                'Mayo',
                'Junio',
                'Julio',
                'Agosto',
                'Septiembre',
                'Octubre',
                'Noviembre',
                'Diciembre'
            ],
            monthsShort: [
                'Ene',
                'Feb',
                'Mar',
                'Abr',
                'May',
                'Jun',
                'Jul',
                'Ago',
                'Sep',
                'Oct',
                'Nov',
                'Dic'
            ],
            weekdays: [
                'Domingo',
                'Lunes',
                'Martes',
                'Miércoles',
                'Jueves',
                'Viernes',
                'Sábado'
            ],
            weekdaysShort: [
                'dom',
                'lun',
                'mar',
                'mie',
                'jue',
                'vie',
                'sab'
            ],
            weekdaysAbbrev: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
        }
    });


});






// November 22nd 2018, 9:50:15 am

