# code-igniter-start
Docker container with
 - Apache
 - PHP:7-2
 - MySql:8
 - PhpMyAdmin
 - CodeIgniter:3-1-9

## Before start up the container
### 1. Configure the environment file
First of all, edit the enviroment file `.env` and give some values for:
* The **name** for the new database.
* The **username** and a **password** for database authentication.
* The **root password**.
* The **IP** or the **hostname** of the host machine in order to enable remote Xdebug connections.

### 2. Import database structure
In order to automatically import any dump SQL file, you have to place them inside the folder: `db/dump`

### 3. Start up the container
Use docker-compose as an orchestrator. To run these container:

```
docker-compose up -d
```

## Start with your new application
###· Edit database.php file
Just edit the file `application/config/database.php` and change the parameters values
with the new ones you used in the environment file `.env`

In the file `database.php` **don't change** the value of the parameter `database = db`

> It is not necessary to change it, but if you want to do it,
you should know that the parameter must have the same value used by the mysql service
in the file `docker-compose.yml`, in this case is `db`

###· Place your static resources
Any static resources like **images**, **css** or **javascript** must be placed inside the `public` folder.
> If you want to make public and accessible via browser the documentation,
simply move the directory `user_guide` to `public` folder.

###· And finally, start writing your code
That's all, you can view:
* Your application: [http://localhost:8080](http://localhost:8080)
* PhpMyAdmin login page: [http://localhost:8000](http://localhost:8000)


## More info about the container
### Web Service
As you can see in the `Dockerfile` this container is based on `php:7.2-apache`
and some apache modules will be enabled:
* [`rewrite`](https://httpd.apache.org/docs/current/mod/mod_rewrite.html) apache module
* [`mysqli`](http://php.net/manual/en/book.mysqli.php) library for manage mysql databases
* [`exif`](http://php.net/manual/en/book.exif.php) library for image manipulation
* [`pdo_mysql`](http://php.net/manual/en/ref.pdo-mysql.php) PDO driver for mysql databases
* [`xdebug`](https://xdebug.org/docs/) allows Xdebug remote connections. 

#### Security
*Source: CodeIgniter Documentation*
> For the best security, both the system and any application folders should be placed above web root
so that they are not directly accessible via a browser

To accomplish with that, new directory has been created: `public`.
 
As you can see in the file `docker-compose.yml`, each directory `code-igniter` and `public` will hang
on different volumes:
* The `code-igniter` folder is mounted under `/opt/app`
* The `public` folder is mounted under `/var/www/html/`

The **public** folder will be the web **DocumentRoot**, 
the file `index.php` has been moved from the `code-igniter` folder to `public` folder. 

Also the file `index.php` has been modified in order to change default values with these new parameter values:
 - `$system_path`= `'/opt/app/system'`;
 - `$application_folder` = `'/opt/app/application'`;
 - `$view_folder` = `'/opt/app/application/views'`;

#### Rewrite
As you can see in the `Dockerfile`, the **rewrite** module has been enabled. Now **apache** can work with URL rewrite rules.

A basic basic rule to hide index.php from the URL has been created in the file `public/.htacces`.
With this rule working on the web server, code-igniter can be configured to remove the index.php from the generated URLs.

To do that, in the file `application/config/config.php` has been modified the next parameter:
* From `$config['index_page'] = 'index.php';`
* To `$config['index_page'] = '';`

### Database service
#### MySQL

The first time the container is executed, a volume called `db-data` is created.
This volume is used to persist the data in the database after shutting down the container.

Also, the first time starting up the container, the SQL files inside the folder `db/dump` will be imported.
The next times you run the container, this SQL files will not be imported again.

If the volume is manually removed through the docker command line, the stored data will be lost. 
In that case, during the start up of the container the SQL files inside the folder `db/dump`
will be imported into a new empty database.

#### PhpMyAdmin
After starting up the container, you can manage your database via web with phpmyadmin.
Just visit the login page at port 8000: [http://localhost:8000](http://localhost:8000)