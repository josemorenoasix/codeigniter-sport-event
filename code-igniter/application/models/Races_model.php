<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 * @property  CI_Loader load
 * @property  CI_DB db
 * @property  Sports_model sports_model
 */
class Races_model extends MY_Model
{

	public function read($id = FALSE)
	{
		if ($id === FALSE)
		{
			return 0;
		}

		return $this->db
			->get_where('races', array('race_id' => $id),1)
			->custom_result_object('races_model');
	}


	public function exists_player($raceId, $userId) {

		$raceId = intval($raceId);
		$userId = intval($userId);

		$ok = $this->db->query(
			"SELECT COUNT(player_id) as registered FROM players WHERE user_id = ? AND race_id = ?",
			array($userId, $raceId)
		)->row_array();
		if ($ok["registered"] != 0) {
			return true;
		}
		return false;
	}

	public function is_valid_date_inscription($raceId){
		$race = $this->read($raceId);
		$now = time();
		if($now > strtotime($race["inscription_finish_date"])){
			throw new Error("Inscripciones cerradas");
		} else if ($now < strtotime($race["inscription_start_date"])) {
			throw new Error("Proxima apertura. Revisa la fecha de apertura");
		} else {
			return true;
		}
	}


	public function addPlayer($raceId, $userId) {

		$raceId = intval($raceId);
		$userId = intval($userId);

		// Ensure the user was not joined to the race previously

		if($this->_exists_player($raceId, $userId)){
			return "Already registered";
		}

		// Ensure the race inscription start and finish date is valid
		$now =  date('Y-m-d H:i:s');
		$race = $this->db->query(
			"SELECT * FROM races WHERE race_id = ? AND inscription_start_date < ? AND inscription_finish_date > ?",
			array($raceId, $now, $now)
		)->row_array();

		if ($race != null) {

			$player["race_id"] = $raceId;
			$player["user_id"] = $userId;

			// Get a valid number for the player
			$row = $this->db->query(
				"SELECT max(number) as last_number FROM players WHERE race_id = ? ",
				array($raceId)
			)->row_array();

			if ($row['last_number'] === null){
				$player["number"] = 1;
			} else {
				$player["number"] = $row['last_number'] + 1;
			}

			if(
				$this->db->query(
				"INSERT INTO players(race_id, user_id, number) VALUES (?,?,?)",
				array($player["race_id"],$player["user_id"], $player["number"])
				)
			){
				$player["player_id"] = $this->db->insert_id();
			};

			$join_data["race"] = $race;
			$join_data["player"] = $player;

			return $join_data;

		} else {
			return "Subscription unavailable";
		}

	}

/*
 *
$this->db->select('*,
				(SELECT COUNT(players.player_id) FROM players WHERE players.race_id = races.race_id) as num_players,
				DATEDIFF(NOW(), date) as date_counter,
				DATEDIFF(NOW(), inscription_start_date) as inscription_start_date_counter,
				DATEDIFF(NOW(), inscription_finish_date) as inscription_finish_date_counter');
			$this->db->from('races');
			$this->db->join('categories','races.category_id = categories.category_id','inner');
			$this->db->join('sports','categories.sport_id = sports.sport_id','inner');
			$this->db->join('municipios','races.city = municipios.id','inner');
			$this->db->order_by('date',"DESC");
			$this->db->limit($limit,$custom_offset);
			$this->db->get()->result_array();


 *
 */

	public function pagination($offset = 1, $limit = 4, $search = null, $user_id = null)
	{
		$custom_offset = ($offset-1) * $limit;

		$select = "*, ";
		if ($user_id){
			$select .= <<<SQL
(SELECT number FROM players WHERE players.race_id = races.race_id AND players.user_id = $user_id) as player_number, 
SQL;
		}

		$select .= <<<SQL
(SELECT COUNT(players.player_id) FROM players WHERE players.race_id = races.race_id) as num_players,
DATEDIFF(NOW(), date) as date_counter,
DATEDIFF(NOW(), inscription_start_date)  as inscription_start_date_counter,
DATEDIFF(NOW(), inscription_finish_date) as inscription_finish_date_counter
SQL;
		$this->db->select($select);
		$this->db->from('races');
		$this->db->join('categories','races.category_id = categories.category_id','inner');
		$this->db->join('sports','categories.sport_id = sports.sport_id','inner');
		$this->db->join('municipios','races.city = municipios.id','inner');

		if(isset($search["province"]) && !is_null($search["province"])) {
			$this->db->join('provincias', 'municipios.provincia_id = provincias.id AND provincias.id =' . $search["province"], 'inner');
		} else {
			$this->db->join('provincias', 'municipios.provincia_id = provincias.id', 'inner');
		}
		if(isset($search["opened_inscription"]) && !is_null($search["opened_inscription"])){
			$where = "NOW() BETWEEN inscription_start_date and inscription_finish_date";
			$this->db->where($where);
		}
		if(isset($search["categories"]) && !is_null($search["categories"])){
			$this->db->where_in('races.category_id', $search["categories"]);
		}
		if(isset($search["city"]) && !is_null($search["city"])){
			$this->db->where('races.city', $search["city"]);
		}
		$this->db->order_by('date',"DESC");
		$this->db->limit($limit,$custom_offset);

		return $this->db->get()->result_array();
	}

	public function search($category,$province,$city,$open_inscription)
	{
		 $this->db->select('*,
(SELECT COUNT(players.player_id) FROM players WHERE players.race_id = races.race_id) as num_players,
DATEDIFF(NOW(), date) as date_counter,
DATEDIFF(NOW(), inscription_start_date)  as inscription_start_date_counter,
DATEDIFF(NOW(), inscription_finish_date) as inscription_finish_date_counter');
		$this->db->from('races');
		$this->db->join('categories','races.category_id = categories.category_id','inner');
		$this->db->join('sports','categories.sport_id = sports.sport_id','inner');
		$this->db->join('municipios','races.city = municipios.id','inner');
		if(!is_null($province)) {
			$this->db->join('provincias', 'municipios.provincia_id = provincias.id AND provincias.id =' . $province, 'inner');
		} else {
			$this->db->join('provincias', 'municipios.provincia_id = provincias.id', 'inner');
		}
		if(!is_null($open_inscription)){
			$where = "NOW() BETWEEN inscription_start_date and inscription_finish_date";
			$this->db->where($where);
		}
		if(!is_null($category)){
			$this->db->where_in('races.category_id', $category);
		}
		if(!is_null($city)){
			$this->db->where_in('races.city', $city);
		}
		$this->db->order_by('date',"DESC");

		return $this->db->get()->result_array();
	}

	public function count($search)
	{
		if(is_null($search)){

			return $this->db->count_all('races');

		} else {
			$this->db->select('COUNT(races.race_id) as num_results');
			$this->db->from('races');
			if(isset($search["province"]) && !is_null($search["province"])) {
				$where = "city in (SELECT id FROM municipios where provincia_id = ".$search["province"].")";
				$this->db->where($where);
			}
			if(isset($search["opened_inscription"]) && !is_null($search["opened_inscription"])){
				$where = "NOW() BETWEEN inscription_start_date and inscription_finish_date";
				$this->db->where($where);
			}
			if(isset($search["categories"]) && !is_null($search["categories"])){
				$this->db->where_in('races.category_id', $search["categories"]);
			}
			if(isset($search["city"]) && !is_null($search["city"])){
				$this->db->where('races.city', $search["city"]);
			}
			$this->db->order_by('date',"DESC");
			$res = $this->db->get()->row_array();
			return $res["num_results"];

		}


	}


}
