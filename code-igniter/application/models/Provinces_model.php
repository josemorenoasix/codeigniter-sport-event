<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/** *
 * @property  CI_DB db
 */
class Provinces_model extends MY_Model
{

	public function read($param = null)
	{
		return $this->db->get_where('provincias', array('id' => $param),"")->row();
	}

	public function search($param = null)
	{
		if(is_null($param)){
			return $this->db->get('provincias')->result();
		}

		return $this->db
			->select('*')
			->from('provincias')
			->like('provincia', $param, 'after')
			->limit(10)
			->get()->result();
	}
	
/*
	function _remsoveAccents($string) {
		return strtolower(trim(preg_replace('~[^0-9a-z]+~i', '-', preg_replace('~&([a-z]{1,2})(acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($string, ENT_QUOTES, 'UTF-8'))), ' '));
	}
*/

}
