<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property  CI_Loader load
 * @property  CI_DB db
 */
class News_model extends MY_Model
{

	/**
	 * @var int
	 */
	public $id;

	/**
	 * @var int
	 */
	public $autor;

	/**
	 * @var string
	 */
	public $title;

	/**
	 * @var string
	 */
	public $description;

	/**
	 * @var string
	 */
	public $body;

	/**
	 * @var string
	 */
	public $image;

	/**
	 * @var DateTime
	 */
	public $created_at;

	/**
	 * @var DateTime
	 */
	public $modified_at;


	public function getAutor($id) {
		return;
	}

	public function read($id = FALSE)
	{
		if ($id === FALSE)
		{
			return 0;
		}

		return $this->db->get_where('news', array('new_id' => $id))->row_array();
	}

	public function pagination($offset = 1, $limit = 4)
	{
		$offset--;
		$this->db->order_by('modified_at',"DESC");
		return $this->db->get('news', $limit, $offset*$limit)->result_array();
	}

	public function count()
	{
		return $this->db->count_all('news');
	}

}
