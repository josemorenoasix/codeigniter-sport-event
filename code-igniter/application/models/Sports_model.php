<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * * @property  CI_DB db
 */
class Sports_model extends MY_Model
{

	public function read($id = null)
	{
		if ($id === null){
			return $this->db->get('sports')->result();
		}
		return $this->db->get_where('sports', array('sport_id' => $id),"")->row();
	}


}
