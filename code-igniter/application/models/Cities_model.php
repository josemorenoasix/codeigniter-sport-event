<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/** *
 * @property  CI_DB db
 */
class Cities_model extends MY_Model
{

	public function read($param = null)
	{
		return $this->db->get_where('municipios', array('id' => $param),"")->row();
	}

	public function search($param = null)
	{

		if(is_null($param)){
			return $this->db
				->select('*')
				->from('municipios')
				->limit(5)
				->get()->result();
		}

		return $this->db
			->select('*')
			->from('municipios')
			->like('municipio', $param, 'both')
			->or_like('slug', $param, 'both')
			->limit(50)
			->order_by('LENGTH(municipio)','ASC')
			->get()->result();
	}

	public function readByProvince($id)
	{
		return $this->db
			->select('*')
			->from('municipios')
			->where('provincia_id', $id)
			->get()->result();
	}

/*
	function _remsoveAccents($string) {
		return strtolower(trim(preg_replace('~[^0-9a-z]+~i', '-', preg_replace('~&([a-z]{1,2})(acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($string, ENT_QUOTES, 'UTF-8'))), ' '));
	}
*/

}
