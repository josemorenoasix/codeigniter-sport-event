<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property  CI_Loader load
 * @property  CI_Email email
 * #
 */
class Mail_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->library('email');
	}

	public function sendMail($from_email, $from_name, $to, $subject, $message, $attach_file = null){

		$this->email->clear(TRUE);
		$this->email->from($from_email, $from_name);
		$this->email->to($to);
		$this->email->subject($subject);
		$this->email->message($message);
		if ($attach_file){
			$this->email->attach($attach_file);
		}

		if(!$this->email->send()){
			echo "Error:". $this->email->print_debugger();
		};

		$message = "";

	}

	public function read($id = FALSE)
	{
		if ($id === FALSE)
		{
			return $this->pagination(1);
		}

		return $this->db->get_where('news', array('new_id' => $id))->row_array();
	}

	public function pagination($offset = 1, $limit = 4)
	{
		$offset--;
		$this->db->order_by('modified_at',"DESC");

		return $this->db->get('news', $limit, $offset)->result_array();
	}

	public function count()
	{
		return $this->db->count_all('news');
	}

}
