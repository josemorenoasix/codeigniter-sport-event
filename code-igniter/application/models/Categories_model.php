<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 * @property  CI_Loader load
 * @property  CI_DB db
 */
class Categories_model extends MY_Model
{

	public function read($id = FALSE)
	{
		if ($id === null){
			return $this->db->get('categories')->result();
		}
		return $this->db->get_where('categories', array('sport_id' => $id),"")->row();
	}

	public function readBySport($id)
	{
		if ($id === null){
			return 0;
		}

		return $this->db
			->select('*')
			->from('categories')
			->where('categories.sport_id', $id)
			->get()->result();
	}



}
