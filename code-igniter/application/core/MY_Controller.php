<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'third_party/community_auth/core/Auth_Controller.php';

/**
 * @property  User_model user_model
 * @property  CI_Session session
 */
class MY_Controller extends Auth_Controller
{

	public function __construct()
	{
		parent::__construct();

		$this->load->library('form_validation');
		//$this->load->library('session');
		$this->load->helper('url');
		$this->load->helper('form');

		//$this->load->helper('date');
		setlocale(LC_ALL, 'es_ES.UTF-8');
		date_default_timezone_set('Europe/Madrid');



		$this->is_logged_in();
		$this->setup_login_form();
	}
}

/* End of file MY_Controller.php */
/* Location: /community_auth/core/MY_Controller.php */
