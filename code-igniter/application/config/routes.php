<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'news';
$route['news'] = "news";
$route['news/(:any)'] = "news/index/$1";
$route['races'] = "races";
$route['races/(:any)'] = "races/index/$1";

$route['api/city'] = "api/cities_search";
$route['api/city/(:num)'] = "api/cities_read/$1";
$route['api/city/(:any)'] = "api/cities_search/$1";
$route['api/city/province/(:num)'] = "api/cities_by_province/$1";

$route['api/race'] = "api/races_pagination";

$route['api/province'] = "api/provinces_search";
$route['api/province/(:num)'] = "api/provinces_read/$1";
$route['api/province/(:any)'] = "api/provinces_search/$1";

$route['api/sport'] = "api/sports_read";
$route['api/sport/(:num)'] = "api/sports_read/$1";

$route['api/category'] = "api/categories_read";
$route['api/category/(:num)'] = "api/categories_read/$1";
$route['api/category/sport/(:num)'] = "api/categories_by_sport/$1";

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

# Authentication routes
$route['login'] = 'user/login';
