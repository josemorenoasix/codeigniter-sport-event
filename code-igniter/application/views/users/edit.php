<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="container">
	<div class="section">
		<div class="row">
			<div class="card col s12 m10 offset-m1 l8 offset-l2 card-profile-edit no-padding">
				<form action="<?= base_url("user/update") ?>" method="post" enctype="multipart/form-data">
				<div class="card-image">
					<a id="background-edit-button" class="btn-floating halfway-fab waves-effect waves-light file-field transparent background-edit-button">
						<i class="material-icons">edit</i>
						<input type="file" name="background" accept="image/*" alt="user-background"/>
					</a>
					<img id="background" src="https://source.unsplash.com/random/700x300">
					<span class="card-title text-flow">Edita tu perfil</span>


					<label for="input-avatar">
						<a class="btn-floating halfway-fab photo-dropify ">
							<input id="input-avatar" type="file" class="" name="avatar" accept="image/*" alt="user-avatar"/>
							<img id="avatar" src="<?= base_url("assets/uploads/files/avatars/").$this->auth_image_avatar ?>">
						</a>
					</label>

				</div>
				<div class="card-content">
					<div class="row">
						<div class="input-field col s12 m6 l6">
							<input value="<?= $this->auth_firstname ?>" id="first_name" name="firstname" type="text" class="validate">
							<label for="first_name">Nombre</label>
						</div>
						<div class="input-field col s12 m6 l6">
							<input value="<?= $this->auth_lastname ?>" id="last_name" name="lastname" type="text" class="validate">
							<label for="last_name">Apellidos</label>
						</div>
						<div class="input-field col s12 m12 l12">
							<input value="<?= $this->auth_email ?>" id="email" name="email" type="email" class="validate">
							<label for="email">Email</label>
						</div>
						<div class="input-field col s12 m6 l6">
							<input value="<?= $this->auth_phone ?>" id="phone" name="phone" type="tel" class="validate">
							<label for="phone">Teléfono</label>
						</div>
						<div class="input-field col s12 m6 l6">
							<input value="<?= $this->auth_birthday ?>"  type="text" class="datepicker">
							<label for="birthday">Fecha de nacimiento</label>
							<input value="<?= $this->auth_birthday ?>" id="birthday" name="birthday" type="hidden">
						</div>
						<div class="input-field col s12 m12 l12">
							<select id="gender" name="gender">
							<?if (empty($this->auth_gender)): ?>
								<option disabled selected>Elige una opción</option>
								<option value="male">Hombre</option>
								<option value="female">Mujer</option>
								<option value="other">Otro</option>
							<?elseif ($this->auth_gender==="male"): ?>
								<option disabled>Elige una opción</option>
								<option value="male" selected>Hombre</option>
								<option value="female">Mujer</option>
								<option value="other">Otro</option>
							<?elseif ($this->auth_gender==="female"): ?>
								<option disabled>Elige una opción</option>
								<option value="male">Hombre</option>
								<option value="female" selected>Mujer</option>
								<option value="other">Otro</option>
							<?else: ?>
								<option disabled>Elige una opción</option>
								<option value="male">Hombre</option>
								<option value="female">Mujer</option>
								<option value="other" selected>Otro</option>
							<?endif; ?>
							</select>
							<label for="gender">Sexo</label>
						</div>
					</div>
				</div>
				<div class="card-action right-align">
					<button class="waves-effect waves-light btn-large light-blue" type="submit">Guardar</button>
				</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url("assets/js/user_edit.js") ?>"></script>

