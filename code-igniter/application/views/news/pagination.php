<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="section no-pad-bot" id="index-banner">
	<div class="container">
		<h1 class="header center orange-text">Sport News</h1>
		<div class="row center">
			<h5 class="header col s12 light">Últimas noticias deportivas </h5>
		</div>
		<div class="row center">
			<a href="#" id="download-button" class="btn-large waves-effect waves-light orange">Last news</a>
		</div>
		<br><br>
	</div>
</div>
<div class="container">
	<div class="section">
		<div class="row">
		<?php foreach($output as $key => $new): ?>
			<!--   Card Section   -->
			<div class="col s12 m6 l3">
				<div class="card">
					<div class="card-image waves-effect waves-block waves-light">
						<img class="activator" src="<?= base_url("assets/uploads/files/news/").$new["image"] ?>">
					</div>
					<div class="card-content">
						<span class="card-title activator grey-text text-darken-4">
							<?= $new["title"] ?>
							<i class="material-icons right">more_vert</i>
						</span>
						<p><a id="ajax_link" value="<?= $new["new_id"] ?>" href="#modal_new" class="waves-effect light-blue btn modal-trigger ajax_link" >Leer</a></p>
					</div>
					<div class="card-reveal">
						<span class="card-title grey-text text-darken-4"><?= $new["title"] ?><i class="material-icons right">close</i></span>
						<p><?= $new["description"] ?></p>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
		</div>
		<!-- /row -->
		<div style="padding: 10px">
			<?php echo $pagination ?>
		</div>
	</div>
</div>
<div id="modal_new" class="modal">
	<div class="modal-content">
		<h4 id="title"></h4>
		<p id="description"></p>
		<div id="body"></div>
	</div>
	<div class="modal-footer">
		<a href="#!" class="modal-close waves-effect waves-green btn-flat">Agree</a>
	</div>
</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.ajax_link').click(function(){
			var new_id =  $(this).attr('value');
			$.ajax({
				url:'<?=base_url()?>news/read_ajax/'+new_id,
				method: 'get',
				data: {new_id: new_id},
				dataType: 'json',
				success:
					function(response){
						console.log(response);
						$("#title").text(response.title);
						$("#description").text(response.description);
						$("#body").html(response.body);
					},
				error:
					function(XMLHttpRequest, textStatus, errorThrown) {
					alert("Status: " + textStatus); alert("Error: " + errorThrown);
				}
			});
		});
	});
</script>
