<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="section no-pad-bot" id="index-banner">
	<div class="container">
		<h1 class="header center orange-text">Sport Races</h1>
		<div class="row center">
			<h5 class="header col s12 light">Próximos eventos deportivos </h5>
		</div>
		<div class="row center">
			<a href="#" id="download-button" class="btn-large waves-effect waves-light orange">Last news</a>
		</div>
		<br><br>
	</div>
</div>
<div class="container">
	<div class="section">
		<!-- /row -->
		<div style="padding: 10px">
			<?php echo $pagination ?>
		</div>
		<div class="row">
			<?php
			// strlen($new["description"]) > 100 ? substr($new["description"],0,100)."..." : $new["description"];
			//print("<pre>".print_r($output,true)."</pre>"); ?>
		<?php foreach($output as $key => $new): ?>
			<!--   Card Section   -->
			<div class="col s12 m6 l4">
				<div class="card sticky-action ">
					<div class="card-image waves-effect waves-block waves-light">
						<img class="activator responsive-img" src="<?= base_url("assets/uploads/files/posters/").$new["poster"] ?>">
					</div>

					<div class="card-content">
						<span class="card-title activator grey-text text-darken-4">
							<?= $new["race_name"] ?>
							<i class="material-icons right">more_vert</i>
						</span>
					</div>

					<div class="card-action">
						<a value="<?= $new["race_id"] ?>" href="#modal_<?= $new["race_id"] ?>" class="btn-small modal-trigger waves-effect waves-light blue" >
							<i class="mdi mdi-plus left"></i>Leer
						</a>
						<?php if( !empty($new["google_map_url"]) && !empty($new["google_map_iframe"])):	?>
							<a href="<?= $new["google_map_url"] ?>" target="_blank" class="btn-small waves-effect waves-green light-green right">
								<i class="mdi mdi-google-maps left"></i>Mapa
							</a>
						<?php endif; ?>
					</div>

					<div class="card-reveal">
						<span class="card-title">Resumen:<i class="material-icons right">close</i></span>
						<div class="row center">

							<div class="col l6 m6 s6">
								<p>
									<i class="mdi small <?= $new["sport_icon"] ?>"></i>
									<br>
									Disciplina
								</p>
								<p>
									<a href="<?= base_url("races/category/").$new["category_id"]?>">
										<i class="mdi tiny mdi-map-marker-distance"></i>
										<?= number_format($new["distance"],0,'','.'); ?>m
									</a>
								</p>
								<p>

								</p>
							</div>
							<div class="col l6 m6 s6">
								<p>
									<i class="mdi small mdi-home-map-marker"></i>
									Municipio
								</p>
								<p>
									<a href="https://www.google.com/maps/@<?= $new["latitud"] ?>,<?= $new["longitud"] ?>,15z">
										<?= $new["municipio"] ?>
									</a>
									<br>
									(<?= $new["provincia"] ?>)
								</p>
							</div>
						</div>
						<div class="row center">
							<div class="col l6 m6 s6">
								<p>
									<i class="mdi small mdi-calendar-clock"></i>
									<br>
									Inscripciones
								</p>
								<p>
									<i class="tooltipped mdi tiny mdi-clock-start" data-position="left" data-tooltip="Inicio"></i>
									<?= date("d/m/y",strtotime($new["inscription_start_date"])) ?>
									<br>
									<i class="tooltipped mdi tiny mdi-clock-end" data-position="left" data-tooltip="Fin"></i>
									<?= date("d/m/y",strtotime($new["inscription_finish_date"])) ?>
								</p>

								<?php if( time() > strtotime($new["inscription_finish_date"]) ): ?>
									<p class="red-text">
										<i class="mdi tiny mdi-lock-clock"></i> Cerradas
									</p>
								<?php elseif (time() <  strtotime($new["inscription_start_date"]) ): ?>
									<p class="blue-grey-text">
										<i class="mdi tiny mdi-calendar-clock"></i>
										En espera: <br>
										<small><?= abs($new["inscription_start_date_counter"]) ?> días restantes</small>
									</p>

								<?php else : ?>
									<p class="green-text">
										<i class="mdi tiny mdi-clock-fast"></i> Abiertas
										<br>
										<small><?= abs($new["inscription_finish_date_counter"]) ?> días restantes</small>
									</p>
								<?php
								endif;
								?>

							</div>

							<div class="col l6 m6 s6 center">

								<p>
									<i class="mdi small mdi-clock-outline"></i>
									<br>
									Horario
								</p>
								<p>
									<i class="mdi tiny mdi-alarm"></i><?= strftime("%n %R", strtotime($new["date"])) ?>
									<br>
									<i class="mdi tiny mdi-calendar-blank"></i><?= strftime("%a, %e %b", strtotime($new["date"])) ?><br>
								</p>

								<?php if( time() > strtotime($new["date"]) ): ?>
									<p class="red-text">
										<i class="mdi tiny mdi-lock-clock"></i> Finalizado
									</p>
								<?php else : ?>
									<p class="indigo-text">
										<i class="mdi tiny mdi-clock-outline"></i>
										Próximo
										<br>
										<small>
											<?= abs($new["date_counter"]) ?> días restantes
										</small>

									</p>
								<?php endif; ?>


							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="modal_<?= $new["race_id"] ?>" class="modal">
				<div class="modal-content">
					<h4 id="race_name"></h4>
					<p id="date"></p>
					<div>
						<?= $new["description"] ?>
					</div>
				</div>
				<div class="modal-footer">
					<a href="#!" class="modal-close waves-effect waves-green btn-flat">Agree</a>
				</div>
			</div>

		<?php endforeach; ?>
		</div>
	</div>
</div>




<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.ajax_link').click(function(){
			var race_id =  $(this).attr('value');
			$.ajax({
				url:'<?=base_url()?>races/read_ajax/'+race_id,
				method: 'get',
				data: {race_id: race_id},
				dataType: 'json',
				success:
					function(response){
						console.log(response);
						$("#race_name").text(response.race_name);
						$("#description").text(response.description);
						$("#date").html(response.date);
					},
				error:
					function(XMLHttpRequest, textStatus, errorThrown) {
					alert("Status: " + textStatus); alert("Error: " + errorThrown);
				}
			});
		});
	});
</script>
