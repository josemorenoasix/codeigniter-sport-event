<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="section no-pad-bot" id="index-banner">
	<div class="container">
		<h1 class="header center orange-text">Sport Races</h1>
		<div class="row center">
			<h5 class="header col s12 light">Próximos eventos deportivos </h5>
		</div>
		<div class="row center">
			<a id="search-button" class="btn-large waves-effect waves-light orange">Search tool</a>
		</div>
		<br><br>
	</div>
</div>

<div id="filter-search" class="container scale-transition scale-out">

	<div class="section">
		<div class="row ">
			<form action="<?= base_url("races") ?>">
				<div class="input-field col s12">
					<i class="material-icons prefix">search</i>
					<input id="icon_prefix" type="text">
					<label for="icon_prefix">Search</label>
				</div>

				<div id="input-sports" class="col s6 m3 l3 filter-sport">
				</div>

				<div id="input-categories" class="col s6 m4 l3 filter-sport">
				</div>

				<div class="col s12 m5 l6">
					<p  class="input-field">
						<input type="text" id="province-input" autocomplete="off" class="autocomplete">
						<label for="province-input">Provincia</label>
						<input type="hidden" name="province" >
					</p>
					<p class="input-field">
						<input type="text" id="city-input" autocomplete="off" class="autocomplete">
						<label for="city-input">Municipio</label>
						<input type="hidden" name="city" >
					</p>
					<div class="col s6 no-padding">
						Inscripciones abiertas
						<div class="switch">
							<label>
								Off
								<input type="checkbox" name="opened_inscription" value="1">
								<span class="lever"></span>
								On
							</label>
						</div>
					</div>
					<div class="col s6 right-align">
						<button class="btn waves-effect waves-light" type="submit">Search
							<i class="material-icons left">search</i>
						</button>
					</div>

				</div>
			</form>
		</div>
	</div>

</div>

<div class="container">
	<div class="section">
		<!-- /row -->
		<div style="padding: 10px">
			<?php echo $pagination ?>
		</div>
		<div class="row">
			<?php
			// strlen($new["description"]) > 100 ? substr($new["description"],0,100)."..." : $new["description"];
			//print("<pre>".print_r($output,true)."</pre>"); ?>
		<?php foreach($output as $key => $new): ?>
			<!--   Card Section   -->
			<div class="col s12 m6 l3">
				<div class="card card-race">
					<div class="card-image">
						<img src="<?= base_url("assets/uploads/files/posters/").$new["poster"] ?>">
						<div class="card-title-top">
							<div class="col s12 no-padding center">
								<div class="autofill autofill-city"><?= $new["municipio"] ?></div>
							</div>
							<div class="col s6 m6 l6 offset-s3 offset-m3 offset-l3 no-padding center">
								<div class="autofill autofill-category"><?= $new["category_name"] ?> | <?= strftime("%b %G", strtotime($new["date"])) ?></div>
							</div>
						</div>
						<div class="card-title-bottom">
							<div class="col s6 m6 l6 offset-s3 offset-m3 offset-l3 no-padding center">
								<div class="autofill autofill-organizer">
									<?= $new["organizer"] ?> | <?= strftime("%a, %e %b %g", strtotime($new["date"])) ?>
								</div>
							</div>
							<div class="col s6 m6 l6 offset-s3 offset-m3 offset-l3 no-padding center">
								<div class="autofill autofill-time">
									--:--:--
								</div>
							</div>
						</div>
						<?if (empty($this->auth_role)) : ?>
							<a href="#modal1" class="btn-floating halfway-fab waves-effect waves-light modal-trigger red"><i class="material-icons">add</i></a>
						<?elseif ( time() < strtotime($new["inscription_start_date"]) ): ?>
							<a href="#join-<?= $new["race_id"] ?>" class="btn-floating halfway-fab waves-effect waves-light modal-trigger blue"><i class="mdi mdi-clock"></i></a>
							<div id="join-<?= $new["race_id"] ?>" class="modal">
								<div class="modal-content">
									<p>
										Proxima apertura.
									</p>
								</div>
								<div class="divider"></div>
								<div class="modal-footer">
									<a href="#!" class="modal-close waves-effect waves-green btn-flat"><i class="material-icons">close</i></a>
								</div>
							</div>
						<?elseif ( time() > strtotime($new["inscription_finish_date"]) ): ?>
							<a href="#join-<?= $new["race_id"] ?>" class="btn-floating halfway-fab waves-effect waves-light modal-trigger blue"><i class="mdi mdi-lock-clock"></i></a>
							<div id="join-<?= $new["race_id"] ?>" class="modal">
								<div class="modal-content">
									<p>
										Cerradas.
									</p>
								</div>
								<div class="divider"></div>
								<div class="modal-footer">
									<a href="#!" class="modal-close waves-effect btn-flat"><i class="material-icons">close</i></a>
								</div>
							</div>
						<?else: ?>
							<a href="#join-<?= $new["race_id"] ?>" value="<?= $new["race_id"] ?>" class="btn-floating halfway-fab waves-effect waves-light modal-trigger red"><i class="material-icons">add</i></a>

							<div id="join-<?= $new["race_id"] ?>" class="modal">
								<div class="modal-content">
									<p>
										Abiertas.
									</p>
									<? if (is_null($new["player_number"])): ?>
									<p>
										Inscribete
									</p>
									<? else : ?>
									<p>
										<?= $new["player_number"] ?>
									</p>
									<? endif; ?>

								</div>
								<div class="divider"></div>
								<div class="modal-footer">
									<a href="#!" class="modal-close waves-effect waves-green btn-flat"><i class="material-icons">close</i></a>
								</div>
							</div>
						<? endif; ?>
					</div>
					<div class="card-content">
						<span class="card-title flow-text" ><?= $new["race_name"] ?></span>
					</div>
					<div class="card-action">
						<a value="<?= $new["race_id"] ?>" href="#modal_<?= $new["race_id"] ?>" class="btn-small modal-trigger waves-effect waves-light blue" >
							<i class="mdi mdi-plus left"></i>Leer
						</a>
					</div>
				</div>
			</div>
			<div id="modal_<?= $new["race_id"] ?>" class="modal">
				<div class="modal-content">
					<h4 id="race_name"><?= $new["race_name"] ?></h4>
					<div class="divider"></div>
					<div class="row center">
						<div class="col s12 m6 l3 race-summary">
							<p>
								<i class="mdi small <?= $new["sport_icon"] ?>"></i>
								<br>
								Disciplina
							</p>
							<p>
								<a href="<?= base_url("races/category/").$new["category_id"] ?>">
									<i class="mdi tiny mdi-map-marker-distance"></i>
									<?= $new["category_name"]; ?>
								</a>
								<br>
								(<?= number_format($new["distance"],0,'','.'); ?>m)
							</p>
							<p>
								<a href="<?= base_url("races/").$new["race_id"] ?>">
									<i class="mdi tiny mdi-run"></i>
									Participantes
								</a>
								<br>
								<?php if($new["num_players"] == 0): ?>
									<small>Sé el primero en inscribirte</small>
								<? else: ?>
									<small><?= number_format($new["num_players"],0,'','.'); ?></small>
								<? endif; ?>
							</p>
						</div>
						<div class="col s12 m6 l3 race-summary">
							<p>
								<i class="mdi small mdi-home-map-marker"></i>
								<br>
								Municipio
							</p>
							<p>
								<a target="_blank" href="https://www.google.com/maps/@<?= $new["latitud"] ?>,<?= $new["longitud"] ?>,15z">
									<?= $new["municipio"] ?>
								</a>
								<br>
								<?php if ($new["municipio"] != $new["provincia"]): ?>
									(<?= $new["provincia"] ?>)
								<?php else: ?>
									<br>
								<? endif; ?>
							</p>
							<p>
								<?php if( !empty($new["google_map_url"]) && !empty($new["google_map_iframe"])):	?>
									<a href="<?= $new["google_map_url"] ?>" target="_blank" class="btn-small waves-effect waves-green light-green">
										<i class="mdi mdi-google-maps left"></i>Mapa
									</a>
								<?php endif; ?>
							</p>
						</div>
						<div class="col s12 m6 l3 race-summary">
							<p>
								<i class="mdi small mdi-calendar-clock"></i>
								<br>
								Inscripciones
							</p>
							<p>
								<i class="tooltipped mdi tiny mdi-clock-start" data-position="left" data-tooltip="Inicio"></i>
								<?= strftime("%d %b %y", strtotime($new["inscription_start_date"]))?>
								<br>
								<i class="tooltipped mdi tiny mdi-clock-end" data-position="left" data-tooltip="Fin"></i>
								<?= strftime("%d %b %y", strtotime($new["inscription_finish_date"])) ?>
							</p>
							<?php if( time() > strtotime($new["inscription_finish_date"]) ): ?>
								<p class="red-text">
									<i class="mdi tiny mdi-lock-clock"></i> Cerradas
								</p>
							<?php elseif (time() <  strtotime($new["inscription_start_date"]) ): ?>
								<p class="blue-grey-text">
									<i class="mdi tiny mdi-calendar-clock"></i>
									En espera: <br>
									<small><?= abs($new["inscription_start_date_counter"]) ?> días restantes</small>
								</p>

							<?php else : ?>
								<p class="green-text">
									<i class="mdi tiny mdi-clock-fast"></i> Abiertas
									<br>
									<small><?= abs($new["inscription_finish_date_counter"]) ?> días restantes</small>
								</p>
							<?php
							endif;
							?>
						</div>
						<div class="col s12 m6 l3 race-summary">
							<p>
								<i class="mdi small mdi-clock-outline"></i>
								<br>
								Horario
							</p>
							<p>
								<i class="mdi tiny mdi-alarm"></i><?= strftime("%n %R", strtotime($new["date"])) ?>
								<br>
								<i class="mdi tiny mdi-calendar-blank"></i><?= strftime("%a, %e %b", strtotime($new["date"])) ?><br>
							</p>

							<?php if( time() > strtotime($new["date"]) ): ?>
								<p class="red-text">
									<i class="mdi tiny mdi-lock-clock"></i> Finalizado
								</p>
							<?php else : ?>
								<p class="indigo-text">
									<i class="mdi tiny mdi-clock-outline"></i>
									Próximo
									<br>
									<small>
										<?= abs($new["date_counter"]) ?> días restantes
									</small>

								</p>
							<?php endif; ?>
						</div>
					</div>
					<div class="divider"></div>
					<div>
						<?= $new["description"] ?>
					</div>
				</div>
				<div class="divider"></div>
				<div class="modal-footer">
					<a href="#!" class="modal-close waves-effect waves-green btn-flat">Agree</a>
				</div>
			</div>
		<?php endforeach; ?>
		</div>
	</div>
</div>



<!--
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		var race_id;

		$('a[href="#join"]').click(function(){
			race_id =  $(this).attr('value');
			console.log(race_id);
		});

		$('.ajax-join').click(function(){
			if (race_id){
				$.ajax({
					url:'<?php //base_url()?>races/join/'+race_id,
					method: 'get',
					data: {race_id: race_id},
					dataType: 'json',
					success:
						function(response){
							console.log(response);
						},
					error:
						function(XMLHttpRequest, textStatus, errorThrown) {
							alert("Status: " + textStatus); alert("Error: " + errorThrown);
						}
				});
			}
		});
	});
</script> -->
