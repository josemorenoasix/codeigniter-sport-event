<div class="container">
	<div class="section">
		<div class="row">
			<!--   Card Section   -->
			<div class="col s12 m12 l12">
				<div class="card-panel grey lighten-5 z-depth-1">
					<div class="row valign-wrapper">
						<div class="col s2">
							<img src="/img/avatar/001-boy.png" alt="" class="circle responsive-img"> <!-- notice the "circle" class -->
						</div>
						<div class="col s10">
             			<span class="black-text">
                			This is a square image. Add the "circle" class to it to make it appear circular.
              			</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
