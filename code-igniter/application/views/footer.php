<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<footer class="page-footer orange">
	<div class="container">
		<div class="row">
			<div class="col l6 s12">
				<h5 class="white-text">Company Bio</h5>
				<p class="grey-text text-lighten-4">We are a team of college students working on this project like it's our full time job. Any amount would help support and continue development on this project and is greatly appreciated.</p>


			</div>
			<div class="col l3 s12">
				<h5 class="white-text">Settings</h5>
				<ul>
					<li><a class="white-text" href="#!">Link 1</a></li>
					<li><a class="white-text" href="#!">Link 2</a></li>
					<li><a class="white-text" href="#!">Link 3</a></li>
					<li><a class="white-text" href="#!">Link 4</a></li>
				</ul>
			</div>
			<div class="col l3 s12">
				<h5 class="white-text">Connect</h5>
				<ul>
					<li><a class="white-text" href="#!">Link 1</a></li>
					<li><a class="white-text" href="#!">Link 2</a></li>
					<li><a class="white-text" href="#!">Link 3</a></li>
					<li><a class="white-text" href="#!">Link 4</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="footer-copyright">
		<div class="container">
			Made by <a class="orange-text text-lighten-3" href="http://materializecss.com">Materialize</a>
		</div>
	</div>
</footer>

</body>
<script type="text/javascript" src="<?php echo base_url("assets/js/jquery-3.3.1.min.js") ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/moment-with-locales.js") ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/fitty.min.js") ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/materialize.js") ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/init.js") ?>"></script>

</html>
<?if (!is_null($this->session->flashdata('login_success'))): ?>
	<?if ($this->session->flashdata('login_success')): ?>
	<script >
		M.toast({html: 'Login success!', classes: 'rounded green'})
	</script>
	<?else: ?>
	<script >
		$(document).ready(function(){
			$('.tabs').tabs('updateTabIndicator');
			$('#modal1').modal('open');
		});
		M.toast({html: 'Bad login!', classes: 'rounded red'})
	</script>
	<?endif; ?>
<?endif; ?>

<?if ($this->session->flashdata('on_hold') === true): ?>
	<script >
		M.toast({html: 'Temporary block!', classes: 'rounded red'})
	</script>
<?endif; ?>

<?if ($this->input->get(AUTH_LOGOUT_PARAM)): ?>
	<script >
		M.toast({html: 'Logged out!', classes: 'rounded blue'})
	</script>
<?endif; ?>

<?if (!is_null($this->session->flashdata('create_success'))): ?>
	<?if ($this->session->flashdata('create_success')): ?>
		<script >
			$(document).ready(function(){
				let tabs = $('.tabs');
				tabs.tabs('updateTabIndicator');
				tabs.tabs('select','login');
				$('#modal1').modal('open');
			});
			M.toast({html: 'Account created!', classes: 'rounded green'});
		</script>
	<?else: ?>
		<script >
			$(document).ready(function(){
				let tabs = $('.tabs');
				tabs.tabs('updateTabIndicator');
				tabs.tabs('select','register');
				$('#modal1').modal('open');
			});
			var data = <?php echo json_encode($this->session->flashdata('create_errors'))?>;
			M.toast({html: 'Account create failed!', classes: 'rounded red'});
			for (var error in data) {
				M.toast({html: data[error]  , classes: 'rounded blue'});
			}
		</script>
	<?endif; ?>
<?endif; ?>

<?if (!is_null($this->session->flashdata('update_user_success'))): ?>
	<?if ($this->session->flashdata('update_user_success')): ?>
		<script >
			M.toast({html: 'Update success!', classes: 'rounded green'})
		</script>
	<?else: ?>
		<script >
			M.toast({html: 'Update failed!', classes: 'rounded red'})
		</script>
	<?endif; ?>
<?endif; ?>

