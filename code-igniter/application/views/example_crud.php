<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php 
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
	<a class="navbar-brand" href="#">Admin</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarNav">
		<ul class="navbar-nav">
			<li class="nav-item active">
				<a class="nav-link" href='<?php echo site_url('/')?>'>Home</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href='<?php echo site_url('crud/sports')?>'>Deportes</a>
			</li>
			<li class="nav-item">
				<a class="nav-link"href='<?php echo site_url('crud/categories')?>'>Categorias</a>
			</li>
			<li class="nav-item">
				<a class="nav-link " href='<?php echo site_url('crud/races')?>'>Carreras</a>
			</li>
			<li class="nav-item">
				<a class="nav-link " href='<?php echo site_url('crud/news')?>'>Noticias</a>
			</li>
			<li class="nav-item">
				<a class="nav-link " href='<?php echo site_url('crud/users')?>'>Usuarios</a>
			</li>
			<li class="nav-item">
				<a class="nav-link " href='<?php echo site_url('crud/players')?>'>Participantes</a>
			</li>
		</ul>
	</div>
</nav>

	<div style='height:20px;'></div>  
    <div style="padding: 10px">
		<?php echo $output; ?>
    </div>
    <?php foreach($js_files as $file): ?>
        <script src="<?php echo $file; ?>"></script>
    <?php endforeach; ?>
</body>
</html>
