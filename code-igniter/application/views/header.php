<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->helper('url');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Sport blog with CodeIgniter</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

	<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons|Montserrat|Roboto+Slab|Nova+Mono">
	<link type="text/css" rel="stylesheet" href="//cdn.materialdesignicons.com/3.0.39/css/materialdesignicons.min.css">
	<link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/css/materialize.css") ?>" media="screen,projection">
	<link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/css/style.css") ?>" media="screen,projection"/>

	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="manifest" href="/site.webmanifest">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="/mstile-144x144.png">
	<meta name="theme-color" content="#f1f1f1">


</head>
<body>
<nav class="light-blue lighten-1" role="navigation">
	<div class="nav-wrapper container">

		<a id="logo-container" href="#" class="brand-logo">Sport-Ready</a>

		<ul class="right hide-on-med-and-down">
			<li><a href="#">
					<?=  strftime("%A, %e %b %n %R"); ?>
				</a></li>
			<?if( $this->auth_role == 'admin' ):?>
				<li><a href="<?=  base_url("crud") ?>">Admin</a></li>
			<?endif;?>
			<li><a href="<?=  base_url("news") ?>">Noticias</a></li>
			<li><a href="<?=  base_url("races") ?>">Carreras</a></li>

			<?php
			if (empty($this->auth_role)) : ?>
				<li>
					<a href="#modal1" title="Account" class="blue waves-effect waves-light btn modal-trigger">
						<i class="large material-icons">power_settings_new</i>
					</a>
				</li>
			<?php else: ?>
				<li>
					<a href="#slide-out" title="<?= $this->auth_email ?>" class="sidenav-trigger show-on-large profile-button">
                  			<span class="avatar-status avatar-online">
                    			<img src="<?= base_url("assets/uploads/files/avatars/").$this->auth_image_avatar ?>" >
                  			</span>
					</a>
				</li>
				<ul id="slide-out" class="sidenav">
					<li>
						<div class="user-view">
							<div class="background">
								<img class="" src="<?= base_url("assets/uploads/files/backgrounds/").$this->auth_image_background ?>">
							</div>
							<a href="<?= base_url("user/edit") ?>"><img class="circle" src="<?= base_url("assets/uploads/files/avatars/").$this->auth_image_avatar ?>"></a>
							<a href="<?= base_url("user/edit") ?>"><span class="white-text name"><?= $this->auth_firstname ?></span></a>
							<a href="<?= base_url("user/edit") ?>"><span class="white-text email"><?= $this->auth_email ?></span></a>
						</div>
					</li>

					<li><a href="<?= base_url("news") ?>"><i class="mdi mdi-newspaper"></i>Noticias</a></li>
					<li><a href="<?= base_url("races") ?>"><i class="mdi mdi-run-fast"></i>Carreras</a></li>
					<li><div class="divider"></div></li>
					<?if( $this->auth_role == 'admin' ):?>
						<li><a href="<?=  base_url("crud") ?>"><i class="mdi mdi-settings-outline"></i>Admin</a></li>
					<?endif;?>
					<li>
						<a class="" href="<?= base_url("user/logout") ?>" >
							<i class="large material-icons">exit_to_app</i> Cerrar sesión
						</a>
					</li>
  				</ul>
			<?php endif; ?>


		</ul>
		<ul id="nav-mobile" class="sidenav">

			<?php
			if (empty($this->auth_role)) : ?>
				<div class="user-view">
					<div class="background">
						<img class="" src="<?= base_url("assets/uploads/files/backgrounds/default-background.jpg") ?>">
					</div>
					<a href="#modal1" title="Inicia sesión" class="waves-effect waves-light modal-trigger">
						<img class="circle" src="<?= base_url("assets/uploads/files/avatars/default-avatar.png") ?>">
						<span class="white-text name">Bienvenido</span>
						<span class="white-text email">Inicia sesión</span>
					</a>
				</div>
				<li><a href="<?= base_url("news") ?>"><i class="mdi mdi-newspaper"></i>Noticias</a></li>
				<li><a href="<?= base_url("races") ?>"><i class="mdi mdi-run-fast"></i>Carreras</a></li>
				<li><div class="divider"></div></li>
				<?if( $this->auth_role == 'admin' ):?>
					<li><a href="<?=  base_url("crud") ?>"><i class="mdi mdi-settings-outline"></i>Admin</a></li>
				<?endif;?>
				<li>
					<a href="#modal1" title="Account" class="waves-effect waves-light modal-trigger">
						<i class="large material-icons">power_settings_new</i> Iniciar sesión
					</a>
				</li>
				<li>

				</li>
			<?php else: ?>
				<li>
					<div class="user-view">
						<div class="background">
							<img class="" src="<?= base_url("assets/uploads/files/backgrounds/").$this->auth_image_background ?>">
						</div>
						<a href="<?= base_url("user/edit") ?>"><img class="circle" src="<?= base_url("assets/uploads/files/avatars/").$this->auth_image_avatar ?>"></a>
						<a href="<?= base_url("user/edit") ?>"><span class="white-text name"><?= $this->auth_firstname ?></span></a>
						<a href="<?= base_url("user/edit") ?>"><span class="white-text email"><?= $this->auth_email ?></span></a>
					</div>
				</li>
				<li><a href="<?= base_url("news") ?>"><i class="mdi mdi-newspaper"></i>Noticias</a></li>
				<li><a href="<?= base_url("races") ?>"><i class="mdi mdi-run-fast"></i>Carreras</a></li>
				<li><div class="divider"></div></li>
				<?if( $this->auth_role == 'admin' ):?>
					<li><a href="<?=  base_url("crud") ?>"><i class="mdi mdi-settings-outline"></i>Admin</a></li>
				<?endif;?>
				<li>
					<a class="" href="<?= base_url("user/logout") ?>" >
						<i class="large material-icons">exit_to_app</i> Cerrar sesión
					</a>
				</li>
			<?php endif; ?>

		</ul>

		<a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>


	</div>
</nav>

<div id="modal1" class="modal">
	<div class="col s12">
		<ul class="tabs light-blue">
			<li id="tab-login" class="tab col s3"><a class="white-text active" href="#login">login</a></li>
			<li id="tab-register" class="tab col s3"><a class="white-text" href="#register">register</a></li>
		</ul>
	</div>

	<div class="modal-content">
		<?php
		echo form_open( base_url("/login"), ['class' => 'std-form'] );
		?>
			<div class="form-container">
				<div id="login" class="row">
					<h3 class="light-blue-text">Hello</h3>
					<div class="input-field col s12 m12 l12">
						<input id="login-email" name="login_string" type="email" class="validate"
							   autocomplete="on" required>
						<label for="login-email" class="light-blue-text">Email</label>
					</div>
					<div class="input-field col s12 m12 l12">
						<input id="login-password" name="login_pass" type="password" required>
						<label for="login-password" class="light-blue-text">Password</label>
					</div>
					<div class="input-field col s12 center">
						<button class="btn waves-effect waves-light light-blue" type="submit" name="action">
							Login
						</button>
					</div>
					<div class="modal-footer">
						<div class="input-field col s12 right">
							<a href="#" class="modal-close waves-effect waves-green btn-flat">Forgot
								password?</a>
						</div>
					</div>
				</div>
			</div>
		<?php
		echo "</form>";
		?>
		<form action="<?= base_url("user/create") ?>" method="post">
			<div class="form-container">
				<div id="register" class="row">
					<h3 class="light-blue-text">Welcome</h3>
					<div class="input-field col s12 m12 l4">
						<input id="input-register-firstname" name="firstname" type="text"
							   class="validate" required>
						<label for="input-register-firstname" class="light-blue-text">Firstname</label>
					</div>
					<div class="input-field col s12 m12 l8">
						<input id="input-register-lastname" name="lastname" type="text"
							   class="validate" required>
						<label for="input-register-lastname" class="light-blue-text">Lastname</label>
					</div>
					<div class="input-field col s12 m12 l12">
						<input id="input-register-email" name="email" type="email"
							   class="validate" required>
						<label for="input-register-email" class="light-blue-text">Email</label>
					</div>
					<div class="input-field col s12 m12 l6">
						<input id="input-register-password" name="passwd" type="password"
							   class="validate" required>
						<label for="input-register-password" class="light-blue-text">Password</label>
					</div>
					<div class="input-field col s12 m12 l6">
						<input id="input-register-password-confirm" name="passwd_confirm" type="password"
							   required>
						<label for="input-register-password-confirm" class="light-blue-text">Confirm password </label>
						<span class="helper-text" data-error="Password not match" data-success="Password Match"></span>
					</div>
					<div class="modal-footer">
						<div class="input-field col s12 center">
							<button class="btn waves-effect waves-light light-blue" type="submit" name="action">
								Submit
							</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>


