<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property  Sports_model races_model
 * @property  Categories_model categories_model
 * @property  CI_Pagination pagination
 */

class Categories extends MY_Controller
{
	public $category_name;

	public function __construct()
	{
		parent::__construct();

		$this->load->model('sports_model');
		$this->load->model('categories_model');
	}

	public function _print($view,$data){
		$this->load->view('header');
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index($num = 1)
	{
		$this->load->library('pagination');
		$this->_print("races/pagination",$this->_get_pagination($num));
	}

	public function read_ajax($id = NULL)
	{
		$data['output'] = $this->categories_model->read($id);
		$this->load->view("json", $data);
	}

}
