<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property  Grocery_CRUD grocery_crud
 * @property  CI_DB db
 * @property  Authentication authentication
 * @property  Examples_model examples_model
 */
class Crud extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		if( $this->require_role('admin') )
		{
			$this->load->library('grocery_CRUD');
		}
	}

	public function index()
	{
		$this->_example_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
		//$this->_example_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}

	public function _example_output($output = null)
	{
		$this->load->view('example_crud.php',(array)$output);
	}

	function _generate_user($post_array)
	{
		$this->load->model('examples/examples_model');
		$post_array['passwd'] = $this->authentication->hash_passwd($post_array['passwd']);
		$post_array['user_id'] = $this->examples_model->get_unused_id();
		$post_array['created_at'] = date('Y-m-d H:i:s');
		return $post_array;
	}

	public function users()
	{

		$this->load->model('examples/validation_callables');

		$this->grocery_crud->set_table('users');
		$this->grocery_crud->set_subject('Usuarios');

		$this->grocery_crud->display_as('email', 'Email');
		$this->grocery_crud->display_as('auth_level', 'Rol de usuario');
		$this->grocery_crud->display_as('passwd', 'Password');
		$this->grocery_crud->display_as('banned', 'Estado de la cuenta');
		$this->grocery_crud->display_as('created_at', 'Fecha de creación');
		$this->grocery_crud->display_as('last_login', 'Último login');

		$this->grocery_crud->display_as('firstname', 'Nombre');
		$this->grocery_crud->display_as('lastname', 'Apellidos');
		$this->grocery_crud->display_as('phone', 'Telefono');
		$this->grocery_crud->display_as('city', 'Ciudad');
		$this->grocery_crud->display_as('address', 'Dirección');
		$this->grocery_crud->display_as('image_avatar', 'Avatar');
		$this->grocery_crud->display_as('image_background', 'Background');
		$this->grocery_crud->display_as('birthday', 'Fecha de nacimiento');
		$this->grocery_crud->display_as('created_at', 'Fecha de creación');
		$this->grocery_crud->display_as('modified_at', 'Última modificación');
		$this->grocery_crud->display_as('gender', 'Sexo');

		$this->grocery_crud->columns('email','auth_level','banned','created_at');

		$this->grocery_crud->add_fields(
			'user_id','email','passwd','auth_level','banned','created_at'
		);
		$this->grocery_crud->edit_fields(
			'email','auth_level','banned','firstname','lastname','phone','address','image_avatar','image_background','birthday','gender'
		);
		$this->grocery_crud->required_fields('email','passwd','auth_level','banned');

		$this->grocery_crud->set_rules(
			[
				[
					'field' => 'passwd',
					'label' => 'passwd',
					'rules' => [
						'trim',
						'required',
						[
							'_check_password_strength',
							[ $this->validation_callables, '_check_password_strength' ]
						]
					],
					'errors' => [
						'required' => 'The password field is required.'
					]
				],
				[
					'field'  => 'email',
					'label'  => 'email',
					'rules'  => 'trim|required|valid_email',
					'errors' => [
						'valid_email' => 'Email no válido.'
					]
				],
				[
					'field'  => 'phone',
					'label'  => 'phone',
					'rules'  => 'numeric',
					'errors' => [
						'numeric' => 'Número invalido.'
					]
				]
			]
		);

		$this->grocery_crud->set_relation('city','municipios','municipio');
		$this->grocery_crud->set_field_upload('image_avatar','assets/uploads/files/avatars');
		$this->grocery_crud->set_field_upload('image_background','assets/uploads/files/backgrounds');

		$this->grocery_crud->field_type('user_id','invisible');
		$this->grocery_crud->field_type('created_at', 'invisible');
		$this->grocery_crud->field_type('passwd', 'password');
		$this->grocery_crud->field_type('banned','dropdown',	array('0' => '(0) Activo', '1' => '(1) Baneado'));
		$this->grocery_crud->field_type('auth_level','dropdown',
			array('1' => '(1) Simple', '6' => '(6) Editor', '9' => '(9) Administrador' ));
		$this->grocery_crud->field_type('gender','dropdown',
			array('male' => 'Hombre', 'female' => 'Mujer', 'other' => 'Otro'));

		$this->grocery_crud->callback_before_insert(array($this,'_generate_user'));

		$output = $this->grocery_crud->render();
		$this->_example_output($output);
	}

	public function sports()
	{

		try {
			$this->grocery_crud->set_table('sports');
			$this->grocery_crud->set_subject('Deporte');

			$this->grocery_crud->display_as('sport_name', 'Disciplina');

			$this->grocery_crud->columns('sport_name');
			$this->grocery_crud->required_fields('sport_name');

			$this->grocery_crud->add_fields(
				'sport_name','sport_icon' );
			$this->grocery_crud->edit_fields(
				'sport_name','sport_icon');


			$output = $this->grocery_crud->render();
			$this->_example_output($output);

		} catch (Exception $e) {
			show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
		}
	}

	public function categories()
	{

		try {
			$this->grocery_crud->set_table('categories');
			$this->grocery_crud->set_subject('Categoría');

			$this->grocery_crud->set_relation('sport_id','sports', 'sport_name');

			$this->grocery_crud->display_as('category_name', 'Categoría');
			$this->grocery_crud->display_as('sport_id', 'Deporte');

			$this->grocery_crud->columns('sport_id','category_name');
			$this->grocery_crud->required_fields('sport_id','category_name');

			$this->grocery_crud->add_fields('sport_id','category_name');
			$this->grocery_crud->edit_fields('sport_id','category_name');

			$output = $this->grocery_crud->render();
			$this->_example_output($output);

		} catch (Exception $e) {
			show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
		}

	}

	public function races()
	{

		try{

			$this->grocery_crud->set_table('races');
			$this->grocery_crud->set_subject('Carrera');

			$this->grocery_crud->display_as('category_id','Categoría');
			$this->grocery_crud->display_as('city','Ciudad');
			$this->grocery_crud->display_as('race_name','Nombre');
			$this->grocery_crud->display_as('description','Descripción');
			$this->grocery_crud->display_as('distance','Distancia (Metros)');
			$this->grocery_crud->display_as('date','Fecha de convocatoria');
			$this->grocery_crud->display_as('inscription_start_date',	'Inicio de inscripción');
			$this->grocery_crud->display_as('inscription_finish_date','Fin de inscripción');
			$this->grocery_crud->display_as('poster','Cartel');
			$this->grocery_crud->display_as('google_map_url','Google Map Url');
			$this->grocery_crud->display_as('google_map_iframe','Google Map Iframe');
			$this->grocery_crud->display_as('rules','Reglamento');
			$this->grocery_crud->display_as('organizer','Organizador');
			$this->grocery_crud->display_as('is_active','Visible');

			$this->grocery_crud->columns(
				'category_id','city','race_name','date','poster');

			$this->grocery_crud->required_fields(
				'category_id','city','race_name','description','distance',
				'date','inscription_start_date','inscription_finish_date',
				'poster');

			$this->grocery_crud->add_fields(
				'category_id', 'city','race_name','description','distance',
				'date','inscription_start_date','inscription_finish_date',
				'poster', 'google_map_url','google_map_iframe','rules','organizer','is_active');
			$this->grocery_crud->edit_fields(
				'category_id', 'city','race_name','description','distance',
				'date','inscription_start_date','inscription_finish_date',
				'poster', 'google_map_url','google_map_iframe','rules','organizer','is_active');

			$this->grocery_crud->set_relation('category_id','categories', '{category_name}');
			$this->grocery_crud->set_relation('city','municipios','municipio');
			$this->grocery_crud->set_field_upload('poster','assets/uploads/files/posters');

			$output = $this->grocery_crud->render();

			$this->_example_output($output);

		} catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function news()
	{

		try{

			$this->grocery_crud->set_table('news');
			$this->grocery_crud->set_subject('Noticia');

			$this->grocery_crud->display_as('autor',	'Autor');
			$this->grocery_crud->display_as('title','Titular');
			$this->grocery_crud->display_as('description','Cabecera');
			$this->grocery_crud->display_as('body','Cuerpo');
			$this->grocery_crud->display_as('image','Imagen');

			$this->grocery_crud->columns('title','autor','image','description');

			$this->grocery_crud->required_fields('title','autor','description','image','body');
			$this->grocery_crud->add_fields('title','autor','description','image','body');
			$this->grocery_crud->edit_fields('title','autor','description','image','body');


			$this->grocery_crud->set_relation('autor','users','{firstname} {lastname}');
			$this->grocery_crud->set_field_upload('image','assets/uploads/files/news');

			$output = $this->grocery_crud->render();
			$this->_example_output($output);

		} catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function players(){

		try{

			$this->grocery_crud->set_model('Custom_grocery_crud_model');
			$this->grocery_crud->set_subject('Participante');

			$query = <<<SQL
SELECT *
FROM `players`
INNER JOIN `races`
ON players.race_id = races.race_id
INNER JOIN `categories`
ON races.category_id = categories.category_id
INNER JOIN `sports`
ON categories.sport_id = sports.sport_id
INNER JOIN `users`
ON players.user_id = users.user_id
INNER JOIN `municipios`
ON races.city = municipios.id
SQL;
			$this->grocery_crud->basic_model->set_custom_query($query);



			$this->grocery_crud->columns(
				'firstname', 'lastname', 'sport_name','category_name','race_name','municipio','position','time'
			);

			$this->grocery_crud->display_as('firstname', 'Nombre');
			$this->grocery_crud->display_as('lastname', 'Apellidos');
			$this->grocery_crud->display_as('race_name','Carrera');
			$this->grocery_crud->display_as('municipio','Municipio');
			$this->grocery_crud->display_as('distance',	'Distancia (Metros)');
			$this->grocery_crud->display_as('position','Posición');
			$this->grocery_crud->display_as('time','Marca');

			$this->grocery_crud->required_fields(
				'race_id','person_id','number');

			$this->grocery_crud->add_fields('race_id', 'user_id','number');
			$this->grocery_crud->edit_fields('race_id', 'user_id','number','position','time');

			$this->grocery_crud->set_relation('race_id','races','{race_name}');
			$this->grocery_crud->set_relation('user_id','users','{email}');


			$output = $this->grocery_crud->render();
			$this->_example_output($output);

		} catch(Exception $e){

			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

}
