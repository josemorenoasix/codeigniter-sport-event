<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property  News_model $news_model
 * @property  CI_Pagination pagination
 */

class News extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('news_model');

	}

	public function _print($view,$data){
		$this->load->view('header');
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index($num = 1)
	{
		$this->load->library('pagination');
		$this->_print("news/pagination",$this->_get_pagination($num));
	}

	public function _get_pagination($num){

		$per_page = 4;

		$this->pagination->initialize(
			$this->_get_pagination_config(
				"/news",
				$this->news_model->count(),
				$per_page
			)
		);

		$data['pagination'] = $this->pagination->create_links();
		$data['output'] = $this->news_model->pagination($num,$per_page);

		return $data;

	}


	public function _get_pagination_config($uri, $total_rows, $per_page)
	{
		$max_size = 20;

		$config['base_url'] = base_url($uri);
		$config['total_rows'] = $total_rows;
		$config['per_page'] = ($per_page > $max_size ? $max_size : $per_page);
		$config['use_page_numbers'] = TRUE;

		$config['suffix']= '';
		$config['prefix']= '';

		/*
		 *  <divclass="col s12 m12 center">
    			<ul class="pagination">
    			    <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
    			    <li class="active"><a href="#!">1</a></li>
    			    <li class="waves-effect"><a href="#!">2</a></li>
    			    <li class="waves-effect"><a href="#!">3</a></li>
    			    <li class="waves-effect"><a href="#!">4</a></li>
    			    <li class="waves-effect"><a href="#!">5</a></li>
    			    <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
    			</ul>
			</div>
		 *
		 * */

		$config['full_tag_open'] = '<div class="col s12 m12 center"><ul class="pagination">';
		$config['full_tag_close'] = '</ul></div>';

		$config['num_tag_open'] = '<li class="waves-effect">';
		$config['num_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['first_url'] = '';
		$config['first_link'] = '<i class="material-icons">first_page</i>';
		$config['first_tag_open'] = '<li class="waves-effect">';
		$config['first_tag_close'] = '</li>';

		$config['last_link'] = '<i class="material-icons">last_page</i>';
		$config['last_tag_open'] = '<li class="waves-effect">';
		$config['last_tag_close'] = '</li>';

		$config['next_link'] = '<i class="material-icons">chevron_right</i>';
		$config['next_tag_open'] = '<li class="waves-effect">';
		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = '<i class="material-icons">chevron_left</i>';
		$config['prev_tag_open'] = '<li class="waves-effect">';
		$config['prev_tag_close'] = '</li>';

		return $config;

	}

	public function read($id = NULL)
	{
		$data['output'] = $this->news_model->read($id);
		$this->_print("news/read",$data);
	}

	public function read_ajax($id = NULL)
	{
		$data['output'] = $this->news_model->read($id);
		$this->load->view("json", $data);
	}
}
