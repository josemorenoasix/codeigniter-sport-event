<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property  Sports_model sports_model
 * @property  Categories_model categories_model
 * @property  Cities_model cities_model
 * @property  Provinces_model provinces_model
 * @property  Races_model races_model
 *
 */

class Api extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('sports_model');
		$this->load->model('categories_model');
		$this->load->model('cities_model');
		$this->load->model('provinces_model');
		$this->load->model('races_model');
	}

	public function _print($view,$data){
		$this->load->view('header');
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		$data['output'] = "Welcome to API";
		$this->load->view("json", $data);
	}

	public function sports_read($id = null)
	{
		$data['output'] = $this->sports_model->read($id);
		$this->load->view("json", $data);
	}

	public function categories_read($id = null)
	{
		$data['output'] = $this->categories_model->read($id);
		$this->load->view("json", $data);
	}

	public function categories_by_sport($id = null)
	{
		$data['output'] = $this->categories_model->readBySport($id);
		$this->load->view("json", $data);
	}

	public function cities_read($id = null)
	{
		$data['output'] = $this->cities_model->read($id);
		$this->load->view("json", $data);
	}
	public function cities_search($string = null)
	{
		$data['output'] = $this->cities_model->search($string);
		$this->load->view("json", $data);
	}

	public function provinces_read($id = null)
	{
		$data['output'] = $this->provinces_model->read($id);
		$this->load->view("json", $data);
	}
	public function provinces_search($string = null)
	{
		$data['output'] = $this->provinces_model->search($string);
		$this->load->view("json", $data);
	}

	public function cities_by_province($id = null)
	{
		$data['output'] = $this->cities_model->readByProvince($id);
		$this->load->view("json", $data);
	}

	public function races_pagination()
	{
		if (empty($_POST)){
			return;
		}

		$category = null;
		$province = null;
		$city = null;
		$opened_inscription = null;

		if(isset($_POST["categories"]) && !empty($_POST["categories"])){
			$category = $_POST["category"];
		}

		if(isset($_POST["province"]) && !empty($_POST["province"])){
			$province = $_POST["province"];
		}

		if(isset($_POST["city"]) && !empty($_POST["city"])){
			$province = null;
			$city  = $_POST["city"];
		}

		if(isset($_POST["opened_inscription"]) && !empty($_POST["opened_inscription"])){
			$opened_inscription  = $_POST["opened_inscription"];
		}

		$data['output'] = $this->races_model->search($category,$province,$city,$opened_inscription);
		$this->load->view("json", $data);
	}






}
