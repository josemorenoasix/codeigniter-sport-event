FROM php:7.2-apache

ARG XDEBUG_REMOTE_HOST

RUN apt-get update && apt-get install -y locales && rm -rf /var/lib/apt/lists/* \
	&& localedef -i es_ES -c -f UTF-8 -A /usr/share/locale/locale.alias es_ES.UTF-8
ENV LANG es_ES.utf8

# Enable apache2 rewrite module
RUN a2enmod rewrite

# Install mysqli native library
RUN docker-php-ext-install mysqli

# Install exif native library for image manipulation
RUN docker-php-ext-install exif

# Install pdo mysql driver
RUN docker-php-ext-install pdo_mysql

# Install xdebug native library and enable remote connections
RUN pecl install xdebug-2.7.0alpha1
RUN docker-php-ext-enable xdebug
RUN echo "xdebug.remote_enable=1" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN echo "xdebug.remote_port=9000" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN echo "xdebug.remote_host="${XDEBUG_REMOTE_HOST} >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini