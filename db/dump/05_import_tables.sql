# Grocery Crud not support composite primary keys

ALTER DATABASE `db_app` CHARACTER SET utf8 COLLATE utf8_general_ci;

ALTER TABLE `users`
ADD `firstname` varchar(32) NOT NULL,
ADD `lastname` varchar(64) NOT NULL,
ADD `phone` varchar(16) DEFAULT NULL,
ADD `city` int(4) unsigned DEFAULT NULL,
ADD `address` varchar(64) DEFAULT NULL,
ADD `image_avatar` varchar(255) NOT NULL DEFAULT 'default-avatar.png',
ADD `image_background` varchar(255) NOT NULL DEFAULT 'default-background.jpg',
ADD `birthday` date DEFAULT NULL,
ADD `gender` enum('male','female','other') DEFAULT NULL;

CREATE TABLE IF NOT EXISTS `sports` (
  `sport_id` int(2) unsigned NOT NULL AUTO_INCREMENT,
  `sport_name` varchar(32) NOT NULL,
  `sport_icon` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`sport_id`),
  UNIQUE KEY `name` (`sport_name`)
) ENGINE = InnoDB DEFAULT CHARSET utf8 COLLATE utf8_general_ci;

CREATE TABLE IF NOT EXISTS `categories` (
  `category_id` int(2) unsigned NOT NULL AUTO_INCREMENT,
  `sport_id` int(2) unsigned NOT NULL,
  `category_name` varchar(32) NOT NULL,
  PRIMARY KEY (`category_id`),
  FOREIGN KEY (`sport_id`) REFERENCES `sports`(`sport_id`),
  UNIQUE KEY `name` (`category_name`, `sport_id`)
) ENGINE = InnoDB DEFAULT CHARSET utf8 COLLATE utf8_general_ci;

CREATE TABLE IF NOT EXISTS races (
  `race_id`         int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id`       int(2) unsigned NOT NULL,
  `city`              int(4) unsigned NOT NULL,
  `race_name`       varchar(64) NOT NULL,
  `description`       text DEFAULT NULL,
  `inscription_start_date`  datetime DEFAULT NULL,
  `inscription_finish_date` datetime DEFAULT NULL,
  `date`              datetime     DEFAULT NULL,
  `distance`          int(8)       DEFAULT NULL,
  `poster`            varchar(255) DEFAULT NULL,
  `google_map_url`    varchar(255) DEFAULT NULL,
  `google_map_iframe` text DEFAULT NULL,
  `organizer`         varchar(32) DEFAULT NULL,
  `is_active`         bool NOT NULL DEFAULT 0,
  `rules`             text  DEFAULT NULL,
  PRIMARY KEY (`race_id`),
  FOREIGN KEY (`category_id`) REFERENCES `categories`(`category_id`),
  FOREIGN KEY (`city`) REFERENCES `municipios`(`id`)
) ENGINE = InnoDB DEFAULT CHARSET utf8 COLLATE utf8_general_ci;

CREATE TABLE IF NOT EXISTS `players` (
  `player_id`         int(10) unsigned NOT NULL AUTO_INCREMENT,
  `race_id`         int(10) unsigned NOT NULL,
  `user_id`         int(10) unsigned NOT NULL,
  `number`            int(8) unsigned           DEFAULT NULL,
  `position`          int(4) unsigned           DEFAULT NULL,
  `time`              time                      DEFAULT NULL,
  `registered_at`     timestamp                 DEFAULT CURRENT_TIMESTAMP,
  `modified_at`       timestamp                 DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`player_id`),
  UNIQUE KEY `player_race`(race_id,`user_id`),
  UNIQUE KEY `player_number`(race_id,`number`),
  UNIQUE KEY `player_position`(race_id,`position`),
  UNIQUE KEY `player_time`(race_id,`time`),
  FOREIGN KEY (`user_id`) REFERENCES `users`(`user_id`),
  FOREIGN KEY (race_id) REFERENCES races(`race_id`)
) ENGINE = InnoDB DEFAULT CHARSET utf8 COLLATE utf8_general_ci;

CREATE TABLE IF NOT EXISTS `news` (
  `new_id`   int(10) unsigned NOT NULL AUTO_INCREMENT,
  `autor`        int(10) unsigned NOT NULL,
  `title`        varchar(64) NOT NULL,
  `description`  varchar(128) NOT NULL,
  `body`         text NOT NULL,
  `image`        varchar(255) NOT NULL,
  `created_at`   timestamp DEFAULT CURRENT_TIMESTAMP,
  `modified_at`  timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`new_id`),
  FOREIGN KEY (`autor`) REFERENCES `users`(`user_id`)
) ENGINE = InnoDB DEFAULT CHARSET utf8 COLLATE utf8_general_ci;