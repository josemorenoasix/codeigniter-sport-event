
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (1, 3, 5385, 'odio condimentum id luctus', 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', '2018-02-17 04:25:55', '2018-03-17 04:25:55', '2018-04-01 04:25:55', 10021, 'berlin-blue.png', null, null, 'Devpulse', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (2, 3, 4935, 'ultrices posuere cubilia curae', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', '2018-12-27 04:48:21', '2019-01-27 04:48:21', '2019-02-11 04:48:21', 10014, 'berlin-blue.png', null, null, 'Edgewire', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (3, 4, 321, 'amet sem fusce', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', '2018-10-05 19:51:13', '2018-11-05 19:51:13', '2018-11-20 19:51:13', 5004, 'amsterdam-orange.png', null, null, 'Lazzy', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (4, 4, 7512, 'et ultrices posuere cubilia curae', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', '2018-12-01 12:08:14', '2019-01-01 12:08:14', '2019-01-16 12:08:14', 5007, 'aberdeen-green.png', null, null, 'Tagcat', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (5, 1, 3488, 'at ipsum ac tellus', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.', '2018-04-04 05:59:27', '2018-05-04 05:59:27', '2018-05-19 05:59:27', 40041, 'chicago-red.png', null, null, 'Twitterwire', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (6, 2, 2473, 'suscipit nulla elit', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', '2018-11-14 01:47:54', '2018-12-14 01:47:54', '2018-12-29 01:47:54', 20004, 'chicago-red.png', null, null, 'Janyx', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (7, 4, 4400, 'justo aliquam quis', 'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', '2018-12-17 05:59:52', '2019-01-17 05:59:52', '2019-02-01 05:59:52', 5026, 'berlin-blue.png', null, null, 'Vipe', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (8, 4, 582, 'lorem vitae mattis nibh ligula nec', 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', '2018-05-24 07:38:41', '2018-06-24 07:38:41', '2018-07-09 07:38:41', 5037, 'berlin-blue.png', null, null, 'Skilith', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (9, 4, 4644, 'non mattis pulvinar nulla', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.

Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.', '2018-10-16 07:05:38', '2018-11-16 07:05:38', '2018-12-01 07:05:38', 5032, 'aberdeen-green.png', null, null, 'Jabberbean', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (10, 4, 7175, 'tristique in tempus sit', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', '2018-11-02 07:13:17', '2018-12-02 07:13:17', '2018-12-17 07:13:17', 5017, 'chicago-red.png', null, null, 'Vidoo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (11, 2, 3940, 'duis aliquam convallis nunc proin at', 'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', '2018-07-01 09:40:25', '2018-08-01 09:40:25', '2018-08-16 09:40:25', 20003, 'chicago-red.png', null, null, 'Yozio', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (12, 3, 2468, 'massa tempor convallis nulla neque libero', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', '2018-01-31 02:54:22', '2018-02-28 02:54:22', '2018-03-15 02:54:22', 10016, 'chicago-red.png', null, null, 'Skyndu', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (13, 3, 951, 'et ultrices posuere cubilia curae', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.', '2018-09-02 16:54:41', '2018-10-02 16:54:41', '2018-10-17 16:54:41', 10030, 'aberdeen-green.png', null, null, 'Twinte', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (14, 1, 7356, 'montes nascetur ridiculus', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', '2018-01-27 07:54:34', '2018-02-27 07:54:34', '2018-03-14 07:54:34', 40010, 'chicago-red.png', null, null, 'Jatri', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (15, 3, 6197, 'egestas metus aenean fermentum donec', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', '2018-01-31 14:44:59', '2018-02-28 14:44:59', '2018-03-15 14:44:59', 10031, 'amsterdam-orange.png', null, null, 'Tagopia', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (16, 1, 6879, 'rutrum nulla nunc purus', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.', '2018-05-08 13:38:35', '2018-06-08 13:38:35', '2018-06-23 13:38:35', 40011, 'berlin-blue.png', null, null, 'Edgepulse', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (17, 1, 4757, 'ac diam cras pellentesque volutpat', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', '2018-06-14 01:20:32', '2018-07-14 01:20:32', '2018-07-29 01:20:32', 40033, 'berlin-blue.png', null, null, 'Agivu', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (18, 4, 7780, 'maecenas leo odio', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', '2018-08-07 21:12:48', '2018-09-07 21:12:48', '2018-09-22 21:12:48', 5028, 'amsterdam-orange.png', null, null, 'Shuffledrive', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (19, 3, 7039, 'dis parturient montes nascetur', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.', '2018-08-06 03:21:52', '2018-09-06 03:21:52', '2018-09-21 03:21:52', 10040, 'amsterdam-orange.png', null, null, 'Photobug', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (20, 3, 1199, 'venenatis non sodales sed tincidunt eu', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', '2018-10-06 01:54:57', '2018-11-06 01:54:57', '2018-11-21 01:54:57', 10012, 'berlin-blue.png', null, null, 'Topiclounge', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (21, 4, 3835, 'quisque porta volutpat', 'In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.', '2018-11-05 06:04:13', '2018-12-05 06:04:13', '2018-12-20 06:04:13', 5022, 'aberdeen-green.png', null, null, 'Quaxo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (22, 3, 7889, 'lorem vitae mattis', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', '2018-03-05 00:53:57', '2018-04-05 00:53:57', '2018-04-20 00:53:57', 10016, 'berlin-blue.png', null, null, 'Edgewire', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (23, 3, 692, 'odio condimentum id luctus nec', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.', '2018-08-09 18:38:29', '2018-09-09 18:38:29', '2018-09-24 18:38:29', 10009, 'berlin-blue.png', null, null, 'Brainlounge', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (24, 1, 489, 'eget eros elementum', 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', '2018-11-04 17:40:45', '2018-12-04 17:40:45', '2018-12-19 17:40:45', 40012, 'aberdeen-green.png', null, null, 'Youopia', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (25, 3, 3553, 'gravida nisi at nibh in', 'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', '2018-01-02 16:51:34', '2018-02-02 16:51:34', '2018-02-17 16:51:34', 10024, 'berlin-blue.png', null, null, 'Buzzster', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (26, 4, 1187, 'sed tristique in tempus sit amet', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', '2018-08-09 16:46:16', '2018-09-09 16:46:16', '2018-09-24 16:46:16', 5050, 'aberdeen-green.png', null, null, 'Linktype', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (27, 2, 3502, 'habitasse platea dictumst maecenas ut', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', '2018-06-28 01:05:00', '2018-07-28 01:05:00', '2018-08-12 01:05:00', 20041, 'aberdeen-green.png', null, null, 'Tagfeed', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (28, 2, 6538, 'congue vivamus metus', 'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.

Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', '2018-08-01 23:36:30', '2018-09-01 23:36:30', '2018-09-16 23:36:30', 20037, 'amsterdam-orange.png', null, null, 'Zazio', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (29, 1, 6810, 'rhoncus aliquam lacus morbi quis', 'Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.', '2018-03-25 21:46:34', '2018-04-25 21:46:34', '2018-05-10 21:46:34', 40023, 'chicago-red.png', null, null, 'Demivee', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (30, 4, 1478, 'sed tincidunt eu felis', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', '2018-03-07 21:45:36', '2018-04-07 21:45:36', '2018-04-22 21:45:36', 5019, 'aberdeen-green.png', null, null, 'Pixoboo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (31, 2, 6533, 'consectetuer adipiscing elit proin interdum mauris', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.', '2018-11-27 03:32:12', '2018-12-27 03:32:12', '2019-01-11 03:32:12', 20001, 'aberdeen-green.png', null, null, 'Thoughtmix', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (32, 3, 5973, 'tristique tortor eu pede', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', '2018-11-27 18:45:44', '2018-12-27 18:45:44', '2019-01-11 18:45:44', 10024, 'amsterdam-orange.png', null, null, 'Photobean', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (33, 4, 4876, 'hendrerit at vulputate', 'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', '2018-01-03 15:26:48', '2018-02-03 15:26:48', '2018-02-18 15:26:48', 5021, 'aberdeen-green.png', null, null, 'Wordpedia', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (34, 2, 3556, 'consectetuer adipiscing elit', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', '2018-05-29 07:58:17', '2018-06-29 07:58:17', '2018-07-14 07:58:17', 20043, 'chicago-red.png', null, null, 'Meetz', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (35, 3, 5368, 'vitae nisl aenean lectus pellentesque eget', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.

Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', '2018-10-02 13:27:06', '2018-11-02 13:27:06', '2018-11-17 13:27:06', 10047, 'berlin-blue.png', null, null, 'Rhyzio', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (36, 1, 6410, 'vestibulum ac est lacinia nisi venenatis', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', '2018-07-24 11:10:28', '2018-08-24 11:10:28', '2018-09-08 11:10:28', 40024, 'amsterdam-orange.png', null, null, 'Janyx', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (37, 2, 6532, 'nisi volutpat eleifend donec ut', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', '2018-04-15 03:36:22', '2018-05-15 03:36:22', '2018-05-30 03:36:22', 20049, 'aberdeen-green.png', null, null, 'Browseblab', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (38, 2, 8101, 'magna ac consequat metus sapien', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.', '2018-04-22 00:44:03', '2018-05-22 00:44:03', '2018-06-06 00:44:03', 20013, 'amsterdam-orange.png', null, null, 'Devify', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (39, 3, 3331, 'et eros vestibulum ac', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', '2018-02-13 05:33:23', '2018-03-13 05:33:23', '2018-03-28 05:33:23', 10030, 'aberdeen-green.png', null, null, 'Zoombeat', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (40, 4, 787, 'turpis adipiscing lorem', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', '2018-03-08 11:51:28', '2018-04-08 11:51:28', '2018-04-23 11:51:28', 5024, 'amsterdam-orange.png', null, null, 'Zooveo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (41, 1, 5631, 'non pretium quis', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', '2018-03-11 08:50:01', '2018-04-11 08:50:01', '2018-04-26 08:50:01', 40042, 'chicago-red.png', null, null, 'Eidel', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (42, 4, 7514, 'interdum eu tincidunt in leo maecenas', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', '2018-11-06 13:48:37', '2018-12-06 13:48:37', '2018-12-21 13:48:37', 5004, 'berlin-blue.png', null, null, 'Skyndu', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (43, 4, 183, 'maecenas leo odio', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', '2018-02-24 13:19:03', '2018-03-24 13:19:03', '2018-04-08 13:19:03', 5002, 'chicago-red.png', null, null, 'Edgeify', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (44, 3, 1279, 'morbi sem mauris laoreet ut', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.', '2018-06-27 06:28:42', '2018-07-27 06:28:42', '2018-08-11 06:28:42', 10002, 'berlin-blue.png', null, null, 'Roombo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (45, 3, 7911, 'eget eros elementum', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', '2018-04-10 20:11:20', '2018-05-10 20:11:20', '2018-05-25 20:11:20', 10014, 'berlin-blue.png', null, null, 'Trudeo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (46, 2, 1519, 'rhoncus aliquet pulvinar sed nisl', 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', '2018-06-29 13:38:27', '2018-07-29 13:38:27', '2018-08-13 13:38:27', 20046, 'amsterdam-orange.png', null, null, 'Avamba', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (47, 3, 3216, 'orci nullam molestie nibh', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', '2018-05-30 03:48:27', '2018-06-30 03:48:27', '2018-07-15 03:48:27', 10034, 'amsterdam-orange.png', null, null, 'Abata', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (48, 1, 1973, 'ipsum primis in faucibus orci luctus', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.', '2018-01-18 12:00:59', '2018-02-18 12:00:59', '2018-03-05 12:00:59', 40031, 'chicago-red.png', null, null, 'Divavu', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (49, 1, 1553, 'justo eu massa donec', 'In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.', '2018-07-31 18:12:14', '2018-08-31 18:12:14', '2018-09-15 18:12:14', 40034, 'chicago-red.png', null, null, 'Linkbridge', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (50, 2, 7566, 'nibh fusce lacus purus aliquet', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', '2018-03-04 10:49:58', '2018-04-04 10:49:58', '2018-04-19 10:49:58', 20013, 'chicago-red.png', null, null, 'Fivespan', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (51, 3, 227, 'id consequat in consequat ut', 'Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', '2018-09-18 20:53:36', '2018-10-18 20:53:36', '2018-11-02 20:53:36', 10019, 'berlin-blue.png', null, null, 'Mydeo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (52, 2, 7658, 'libero rutrum ac lobortis vel', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', '2018-09-28 00:35:16', '2018-10-28 00:35:16', '2018-11-12 00:35:16', 20041, 'amsterdam-orange.png', null, null, 'Jaxnation', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (53, 3, 2583, 'phasellus in felis donec', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', '2018-02-05 02:33:01', '2018-03-05 02:33:01', '2018-03-20 02:33:01', 10035, 'chicago-red.png', null, null, 'Zooxo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (54, 4, 5144, 'sed magna at nunc', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.

Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', '2018-10-01 15:06:39', '2018-11-01 15:06:39', '2018-11-16 15:06:39', 5015, 'amsterdam-orange.png', null, null, 'Photojam', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (55, 2, 7035, 'odio donec vitae nisi', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.', '2018-07-31 00:02:10', '2018-08-31 00:02:10', '2018-09-15 00:02:10', 20033, 'amsterdam-orange.png', null, null, 'Bubblemix', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (56, 2, 4425, 'tempor convallis nulla neque', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', '2018-09-23 14:08:53', '2018-10-23 14:08:53', '2018-11-07 14:08:53', 20009, 'chicago-red.png', null, null, 'Flipbug', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (57, 4, 4694, 'varius integer ac leo', 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.', '2018-03-03 07:45:27', '2018-04-03 07:45:27', '2018-04-18 07:45:27', 5015, 'berlin-blue.png', null, null, 'Lazz', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (58, 4, 3788, 'adipiscing lorem vitae mattis', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', '2018-09-15 08:40:16', '2018-10-15 08:40:16', '2018-10-30 08:40:16', 5000, 'chicago-red.png', null, null, 'Dynazzy', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (59, 2, 4425, 'faucibus orci luctus et ultrices posuere', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', '2018-03-18 23:22:41', '2018-04-18 23:22:41', '2018-05-03 23:22:41', 20045, 'berlin-blue.png', null, null, 'Devify', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (60, 2, 4030, 'id ligula suspendisse ornare consequat lectus', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', '2018-12-05 17:48:42', '2019-01-05 17:48:42', '2019-01-20 17:48:42', 20048, 'berlin-blue.png', null, null, 'Tambee', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (61, 1, 799, 'nec molestie sed justo', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.', '2018-03-03 00:45:10', '2018-04-03 00:45:10', '2018-04-18 00:45:10', 40002, 'aberdeen-green.png', null, null, 'Topdrive', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (62, 4, 6234, 'nisl aenean lectus pellentesque eget', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', '2018-08-08 03:18:34', '2018-09-08 03:18:34', '2018-09-23 03:18:34', 5044, 'chicago-red.png', null, null, 'Zoozzy', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (63, 3, 2, 'ut rhoncus aliquet pulvinar sed', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', '2018-02-05 00:20:27', '2018-03-05 00:20:27', '2018-03-20 00:20:27', 10002, 'berlin-blue.png', null, null, 'Zoombox', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (64, 4, 5620, 'nec euismod scelerisque', 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.

Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.', '2018-09-22 08:49:59', '2018-10-22 08:49:59', '2018-11-06 08:49:59', 5014, 'berlin-blue.png', null, null, 'Kwideo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (65, 4, 4174, 'risus dapibus augue', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', '2018-12-10 01:28:17', '2019-01-10 01:28:17', '2019-01-25 01:28:17', 5022, 'aberdeen-green.png', null, null, 'Meedoo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (66, 3, 2150, 'erat vestibulum sed magna at nunc', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', '2018-02-23 22:53:28', '2018-03-23 22:53:28', '2018-04-07 22:53:28', 10039, 'berlin-blue.png', null, null, 'Edgeclub', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (67, 1, 2666, 'cursus id turpis', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', '2018-03-08 21:07:38', '2018-04-08 21:07:38', '2018-04-23 21:07:38', 40020, 'aberdeen-green.png', null, null, 'Yoveo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (68, 4, 1776, 'nunc nisl duis bibendum felis', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', '2018-09-26 19:04:59', '2018-10-26 19:04:59', '2018-11-10 19:04:59', 5043, 'berlin-blue.png', null, null, 'Skipstorm', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (69, 3, 4150, 'rutrum rutrum neque aenean auctor', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', '2018-04-07 15:08:52', '2018-05-07 15:08:52', '2018-05-22 15:08:52', 10003, 'chicago-red.png', null, null, 'Muxo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (70, 2, 3885, 'nec nisi volutpat eleifend donec ut', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', '2018-07-19 05:20:18', '2018-08-19 05:20:18', '2018-09-03 05:20:18', 20009, 'aberdeen-green.png', null, null, 'Eazzy', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (71, 1, 5922, 'vestibulum proin eu mi nulla', 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.

Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.', '2018-08-14 11:56:58', '2018-09-14 11:56:58', '2018-09-29 11:56:58', 40036, 'aberdeen-green.png', null, null, 'Rhynyx', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (72, 3, 4398, 'diam id ornare imperdiet sapien', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', '2018-07-25 18:30:12', '2018-08-25 18:30:12', '2018-09-09 18:30:12', 10026, 'aberdeen-green.png', null, null, 'Thoughtbeat', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (73, 4, 5614, 'blandit mi in porttitor pede', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.', '2018-01-07 19:29:58', '2018-02-07 19:29:58', '2018-02-22 19:29:58', 5017, 'amsterdam-orange.png', null, null, 'Meedoo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (74, 1, 398, 'ipsum primis in faucibus orci luctus', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.', '2018-12-23 09:36:53', '2019-01-23 09:36:53', '2019-02-07 09:36:53', 40049, 'berlin-blue.png', null, null, 'Kimia', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (75, 4, 6537, 'erat vestibulum sed', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', '2018-07-28 12:17:57', '2018-08-28 12:17:57', '2018-09-12 12:17:57', 5035, 'chicago-red.png', null, null, 'Edgeclub', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (76, 1, 2249, 'morbi ut odio cras mi', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', '2018-09-17 08:41:02', '2018-10-17 08:41:02', '2018-11-01 08:41:02', 40019, 'amsterdam-orange.png', null, null, 'Jetpulse', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (77, 3, 498, 'sapien placerat ante nulla justo aliquam', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', '2018-11-14 20:53:39', '2018-12-14 20:53:39', '2018-12-29 20:53:39', 10048, 'amsterdam-orange.png', null, null, 'Babbleopia', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (78, 1, 2027, 'lectus pellentesque at nulla', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', '2018-02-02 03:09:36', '2018-03-02 03:09:36', '2018-03-17 03:09:36', 40005, 'berlin-blue.png', null, null, 'Browseblab', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (79, 2, 6140, 'cursus id turpis integer aliquet', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.', '2018-11-06 14:16:54', '2018-12-06 14:16:54', '2018-12-21 14:16:54', 20044, 'chicago-red.png', null, null, 'Fanoodle', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (80, 1, 3838, 'aliquet pulvinar sed nisl', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', '2018-01-31 03:11:10', '2018-02-28 03:11:10', '2018-03-15 03:11:10', 40033, 'chicago-red.png', null, null, 'Roodel', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (81, 4, 3200, 'vivamus in felis', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', '2018-06-06 13:39:26', '2018-07-06 13:39:26', '2018-07-21 13:39:26', 5040, 'aberdeen-green.png', null, null, 'Flashspan', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (82, 2, 4482, 'justo maecenas rhoncus aliquam lacus morbi', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', '2018-10-13 09:24:11', '2018-11-13 09:24:11', '2018-11-28 09:24:11', 20032, 'aberdeen-green.png', null, null, 'Skipfire', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (83, 2, 868, 'a nibh in', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', '2018-09-03 21:36:26', '2018-10-03 21:36:26', '2018-10-18 21:36:26', 20039, 'chicago-red.png', null, null, 'Kwimbee', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (84, 1, 6938, 'ante ipsum primis', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', '2018-11-24 10:37:05', '2018-12-24 10:37:05', '2019-01-08 10:37:05', 40045, 'amsterdam-orange.png', null, null, 'Thoughtsphere', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (85, 1, 4258, 'luctus ultricies eu nibh quisque id', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.', '2018-07-23 10:20:55', '2018-08-23 10:20:55', '2018-09-07 10:20:55', 40049, 'aberdeen-green.png', null, null, 'Centimia', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (86, 2, 5714, 'gravida nisi at nibh in', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', '2018-01-15 14:44:00', '2018-02-15 14:44:00', '2018-03-02 14:44:00', 20007, 'chicago-red.png', null, null, 'Yozio', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (87, 3, 5094, 'viverra diam vitae quam suspendisse', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', '2018-04-16 21:51:02', '2018-05-16 21:51:02', '2018-05-31 21:51:02', 10019, 'amsterdam-orange.png', null, null, 'Mybuzz', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (88, 2, 1069, 'proin risus praesent lectus vestibulum', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.', '2018-01-06 21:34:56', '2018-02-06 21:34:56', '2018-02-21 21:34:56', 20017, 'aberdeen-green.png', null, null, 'Oyoba', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (89, 1, 7924, 'eget orci vehicula', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.', '2018-04-01 20:26:31', '2018-05-01 20:26:31', '2018-05-16 20:26:31', 40042, 'amsterdam-orange.png', null, null, 'Browsetype', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (90, 4, 3057, 'id justo sit amet sapien', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.', '2018-06-03 12:07:57', '2018-07-03 12:07:57', '2018-07-18 12:07:57', 5020, 'amsterdam-orange.png', null, null, 'Voolia', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (91, 4, 4798, 'interdum mauris ullamcorper purus sit', 'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', '2018-06-08 13:38:47', '2018-07-08 13:38:47', '2018-07-23 13:38:47', 5006, 'chicago-red.png', null, null, 'Fivechat', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (92, 4, 3740, 'velit nec nisi vulputate nonummy maecenas', 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.

Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.', '2018-02-03 01:09:52', '2018-03-03 01:09:52', '2018-03-18 01:09:52', 5035, 'amsterdam-orange.png', null, null, 'Fivespan', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (93, 4, 330, 'felis fusce posuere felis sed', 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.

Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', '2018-02-01 11:43:22', '2018-03-01 11:43:22', '2018-03-16 11:43:22', 5030, 'amsterdam-orange.png', null, null, 'Photojam', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (94, 3, 7414, 'rutrum neque aenean auctor gravida sem', 'Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', '2018-04-10 15:22:59', '2018-05-10 15:22:59', '2018-05-25 15:22:59', 10043, 'chicago-red.png', null, null, 'Tagcat', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (95, 1, 853, 'sed accumsan felis', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', '2018-10-03 14:17:55', '2018-11-03 14:17:55', '2018-11-18 14:17:55', 40047, 'chicago-red.png', null, null, 'Skinte', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (96, 3, 958, 'pharetra magna vestibulum', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.', '2018-08-29 15:56:49', '2018-09-29 15:56:49', '2018-10-14 15:56:49', 10043, 'amsterdam-orange.png', null, null, 'Skimia', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (97, 1, 1747, 'metus vitae ipsum', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', '2018-10-18 01:25:30', '2018-11-18 01:25:30', '2018-12-03 01:25:30', 40008, 'aberdeen-green.png', null, null, 'Mydeo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (98, 2, 4845, 'et magnis dis parturient montes', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', '2018-09-25 23:20:02', '2018-10-25 23:20:02', '2018-11-09 23:20:02', 20030, 'aberdeen-green.png', null, null, 'Nlounge', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (99, 3, 3880, 'bibendum felis sed interdum', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', '2018-09-21 22:27:17', '2018-10-21 22:27:17', '2018-11-05 22:27:17', 10030, 'amsterdam-orange.png', null, null, 'Jayo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (100, 4, 5129, 'donec ut mauris', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', '2018-10-06 19:11:21', '2018-11-06 19:11:21', '2018-11-21 19:11:21', 5003, 'aberdeen-green.png', null, null, 'Mita', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (101, 1, 550, 'urna ut tellus nulla ut erat', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', '2018-11-15 21:25:04', '2018-12-15 21:25:04', '2018-12-30 21:25:04', 40039, 'amsterdam-orange.png', null, null, 'Flashspan', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (102, 1, 2677, 'cubilia curae donec pharetra magna', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', '2018-04-06 01:44:46', '2018-05-06 01:44:46', '2018-05-21 01:44:46', 40033, 'amsterdam-orange.png', null, null, 'Meedoo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (103, 1, 782, 'nibh in lectus pellentesque at', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', '2018-04-03 02:38:38', '2018-05-03 02:38:38', '2018-05-18 02:38:38', 40042, 'amsterdam-orange.png', null, null, 'Topicblab', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (104, 3, 5627, 'eget tempus vel pede morbi', 'Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.', '2018-01-06 14:10:17', '2018-02-06 14:10:17', '2018-02-21 14:10:17', 10041, 'berlin-blue.png', null, null, 'Realfire', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (105, 1, 3986, 'sed accumsan felis ut at', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.', '2018-08-23 03:13:03', '2018-09-23 03:13:03', '2018-10-08 03:13:03', 40044, 'aberdeen-green.png', null, null, 'Kaymbo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (106, 3, 4142, 'pede venenatis non', 'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', '2018-06-28 22:58:11', '2018-07-28 22:58:11', '2018-08-12 22:58:11', 10021, 'aberdeen-green.png', null, null, 'Jaxspan', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (107, 3, 1797, 'dui nec nisi volutpat eleifend donec', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', '2018-09-05 06:33:48', '2018-10-05 06:33:48', '2018-10-20 06:33:48', 10016, 'aberdeen-green.png', null, null, 'Wikibox', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (108, 2, 7325, 'turpis integer aliquet massa', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.', '2018-01-15 18:53:57', '2018-02-15 18:53:57', '2018-03-02 18:53:57', 20050, 'chicago-red.png', null, null, 'Flashpoint', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (109, 3, 3845, 'sed augue aliquam erat', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', '2018-07-09 09:02:29', '2018-08-09 09:02:29', '2018-08-24 09:02:29', 10009, 'berlin-blue.png', null, null, 'Skimia', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (110, 4, 4027, 'justo maecenas rhoncus aliquam lacus morbi', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', '2018-10-30 00:14:10', '2018-11-30 00:14:10', '2018-12-15 00:14:10', 5042, 'chicago-red.png', null, null, 'Dynabox', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (111, 1, 845, 'sed justo pellentesque', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', '2018-11-28 05:58:20', '2018-12-28 05:58:20', '2019-01-12 05:58:20', 40020, 'berlin-blue.png', null, null, 'Lazzy', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (112, 1, 5179, 'aliquam convallis nunc', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', '2018-11-10 03:33:33', '2018-12-10 03:33:33', '2018-12-25 03:33:33', 40042, 'aberdeen-green.png', null, null, 'Flashspan', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (113, 4, 2230, 'habitasse platea dictumst maecenas ut massa', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', '2018-03-26 17:12:25', '2018-04-26 17:12:25', '2018-05-11 17:12:25', 5009, 'amsterdam-orange.png', null, null, 'Fadeo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (114, 4, 6095, 'at nulla suspendisse potenti cras', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', '2018-03-25 19:25:44', '2018-04-25 19:25:44', '2018-05-10 19:25:44', 5050, 'berlin-blue.png', null, null, 'Ailane', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (115, 3, 7666, 'vel augue vestibulum ante', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', '2018-10-20 07:57:50', '2018-11-20 07:57:50', '2018-12-05 07:57:50', 10042, 'berlin-blue.png', null, null, 'Voonix', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (116, 3, 948, 'vivamus vestibulum sagittis sapien', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', '2018-05-05 06:47:50', '2018-06-05 06:47:50', '2018-06-20 06:47:50', 10036, 'chicago-red.png', null, null, 'Jamia', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (117, 2, 2500, 'erat quisque erat eros viverra eget', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.', '2018-11-05 20:21:00', '2018-12-05 20:21:00', '2018-12-20 20:21:00', 20035, 'aberdeen-green.png', null, null, 'Miboo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (118, 1, 7870, 'nullam orci pede venenatis non sodales', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', '2018-08-11 19:14:02', '2018-09-11 19:14:02', '2018-09-26 19:14:02', 40021, 'berlin-blue.png', null, null, 'Oloo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (119, 3, 1239, 'pharetra magna ac consequat metus', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', '2018-05-07 00:51:48', '2018-06-07 00:51:48', '2018-06-22 00:51:48', 10021, 'berlin-blue.png', null, null, 'Tagfeed', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (120, 3, 7629, 'nisi venenatis tristique fusce congue', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.', '2018-02-06 18:55:54', '2018-03-06 18:55:54', '2018-03-21 18:55:54', 10001, 'aberdeen-green.png', null, null, 'Twitterlist', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (121, 1, 4307, 'nulla ut erat id mauris vulputate', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', '2018-09-26 19:01:46', '2018-10-26 19:01:46', '2018-11-10 19:01:46', 40008, 'amsterdam-orange.png', null, null, 'Viva', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (122, 4, 6223, 'adipiscing elit proin interdum mauris non', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.', '2018-11-11 17:48:03', '2018-12-11 17:48:03', '2018-12-26 17:48:03', 5024, 'amsterdam-orange.png', null, null, 'Wikivu', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (123, 4, 5954, 'et magnis dis parturient', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', '2018-01-28 11:07:19', '2018-02-28 11:07:19', '2018-03-15 11:07:19', 5037, 'amsterdam-orange.png', null, null, 'Devify', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (124, 2, 2522, 'at diam nam tristique', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.', '2018-10-18 01:46:20', '2018-11-18 01:46:20', '2018-12-03 01:46:20', 20050, 'amsterdam-orange.png', null, null, 'Viva', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (125, 1, 3249, 'viverra pede ac', 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.

Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', '2018-07-07 15:58:30', '2018-08-07 15:58:30', '2018-08-22 15:58:30', 40034, 'berlin-blue.png', null, null, 'Yakidoo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (126, 3, 7584, 'mi nulla ac enim in', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', '2018-11-20 22:56:29', '2018-12-20 22:56:29', '2019-01-04 22:56:29', 10021, 'chicago-red.png', null, null, 'Leenti', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (127, 2, 5478, 'volutpat quam pede', 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', '2018-10-09 07:53:19', '2018-11-09 07:53:19', '2018-11-24 07:53:19', 20027, 'berlin-blue.png', null, null, 'Leexo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (128, 3, 112, 'augue vestibulum rutrum rutrum neque aenean', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', '2018-08-30 23:36:20', '2018-09-30 23:36:20', '2018-10-15 23:36:20', 10040, 'amsterdam-orange.png', null, null, 'Tazz', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (129, 4, 2298, 'arcu sed augue aliquam erat', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', '2018-04-03 04:06:41', '2018-05-03 04:06:41', '2018-05-18 04:06:41', 5029, 'berlin-blue.png', null, null, 'Blogtag', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (130, 1, 3120, 'ipsum primis in', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.', '2018-07-31 19:56:54', '2018-08-31 19:56:54', '2018-09-15 19:56:54', 40042, 'aberdeen-green.png', null, null, 'Edgeblab', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (131, 1, 5533, 'non mi integer ac neque', 'In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', '2018-11-11 09:52:42', '2018-12-11 09:52:42', '2018-12-26 09:52:42', 40032, 'chicago-red.png', null, null, 'Youspan', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (132, 1, 4304, 'odio justo sollicitudin ut suscipit a', 'Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.', '2018-10-08 06:09:19', '2018-11-08 06:09:19', '2018-11-23 06:09:19', 40050, 'aberdeen-green.png', null, null, 'Yodoo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (133, 2, 5229, 'nec molestie sed justo', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', '2018-02-23 07:21:35', '2018-03-23 07:21:35', '2018-04-07 07:21:35', 20016, 'chicago-red.png', null, null, 'Twimm', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (134, 1, 2915, 'curae mauris viverra diam vitae quam', 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', '2018-03-22 04:49:12', '2018-04-22 04:49:12', '2018-05-07 04:49:12', 40039, 'aberdeen-green.png', null, null, 'Meevee', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (135, 4, 272, 'scelerisque quam turpis adipiscing', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.', '2018-03-17 10:04:42', '2018-04-17 10:04:42', '2018-05-02 10:04:42', 5023, 'aberdeen-green.png', null, null, 'Wikizz', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (136, 4, 358, 'elit ac nulla sed vel enim', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', '2018-09-02 04:22:40', '2018-10-02 04:22:40', '2018-10-17 04:22:40', 5012, 'aberdeen-green.png', null, null, 'Thoughtstorm', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (137, 1, 4247, 'tristique est et tempus semper est', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', '2018-08-30 06:02:42', '2018-09-30 06:02:42', '2018-10-15 06:02:42', 40001, 'amsterdam-orange.png', null, null, 'Skyble', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (138, 3, 6293, 'vulputate vitae nisl', 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', '2018-05-19 04:23:11', '2018-06-19 04:23:11', '2018-07-04 04:23:11', 10033, 'chicago-red.png', null, null, 'Oyoloo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (139, 1, 5440, 'natoque penatibus et magnis', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', '2018-09-01 10:55:59', '2018-10-01 10:55:59', '2018-10-16 10:55:59', 40023, 'amsterdam-orange.png', null, null, 'Blogtags', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (140, 1, 400, 'fusce posuere felis sed lacus', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', '2018-07-07 19:10:54', '2018-08-07 19:10:54', '2018-08-22 19:10:54', 40050, 'berlin-blue.png', null, null, 'Skynoodle', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (141, 2, 3715, 'non mi integer ac neque duis', 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', '2018-05-18 11:16:24', '2018-06-18 11:16:24', '2018-07-03 11:16:24', 20041, 'berlin-blue.png', null, null, 'Blogtag', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (142, 1, 5894, 'ipsum primis in', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', '2018-07-16 06:10:38', '2018-08-16 06:10:38', '2018-08-31 06:10:38', 40005, 'amsterdam-orange.png', null, null, 'Yabox', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (143, 4, 3101, 'duis bibendum morbi non', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', '2018-02-07 17:17:33', '2018-03-07 17:17:33', '2018-03-22 17:17:33', 5031, 'aberdeen-green.png', null, null, 'Fanoodle', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (144, 4, 5917, 'faucibus orci luctus', 'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', '2018-05-15 19:03:56', '2018-06-15 19:03:56', '2018-06-30 19:03:56', 5050, 'berlin-blue.png', null, null, 'Livefish', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (145, 4, 691, 'integer non velit donec diam neque', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', '2018-01-23 10:07:55', '2018-02-23 10:07:55', '2018-03-10 10:07:55', 5030, 'chicago-red.png', null, null, 'Linktype', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (146, 3, 3193, 'phasellus id sapien', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', '2018-02-10 11:09:25', '2018-03-10 11:09:25', '2018-03-25 11:09:25', 10018, 'berlin-blue.png', null, null, 'Youspan', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (147, 3, 1141, 'aliquet pulvinar sed', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', '2018-10-25 23:30:08', '2018-11-25 23:30:08', '2018-12-10 23:30:08', 10036, 'amsterdam-orange.png', null, null, 'Meevee', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (148, 4, 462, 'dictumst etiam faucibus cursus', 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.', '2018-08-07 21:20:05', '2018-09-07 21:20:05', '2018-09-22 21:20:05', 5038, 'berlin-blue.png', null, null, 'Babbleset', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (149, 3, 6741, 'odio condimentum id luctus', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.', '2018-05-20 04:42:37', '2018-06-20 04:42:37', '2018-07-05 04:42:37', 10006, 'chicago-red.png', null, null, 'Quatz', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (150, 2, 4470, 'nisl aenean lectus pellentesque', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.', '2018-01-14 02:37:16', '2018-02-14 02:37:16', '2018-03-01 02:37:16', 20020, 'amsterdam-orange.png', null, null, 'Photobug', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (151, 3, 4604, 'nunc vestibulum ante ipsum', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', '2018-10-18 20:55:22', '2018-11-18 20:55:22', '2018-12-03 20:55:22', 10016, 'chicago-red.png', null, null, 'Eare', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (152, 2, 348, 'purus eu magna vulputate', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.

Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.

Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', '2018-05-29 09:53:48', '2018-06-29 09:53:48', '2018-07-14 09:53:48', 20022, 'aberdeen-green.png', null, null, 'Buzzster', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (153, 3, 1480, 'convallis duis consequat', 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.

Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', '2018-10-31 15:17:22', '2018-11-30 15:17:22', '2018-12-15 15:17:22', 10029, 'amsterdam-orange.png', null, null, 'Kwilith', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (154, 2, 7233, 'ante nulla justo', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', '2018-02-12 01:28:45', '2018-03-12 01:28:45', '2018-03-27 01:28:45', 20030, 'berlin-blue.png', null, null, 'Kaymbo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (155, 3, 3662, 'justo nec condimentum neque sapien placerat', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', '2018-07-30 08:38:47', '2018-08-30 08:38:47', '2018-09-14 08:38:47', 10031, 'berlin-blue.png', null, null, 'Tagchat', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (156, 4, 1855, 'blandit non interdum in ante vestibulum', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.', '2018-09-17 09:59:06', '2018-10-17 09:59:06', '2018-11-01 09:59:06', 5010, 'chicago-red.png', null, null, 'Thoughtsphere', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (157, 3, 6048, 'neque vestibulum eget vulputate ut ultrices', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.', '2018-12-22 16:11:15', '2019-01-22 16:11:15', '2019-02-06 16:11:15', 10014, 'aberdeen-green.png', null, null, 'Gabvine', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (158, 2, 7235, 'at nunc commodo placerat', 'Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', '2018-06-17 09:03:50', '2018-07-17 09:03:50', '2018-08-01 09:03:50', 20013, 'aberdeen-green.png', null, null, 'Wordify', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (159, 2, 6193, 'libero convallis eget eleifend luctus', 'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.', '2018-04-30 05:55:15', '2018-05-30 05:55:15', '2018-06-14 05:55:15', 20006, 'chicago-red.png', null, null, 'Voonix', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (160, 2, 343, 'primis in faucibus', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.', '2018-07-24 17:42:43', '2018-08-24 17:42:43', '2018-09-08 17:42:43', 20008, 'chicago-red.png', null, null, 'Voonte', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (161, 3, 7075, 'aliquet at feugiat non pretium', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', '2018-05-24 16:52:59', '2018-06-24 16:52:59', '2018-07-09 16:52:59', 10030, 'berlin-blue.png', null, null, 'Centidel', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (162, 4, 1727, 'vulputate nonummy maecenas tincidunt', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', '2018-03-30 09:14:51', '2018-04-30 09:14:51', '2018-05-15 09:14:51', 5005, 'aberdeen-green.png', null, null, 'Mycat', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (163, 2, 4026, 'accumsan tellus nisi eu orci', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', '2018-02-28 07:44:52', '2018-03-28 07:44:52', '2018-04-12 07:44:52', 20032, 'chicago-red.png', null, null, 'Roodel', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (164, 4, 3654, 'dolor sit amet consectetuer adipiscing', 'Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', '2018-08-10 06:32:50', '2018-09-10 06:32:50', '2018-09-25 06:32:50', 5036, 'aberdeen-green.png', null, null, 'Skaboo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (165, 3, 4883, 'sem mauris laoreet ut rhoncus aliquet', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.', '2018-01-22 10:18:56', '2018-02-22 10:18:56', '2018-03-09 10:18:56', 10029, 'amsterdam-orange.png', null, null, 'Voomm', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (166, 1, 4678, 'viverra diam vitae quam', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', '2018-05-13 12:50:02', '2018-06-13 12:50:02', '2018-06-28 12:50:02', 40021, 'chicago-red.png', null, null, 'Mybuzz', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (167, 1, 6700, 'curae nulla dapibus dolor vel', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', '2018-04-08 08:09:19', '2018-05-08 08:09:19', '2018-05-23 08:09:19', 40013, 'chicago-red.png', null, null, 'Livefish', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (168, 3, 7896, 'at velit vivamus vel', 'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', '2018-01-16 14:14:46', '2018-02-16 14:14:46', '2018-03-03 14:14:46', 10028, 'berlin-blue.png', null, null, 'Eamia', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (169, 3, 7488, 'amet consectetuer adipiscing elit proin interdum', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', '2018-06-27 19:42:38', '2018-07-27 19:42:38', '2018-08-11 19:42:38', 10005, 'amsterdam-orange.png', null, null, 'Skalith', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (170, 1, 3373, 'sit amet erat nulla tempus', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', '2018-06-30 21:47:22', '2018-07-30 21:47:22', '2018-08-14 21:47:22', 40032, 'chicago-red.png', null, null, 'Skalith', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (171, 3, 7670, 'in eleifend quam a odio', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.', '2018-06-10 06:07:27', '2018-07-10 06:07:27', '2018-07-25 06:07:27', 10001, 'amsterdam-orange.png', null, null, 'Viva', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (172, 2, 2624, 'porttitor lorem id ligula', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', '2018-01-14 14:10:31', '2018-02-14 14:10:31', '2018-03-01 14:10:31', 20028, 'chicago-red.png', null, null, 'Jabberstorm', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (173, 1, 6982, 'ultrices posuere cubilia curae', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', '2018-02-18 22:23:25', '2018-03-18 22:23:25', '2018-04-02 22:23:25', 40017, 'chicago-red.png', null, null, 'Blognation', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (174, 2, 602, 'aliquet pulvinar sed nisl', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', '2018-04-12 11:11:35', '2018-05-12 11:11:35', '2018-05-27 11:11:35', 20039, 'aberdeen-green.png', null, null, 'Devbug', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (175, 2, 1343, 'in faucibus orci', 'Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', '2018-03-26 06:01:02', '2018-04-26 06:01:02', '2018-05-11 06:01:02', 20003, 'berlin-blue.png', null, null, 'Vidoo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (176, 4, 1831, 'eget semper rutrum nulla nunc purus', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.', '2018-12-03 13:32:50', '2019-01-03 13:32:50', '2019-01-18 13:32:50', 5049, 'berlin-blue.png', null, null, 'Topiclounge', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (177, 4, 6245, 'augue vestibulum rutrum rutrum neque', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', '2018-01-10 23:56:50', '2018-02-10 23:56:50', '2018-02-25 23:56:50', 5027, 'aberdeen-green.png', null, null, 'Vinte', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (178, 2, 116, 'sit amet diam in magna', 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', '2018-09-26 10:51:59', '2018-10-26 10:51:59', '2018-11-10 10:51:59', 20003, 'aberdeen-green.png', null, null, 'Gabtype', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (179, 3, 4997, 'lobortis est phasellus', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', '2018-12-19 04:50:23', '2019-01-19 04:50:23', '2019-02-03 04:50:23', 10008, 'amsterdam-orange.png', null, null, 'Mudo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (180, 3, 3539, 'rhoncus sed vestibulum sit amet', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.', '2018-04-05 10:46:50', '2018-05-05 10:46:50', '2018-05-20 10:46:50', 10020, 'amsterdam-orange.png', null, null, 'Flashpoint', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (181, 4, 2446, 'eleifend quam a odio', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', '2018-08-19 21:32:33', '2018-09-19 21:32:33', '2018-10-04 21:32:33', 5032, 'berlin-blue.png', null, null, 'Photospace', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (182, 4, 3539, 'ligula sit amet eleifend pede', 'In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', '2018-02-17 21:08:28', '2018-03-17 21:08:28', '2018-04-01 21:08:28', 5032, 'berlin-blue.png', null, null, 'Centimia', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (183, 1, 45, 'sagittis sapien cum sociis natoque penatibus', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', '2018-11-14 16:49:47', '2018-12-14 16:49:47', '2018-12-29 16:49:47', 40049, 'chicago-red.png', null, null, 'Trunyx', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (184, 3, 3506, 'volutpat erat quisque erat eros viverra', 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.', '2018-04-16 06:07:23', '2018-05-16 06:07:23', '2018-05-31 06:07:23', 10032, 'amsterdam-orange.png', null, null, 'Topicware', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (185, 2, 7882, 'orci pede venenatis', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.', '2018-05-06 07:45:46', '2018-06-06 07:45:46', '2018-06-21 07:45:46', 20021, 'amsterdam-orange.png', null, null, 'Eadel', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (186, 2, 3819, 'massa donec dapibus duis', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', '2018-06-22 06:26:33', '2018-07-22 06:26:33', '2018-08-06 06:26:33', 20047, 'amsterdam-orange.png', null, null, 'Twitterlist', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (187, 1, 7195, 'est quam pharetra', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', '2018-08-02 07:29:39', '2018-09-02 07:29:39', '2018-09-17 07:29:39', 40025, 'amsterdam-orange.png', null, null, 'Flashset', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (188, 2, 1859, 'non velit nec nisi', 'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.

Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', '2018-08-07 14:06:28', '2018-09-07 14:06:28', '2018-09-22 14:06:28', 20017, 'berlin-blue.png', null, null, 'Livepath', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (189, 3, 7345, 'libero non mattis', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.', '2018-04-06 00:15:45', '2018-05-06 00:15:45', '2018-05-21 00:15:45', 10033, 'berlin-blue.png', null, null, 'Thoughtsphere', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (190, 2, 6502, 'dis parturient montes nascetur', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.', '2018-01-30 12:41:16', '2018-02-28 12:41:16', '2018-03-15 12:41:16', 20033, 'chicago-red.png', null, null, 'Nlounge', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (191, 2, 1185, 'mattis pulvinar nulla pede ullamcorper augue', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', '2018-07-25 16:03:37', '2018-08-25 16:03:37', '2018-09-09 16:03:37', 20017, 'chicago-red.png', null, null, 'Kazio', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (192, 4, 4904, 'vestibulum quam sapien varius', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.', '2018-08-22 07:49:46', '2018-09-22 07:49:46', '2018-10-07 07:49:46', 5019, 'amsterdam-orange.png', null, null, 'Quimba', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (193, 4, 2758, 'mi nulla ac enim in tempor', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', '2018-03-21 03:19:19', '2018-04-21 03:19:19', '2018-05-06 03:19:19', 5005, 'aberdeen-green.png', null, null, 'Fiveclub', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (194, 3, 4919, 'mattis egestas metus', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', '2018-03-07 18:36:15', '2018-04-07 18:36:15', '2018-04-22 18:36:15', 10022, 'aberdeen-green.png', null, null, 'Yombu', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (195, 2, 7800, 'cras in purus eu', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.', '2018-07-25 05:08:22', '2018-08-25 05:08:22', '2018-09-09 05:08:22', 20035, 'amsterdam-orange.png', null, null, 'Voomm', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (196, 4, 6048, 'semper porta volutpat quam pede', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.', '2018-01-29 13:25:29', '2018-02-28 13:25:29', '2018-03-15 13:25:29', 5019, 'aberdeen-green.png', null, null, 'Quinu', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (197, 2, 7667, 'metus arcu adipiscing', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', '2018-03-22 17:01:46', '2018-04-22 17:01:46', '2018-05-07 17:01:46', 20005, 'berlin-blue.png', null, null, 'Twimbo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (198, 3, 2564, 'in consequat ut nulla', 'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', '2018-04-07 16:26:19', '2018-05-07 16:26:19', '2018-05-22 16:26:19', 10027, 'aberdeen-green.png', null, null, 'Skynoodle', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (199, 3, 7508, 'consequat nulla nisl nunc', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.', '2018-01-07 22:16:15', '2018-02-07 22:16:15', '2018-02-22 22:16:15', 10028, 'berlin-blue.png', null, null, 'Thoughtbridge', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (200, 2, 2380, 'metus aenean fermentum donec ut', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', '2018-03-28 16:39:20', '2018-04-28 16:39:20', '2018-05-13 16:39:20', 20000, 'amsterdam-orange.png', null, null, 'Lazz', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (201, 1, 2843, 'leo pellentesque ultrices', 'In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', '2018-11-19 20:26:07', '2018-12-19 20:26:07', '2019-01-03 20:26:07', 40028, 'amsterdam-orange.png', null, null, 'Zoozzy', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (202, 2, 6129, 'eget elit sodales', 'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', '2018-08-22 17:36:48', '2018-09-22 17:36:48', '2018-10-07 17:36:48', 20009, 'amsterdam-orange.png', null, null, 'Realcube', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (203, 2, 2772, 'massa id lobortis convallis tortor risus', 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.

Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', '2018-11-24 22:18:30', '2018-12-24 22:18:30', '2019-01-08 22:18:30', 20046, 'aberdeen-green.png', null, null, 'Oyope', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (204, 4, 382, 'tempor convallis nulla', 'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.', '2018-12-05 13:25:30', '2019-01-05 13:25:30', '2019-01-20 13:25:30', 5016, 'aberdeen-green.png', null, null, 'Bubbletube', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (205, 1, 60, 'vestibulum sagittis sapien cum sociis natoque', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', '2018-01-03 14:30:57', '2018-02-03 14:30:57', '2018-02-18 14:30:57', 40045, 'chicago-red.png', null, null, 'Gabvine', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (206, 3, 913, 'dolor sit amet consectetuer adipiscing elit', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', '2018-03-01 03:47:38', '2018-04-01 03:47:38', '2018-04-16 03:47:38', 10003, 'amsterdam-orange.png', null, null, 'Divavu', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (207, 3, 5217, 'consequat metus sapien', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', '2018-10-13 15:22:52', '2018-11-13 15:22:52', '2018-11-28 15:22:52', 10018, 'chicago-red.png', null, null, 'Jetpulse', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (208, 1, 1159, 'aliquam quis turpis eget elit', 'In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.', '2018-08-02 18:59:59', '2018-09-02 18:59:59', '2018-09-17 18:59:59', 40028, 'aberdeen-green.png', null, null, 'Jatri', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (209, 2, 6632, 'massa tempor convallis nulla neque', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', '2018-03-10 03:35:38', '2018-04-10 03:35:38', '2018-04-25 03:35:38', 20015, 'amsterdam-orange.png', null, null, 'Youspan', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (210, 4, 7351, 'leo maecenas pulvinar lobortis', 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', '2018-11-16 09:52:41', '2018-12-16 09:52:41', '2018-12-31 09:52:41', 5028, 'berlin-blue.png', null, null, 'Topicstorm', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (211, 4, 3299, 'dapibus nulla suscipit ligula', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', '2018-06-14 02:49:06', '2018-07-14 02:49:06', '2018-07-29 02:49:06', 5025, 'chicago-red.png', null, null, 'Camimbo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (212, 4, 6389, 'consequat lectus in est risus auctor', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.', '2018-10-14 14:43:34', '2018-11-14 14:43:34', '2018-11-29 14:43:34', 5017, 'amsterdam-orange.png', null, null, 'Gigashots', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (213, 1, 4212, 'lobortis est phasellus sit amet erat', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.', '2018-04-03 13:10:50', '2018-05-03 13:10:50', '2018-05-18 13:10:50', 40032, 'amsterdam-orange.png', null, null, 'Gabvine', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (214, 4, 835, 'parturient montes nascetur', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', '2018-05-28 09:08:47', '2018-06-28 09:08:47', '2018-07-13 09:08:47', 5021, 'berlin-blue.png', null, null, 'Oozz', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (215, 4, 7675, 'ullamcorper augue a suscipit nulla elit', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', '2018-09-01 21:37:31', '2018-10-01 21:37:31', '2018-10-16 21:37:31', 5038, 'amsterdam-orange.png', null, null, 'Eadel', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (216, 2, 1629, 'ut erat curabitur gravida', 'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', '2018-06-03 18:09:17', '2018-07-03 18:09:17', '2018-07-18 18:09:17', 20032, 'berlin-blue.png', null, null, 'Bluejam', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (217, 1, 24, 'fusce consequat nulla nisl nunc nisl', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', '2018-04-26 05:00:22', '2018-05-26 05:00:22', '2018-06-10 05:00:22', 40040, 'chicago-red.png', null, null, 'Twinte', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (218, 2, 644, 'nisl aenean lectus', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', '2018-02-18 20:15:55', '2018-03-18 20:15:55', '2018-04-02 20:15:55', 20003, 'amsterdam-orange.png', null, null, 'Minyx', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (219, 3, 5742, 'metus sapien ut nunc vestibulum ante', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', '2018-07-09 10:18:36', '2018-08-09 10:18:36', '2018-08-24 10:18:36', 10023, 'aberdeen-green.png', null, null, 'Photofeed', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (220, 3, 3840, 'pharetra magna vestibulum aliquet ultrices', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', '2018-03-31 09:17:17', '2018-04-30 09:17:17', '2018-05-15 09:17:17', 10028, 'chicago-red.png', null, null, 'Kimia', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (221, 3, 1530, 'mauris laoreet ut rhoncus', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.

Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.

Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', '2018-08-09 06:24:05', '2018-09-09 06:24:05', '2018-09-24 06:24:05', 10010, 'amsterdam-orange.png', null, null, 'Rhybox', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (222, 2, 5625, 'dapibus at diam', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.', '2018-02-02 05:43:07', '2018-03-02 05:43:07', '2018-03-17 05:43:07', 20047, 'chicago-red.png', null, null, 'Livefish', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (223, 2, 8106, 'sem fusce consequat nulla nisl nunc', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.', '2018-12-04 07:17:57', '2019-01-04 07:17:57', '2019-01-19 07:17:57', 20042, 'aberdeen-green.png', null, null, 'Cogidoo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (224, 2, 4804, 'nulla nunc purus phasellus', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', '2018-07-28 04:22:47', '2018-08-28 04:22:47', '2018-09-12 04:22:47', 20046, 'aberdeen-green.png', null, null, 'Realpoint', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (225, 2, 2375, 'porttitor lorem id ligula suspendisse', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', '2018-05-15 14:54:47', '2018-06-15 14:54:47', '2018-06-30 14:54:47', 20034, 'amsterdam-orange.png', null, null, 'Quaxo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (226, 4, 7674, 'vulputate ut ultrices vel', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', '2018-06-03 16:54:24', '2018-07-03 16:54:24', '2018-07-18 16:54:24', 5048, 'berlin-blue.png', null, null, 'Jaxworks', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (227, 3, 5946, 'dapibus dolor vel est donec', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', '2018-11-24 13:29:24', '2018-12-24 13:29:24', '2019-01-08 13:29:24', 10014, 'chicago-red.png', null, null, 'Dabjam', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (228, 2, 6352, 'in felis donec semper sapien', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', '2018-03-07 14:15:45', '2018-04-07 14:15:45', '2018-04-22 14:15:45', 20008, 'chicago-red.png', null, null, 'Tavu', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (229, 3, 6072, 'lobortis sapien sapien non mi integer', 'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', '2018-06-19 19:13:28', '2018-07-19 19:13:28', '2018-08-03 19:13:28', 10000, 'aberdeen-green.png', null, null, 'Realbridge', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (230, 2, 6441, 'orci luctus et ultrices posuere cubilia', 'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.

Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', '2018-12-29 03:27:14', '2019-01-29 03:27:14', '2019-02-13 03:27:14', 20015, 'berlin-blue.png', null, null, 'Brainsphere', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (231, 1, 7436, 'augue vel accumsan tellus', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', '2018-06-22 14:43:18', '2018-07-22 14:43:18', '2018-08-06 14:43:18', 40021, 'chicago-red.png', null, null, 'Quimm', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (232, 1, 452, 'habitasse platea dictumst', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', '2018-02-16 14:03:28', '2018-03-16 14:03:28', '2018-03-31 14:03:28', 40032, 'aberdeen-green.png', null, null, 'Vipe', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (233, 2, 776, 'at velit vivamus vel nulla', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', '2018-03-16 07:09:29', '2018-04-16 07:09:29', '2018-05-01 07:09:29', 20043, 'amsterdam-orange.png', null, null, 'Zoombeat', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (234, 1, 1291, 'justo sollicitudin ut suscipit a', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.', '2018-09-01 20:57:47', '2018-10-01 20:57:47', '2018-10-16 20:57:47', 40046, 'amsterdam-orange.png', null, null, 'Gigabox', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (235, 4, 4131, 'justo nec condimentum neque sapien placerat', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.', '2018-03-28 11:52:58', '2018-04-28 11:52:58', '2018-05-13 11:52:58', 5031, 'aberdeen-green.png', null, null, 'Zava', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (236, 2, 5738, 'orci luctus et ultrices', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.

Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.', '2018-05-04 00:13:42', '2018-06-04 00:13:42', '2018-06-19 00:13:42', 20040, 'amsterdam-orange.png', null, null, 'Babblestorm', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (237, 3, 5134, 'id lobortis convallis tortor risus dapibus', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', '2018-08-31 14:27:44', '2018-09-30 14:27:44', '2018-10-15 14:27:44', 10006, 'chicago-red.png', null, null, 'Tazzy', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (238, 2, 4456, 'orci eget orci vehicula condimentum', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', '2018-07-13 11:41:46', '2018-08-13 11:41:46', '2018-08-28 11:41:46', 20043, 'amsterdam-orange.png', null, null, 'Brightbean', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (239, 3, 6758, 'cursus urna ut tellus', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', '2018-09-16 16:39:59', '2018-10-16 16:39:59', '2018-10-31 16:39:59', 10028, 'chicago-red.png', null, null, 'Wikizz', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (240, 1, 5430, 'gravida sem praesent id massa id', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', '2018-04-08 08:45:18', '2018-05-08 08:45:18', '2018-05-23 08:45:18', 40004, 'berlin-blue.png', null, null, 'Jabbersphere', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (241, 3, 5138, 'nulla suscipit ligula in lacus', 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.

Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.', '2018-08-23 22:38:23', '2018-09-23 22:38:23', '2018-10-08 22:38:23', 10038, 'chicago-red.png', null, null, 'Feedspan', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (242, 3, 3976, 'gravida sem praesent id massa', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.', '2018-02-03 03:05:45', '2018-03-03 03:05:45', '2018-03-18 03:05:45', 10030, 'amsterdam-orange.png', null, null, 'Blogtag', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (243, 2, 7141, 'accumsan tortor quis turpis sed ante', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', '2018-03-13 17:27:12', '2018-04-13 17:27:12', '2018-04-28 17:27:12', 20012, 'amsterdam-orange.png', null, null, 'Demimbu', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (244, 1, 1742, 'nunc proin at', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', '2018-06-17 05:47:54', '2018-07-17 05:47:54', '2018-08-01 05:47:54', 40019, 'aberdeen-green.png', null, null, 'Chatterpoint', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (245, 3, 2439, 'sit amet sapien', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', '2018-03-01 14:48:51', '2018-04-01 14:48:51', '2018-04-16 14:48:51', 10029, 'chicago-red.png', null, null, 'Topicshots', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (246, 4, 6052, 'eros suspendisse accumsan tortor quis turpis', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.', '2018-07-13 07:48:03', '2018-08-13 07:48:03', '2018-08-28 07:48:03', 5000, 'berlin-blue.png', null, null, 'Flipopia', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (247, 2, 7480, 'lectus pellentesque eget nunc', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.', '2018-08-29 05:23:37', '2018-09-29 05:23:37', '2018-10-14 05:23:37', 20042, 'chicago-red.png', null, null, 'Rooxo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (248, 3, 2469, 'rhoncus mauris enim', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', '2018-06-15 23:58:48', '2018-07-15 23:58:48', '2018-07-30 23:58:48', 10002, 'aberdeen-green.png', null, null, 'BlogXS', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (249, 4, 24, 'interdum venenatis turpis enim blandit', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', '2018-02-18 10:50:21', '2018-03-18 10:50:21', '2018-04-02 10:50:21', 5009, 'chicago-red.png', null, null, 'Dynava', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (250, 4, 3314, 'accumsan tellus nisi eu', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', '2018-10-20 15:45:28', '2018-11-20 15:45:28', '2018-12-05 15:45:28', 5009, 'berlin-blue.png', null, null, 'Cogibox', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (251, 1, 1778, 'non ligula pellentesque', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', '2018-08-11 08:05:53', '2018-09-11 08:05:53', '2018-09-26 08:05:53', 40020, 'aberdeen-green.png', null, null, 'Oloo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (252, 1, 7671, 'pulvinar sed nisl nunc rhoncus', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', '2018-05-01 12:55:29', '2018-06-01 12:55:29', '2018-06-16 12:55:29', 40030, 'berlin-blue.png', null, null, 'Browsetype', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (253, 4, 6356, 'nibh in quis justo maecenas', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', '2018-03-18 17:09:02', '2018-04-18 17:09:02', '2018-05-03 17:09:02', 5011, 'berlin-blue.png', null, null, 'Skilith', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (254, 4, 6109, 'orci mauris lacinia', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', '2018-12-25 00:22:55', '2019-01-25 00:22:55', '2019-02-09 00:22:55', 5049, 'berlin-blue.png', null, null, 'Feedfish', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (255, 1, 4747, 'auctor sed tristique in tempus sit', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', '2018-06-13 12:09:13', '2018-07-13 12:09:13', '2018-07-28 12:09:13', 40050, 'berlin-blue.png', null, null, 'Gevee', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (256, 2, 4583, 'ultrices erat tortor sollicitudin', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', '2018-04-08 04:29:31', '2018-05-08 04:29:31', '2018-05-23 04:29:31', 20027, 'aberdeen-green.png', null, null, 'Twitternation', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (257, 2, 7913, 'est et tempus semper est', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', '2018-07-31 17:43:55', '2018-08-31 17:43:55', '2018-09-15 17:43:55', 20009, 'aberdeen-green.png', null, null, 'Quatz', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (258, 4, 6315, 'consequat dui nec', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', '2018-02-20 00:25:27', '2018-03-20 00:25:27', '2018-04-04 00:25:27', 5038, 'amsterdam-orange.png', null, null, 'Yamia', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (259, 1, 2604, 'quis orci nullam molestie nibh in', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', '2018-07-19 14:13:04', '2018-08-19 14:13:04', '2018-09-03 14:13:04', 40011, 'berlin-blue.png', null, null, 'Jabbercube', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (260, 2, 6890, 'primis in faucibus', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', '2018-01-05 21:03:09', '2018-02-05 21:03:09', '2018-02-20 21:03:09', 20017, 'aberdeen-green.png', null, null, 'Riffpath', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (261, 4, 6479, 'libero nullam sit amet turpis elementum', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.', '2018-02-18 07:36:05', '2018-03-18 07:36:05', '2018-04-02 07:36:05', 5039, 'amsterdam-orange.png', null, null, 'Camimbo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (262, 1, 1991, 'in sagittis dui vel', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', '2018-02-24 00:27:52', '2018-03-24 00:27:52', '2018-04-08 00:27:52', 40024, 'amsterdam-orange.png', null, null, 'Kare', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (263, 2, 7912, 'luctus et ultrices posuere', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', '2018-05-19 15:52:50', '2018-06-19 15:52:50', '2018-07-04 15:52:50', 20026, 'amsterdam-orange.png', null, null, 'Thoughtstorm', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (264, 4, 6749, 'vestibulum aliquet ultrices', 'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', '2018-02-14 06:45:10', '2018-03-14 06:45:10', '2018-03-29 06:45:10', 5014, 'amsterdam-orange.png', null, null, 'LiveZ', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (265, 4, 5885, 'cras mi pede malesuada in', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', '2018-03-12 21:14:30', '2018-04-12 21:14:30', '2018-04-27 21:14:30', 5036, 'berlin-blue.png', null, null, 'Zoomcast', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (266, 3, 435, 'faucibus orci luctus et ultrices', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', '2018-04-20 04:04:07', '2018-05-20 04:04:07', '2018-06-04 04:04:07', 10006, 'chicago-red.png', null, null, 'Wikibox', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (267, 4, 1289, 'diam id ornare imperdiet sapien urna', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.', '2018-10-13 08:17:15', '2018-11-13 08:17:15', '2018-11-28 08:17:15', 5015, 'amsterdam-orange.png', null, null, 'Avavee', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (268, 3, 949, 'malesuada in imperdiet et', 'Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', '2018-12-29 12:07:40', '2019-01-29 12:07:40', '2019-02-13 12:07:40', 10048, 'amsterdam-orange.png', null, null, 'Bluejam', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (269, 4, 7674, 'vitae quam suspendisse potenti nullam porttitor', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', '2018-01-09 12:05:54', '2018-02-09 12:05:54', '2018-02-24 12:05:54', 5007, 'amsterdam-orange.png', null, null, 'Mydeo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (270, 2, 4404, 'nascetur ridiculus mus etiam vel', 'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', '2018-07-06 16:00:06', '2018-08-06 16:00:06', '2018-08-21 16:00:06', 20044, 'aberdeen-green.png', null, null, 'Riffpath', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (271, 3, 1639, 'consectetuer adipiscing elit proin risus praesent', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', '2018-02-16 03:41:32', '2018-03-16 03:41:32', '2018-03-31 03:41:32', 10018, 'amsterdam-orange.png', null, null, 'Oyoyo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (272, 1, 5021, 'at velit vivamus vel nulla', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', '2018-12-29 06:15:54', '2019-01-29 06:15:54', '2019-02-13 06:15:54', 40035, 'berlin-blue.png', null, null, 'Skyble', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (273, 3, 5864, 'luctus et ultrices', 'In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', '2018-10-13 12:22:33', '2018-11-13 12:22:33', '2018-11-28 12:22:33', 10041, 'berlin-blue.png', null, null, 'Devify', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (274, 4, 1218, 'quis tortor id nulla ultrices', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', '2018-04-10 04:57:35', '2018-05-10 04:57:35', '2018-05-25 04:57:35', 5050, 'aberdeen-green.png', null, null, 'Youfeed', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (275, 2, 774, 'suspendisse potenti in eleifend quam a', 'In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.', '2018-09-29 21:25:58', '2018-10-29 21:25:58', '2018-11-13 21:25:58', 20032, 'berlin-blue.png', null, null, 'Dynabox', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (276, 4, 5337, 'suspendisse potenti nullam', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', '2018-12-29 23:28:01', '2019-01-29 23:28:01', '2019-02-13 23:28:01', 5039, 'amsterdam-orange.png', null, null, 'Kare', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (277, 2, 7244, 'mattis odio donec vitae nisi', 'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', '2018-10-18 14:00:58', '2018-11-18 14:00:58', '2018-12-03 14:00:58', 20019, 'amsterdam-orange.png', null, null, 'Divanoodle', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (278, 2, 561, 'dictumst morbi vestibulum velit id', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.

Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.', '2018-08-24 22:37:10', '2018-09-24 22:37:10', '2018-10-09 22:37:10', 20050, 'berlin-blue.png', null, null, 'Trudoo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (279, 1, 6231, 'praesent blandit lacinia erat vestibulum', 'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', '2018-04-06 04:46:14', '2018-05-06 04:46:14', '2018-05-21 04:46:14', 40026, 'aberdeen-green.png', null, null, 'Realcube', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (280, 3, 4485, 'lacus at velit vivamus vel', 'Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', '2018-08-28 17:04:20', '2018-09-28 17:04:20', '2018-10-13 17:04:20', 10009, 'chicago-red.png', null, null, 'Skiptube', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (281, 4, 307, 'faucibus cursus urna', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.', '2018-06-13 23:59:38', '2018-07-13 23:59:38', '2018-07-28 23:59:38', 5014, 'berlin-blue.png', null, null, 'Aimbo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (282, 4, 3432, 'elit proin risus', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.', '2018-10-10 15:09:37', '2018-11-10 15:09:37', '2018-11-25 15:09:37', 5025, 'amsterdam-orange.png', null, null, 'Blogpad', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (283, 2, 5017, 'in libero ut massa volutpat', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', '2018-07-28 15:27:31', '2018-08-28 15:27:31', '2018-09-12 15:27:31', 20014, 'amsterdam-orange.png', null, null, 'Skyvu', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (284, 3, 5927, 'risus semper porta', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', '2018-11-05 03:26:47', '2018-12-05 03:26:47', '2018-12-20 03:26:47', 10017, 'chicago-red.png', null, null, 'Bluejam', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (285, 1, 5417, 'augue a suscipit nulla', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.', '2018-05-20 22:04:33', '2018-06-20 22:04:33', '2018-07-05 22:04:33', 40035, 'amsterdam-orange.png', null, null, 'Kwideo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (286, 3, 653, 'mauris viverra diam vitae quam', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', '2018-04-03 05:38:40', '2018-05-03 05:38:40', '2018-05-18 05:38:40', 10038, 'amsterdam-orange.png', null, null, 'Fliptune', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (287, 1, 7944, 'elementum pellentesque quisque porta volutpat', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', '2018-05-08 02:46:35', '2018-06-08 02:46:35', '2018-06-23 02:46:35', 40028, 'chicago-red.png', null, null, 'Feedfish', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (288, 4, 7879, 'eleifend quam a odio in hac', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', '2018-07-22 05:07:00', '2018-08-22 05:07:00', '2018-09-06 05:07:00', 5008, 'amsterdam-orange.png', null, null, 'InnoZ', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (289, 1, 1216, 'libero rutrum ac lobortis vel', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', '2018-07-04 09:59:24', '2018-08-04 09:59:24', '2018-08-19 09:59:24', 40029, 'amsterdam-orange.png', null, null, 'Demivee', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (290, 4, 7806, 'et ultrices posuere', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', '2018-09-18 17:54:44', '2018-10-18 17:54:44', '2018-11-02 17:54:44', 5012, 'amsterdam-orange.png', null, null, 'Mycat', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (291, 3, 5800, 'vulputate luctus cum', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.', '2018-07-12 23:18:49', '2018-08-12 23:18:49', '2018-08-27 23:18:49', 10004, 'chicago-red.png', null, null, 'Gabspot', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (292, 4, 7407, 'vel augue vestibulum', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', '2018-04-11 06:36:43', '2018-05-11 06:36:43', '2018-05-26 06:36:43', 5025, 'berlin-blue.png', null, null, 'Devshare', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (293, 2, 1791, 'nisl duis ac nibh fusce lacus', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', '2018-09-20 20:23:12', '2018-10-20 20:23:12', '2018-11-04 20:23:12', 20004, 'berlin-blue.png', null, null, 'Quatz', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (294, 1, 156, 'erat vestibulum sed', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', '2018-07-11 08:17:12', '2018-08-11 08:17:12', '2018-08-26 08:17:12', 40002, 'amsterdam-orange.png', null, null, 'Leenti', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (295, 4, 4067, 'lorem quisque ut erat curabitur', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', '2018-08-31 18:16:17', '2018-09-30 18:16:17', '2018-10-15 18:16:17', 5032, 'amsterdam-orange.png', null, null, 'Miboo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (296, 2, 6714, 'curae duis faucibus', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.', '2018-09-06 20:09:16', '2018-10-06 20:09:16', '2018-10-21 20:09:16', 20029, 'chicago-red.png', null, null, 'Livetube', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (297, 4, 6907, 'quam fringilla rhoncus', 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.

Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', '2018-07-28 03:40:38', '2018-08-28 03:40:38', '2018-09-12 03:40:38', 5033, 'aberdeen-green.png', null, null, 'Twitterbeat', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (298, 3, 3083, 'lorem ipsum dolor sit amet', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', '2018-12-06 01:07:10', '2019-01-06 01:07:10', '2019-01-21 01:07:10', 10006, 'aberdeen-green.png', null, null, 'Gabvine', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (299, 1, 2682, 'ornare imperdiet sapien urna', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.', '2018-11-01 04:46:29', '2018-12-01 04:46:29', '2018-12-16 04:46:29', 40036, 'aberdeen-green.png', null, null, 'Rhyloo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (300, 3, 4557, 'vel ipsum praesent blandit lacinia', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', '2018-02-26 05:25:37', '2018-03-26 05:25:37', '2018-04-10 05:25:37', 10014, 'amsterdam-orange.png', null, null, 'Livefish', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (301, 1, 3399, 'vitae nisi nam', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', '2018-07-15 22:42:59', '2018-08-15 22:42:59', '2018-08-30 22:42:59', 40031, 'aberdeen-green.png', null, null, 'Photobug', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (302, 1, 5296, 'mus vivamus vestibulum sagittis', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', '2018-02-21 09:57:07', '2018-03-21 09:57:07', '2018-04-05 09:57:07', 40006, 'berlin-blue.png', null, null, 'Kanoodle', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (303, 2, 1766, 'sapien cursus vestibulum proin eu mi', 'Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', '2018-06-05 12:29:34', '2018-07-05 12:29:34', '2018-07-20 12:29:34', 20010, 'amsterdam-orange.png', null, null, 'Voonte', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (304, 3, 7997, 'vitae quam suspendisse potenti nullam', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', '2018-08-17 04:11:25', '2018-09-17 04:11:25', '2018-10-02 04:11:25', 10025, 'amsterdam-orange.png', null, null, 'Voolia', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (305, 3, 1433, 'purus eu magna vulputate luctus cum', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', '2018-01-12 14:13:53', '2018-02-12 14:13:53', '2018-02-27 14:13:53', 10043, 'amsterdam-orange.png', null, null, 'Livepath', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (306, 3, 2878, 'vel accumsan tellus nisi eu orci', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', '2018-12-18 10:32:28', '2019-01-18 10:32:28', '2019-02-02 10:32:28', 10025, 'berlin-blue.png', null, null, 'Izio', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (307, 1, 7672, 'suspendisse potenti nullam', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', '2018-05-09 09:51:10', '2018-06-09 09:51:10', '2018-06-24 09:51:10', 40048, 'berlin-blue.png', null, null, 'Photospace', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (308, 4, 1648, 'quis augue luctus tincidunt', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', '2018-04-18 23:08:08', '2018-05-18 23:08:08', '2018-06-02 23:08:08', 5015, 'chicago-red.png', null, null, 'Blogtags', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (309, 2, 1872, 'dis parturient montes', 'Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', '2018-03-05 14:08:11', '2018-04-05 14:08:11', '2018-04-20 14:08:11', 20010, 'chicago-red.png', null, null, 'Wordify', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (310, 4, 949, 'pellentesque eget nunc donec quis orci', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', '2018-11-04 10:41:08', '2018-12-04 10:41:08', '2018-12-19 10:41:08', 5026, 'berlin-blue.png', null, null, 'Skyndu', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (311, 2, 3087, 'eu interdum eu', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', '2018-02-21 09:05:35', '2018-03-21 09:05:35', '2018-04-05 09:05:35', 20030, 'berlin-blue.png', null, null, 'Tekfly', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (312, 1, 6003, 'quis tortor id', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.', '2018-07-28 09:04:21', '2018-08-28 09:04:21', '2018-09-12 09:04:21', 40017, 'berlin-blue.png', null, null, 'Plambee', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (313, 2, 7721, 'diam cras pellentesque volutpat dui maecenas', 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.

Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.', '2018-11-06 08:02:51', '2018-12-06 08:02:51', '2018-12-21 08:02:51', 20002, 'chicago-red.png', null, null, 'Mycat', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (314, 1, 3765, 'suspendisse ornare consequat lectus in est', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.', '2018-05-11 13:11:44', '2018-06-11 13:11:44', '2018-06-26 13:11:44', 40027, 'aberdeen-green.png', null, null, 'Linklinks', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (315, 2, 5900, 'viverra dapibus nulla suscipit', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', '2018-07-10 20:35:48', '2018-08-10 20:35:48', '2018-08-25 20:35:48', 20038, 'aberdeen-green.png', null, null, 'Quimba', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (316, 1, 3467, 'sit amet lobortis sapien sapien non', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', '2018-11-25 11:02:31', '2018-12-25 11:02:31', '2019-01-09 11:02:31', 40027, 'berlin-blue.png', null, null, 'Rooxo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (317, 1, 3165, 'vestibulum ante ipsum primis in faucibus', 'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.', '2018-11-25 10:51:03', '2018-12-25 10:51:03', '2019-01-09 10:51:03', 40025, 'berlin-blue.png', null, null, 'Skyba', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (318, 3, 7221, 'lectus aliquam sit amet diam', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', '2018-12-11 07:28:54', '2019-01-11 07:28:54', '2019-01-26 07:28:54', 10027, 'chicago-red.png', null, null, 'Kaymbo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (319, 3, 7227, 'morbi ut odio cras mi pede', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.', '2018-01-23 08:10:35', '2018-02-23 08:10:35', '2018-03-10 08:10:35', 10012, 'aberdeen-green.png', null, null, 'Flashset', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (320, 1, 4135, 'donec odio justo sollicitudin ut', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', '2018-05-22 04:26:00', '2018-06-22 04:26:00', '2018-07-07 04:26:00', 40000, 'amsterdam-orange.png', null, null, 'Browsecat', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (321, 1, 4168, 'lacus morbi sem mauris', 'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', '2018-11-21 01:39:52', '2018-12-21 01:39:52', '2019-01-05 01:39:52', 40029, 'berlin-blue.png', null, null, 'Dynava', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (322, 3, 7275, 'nisl duis bibendum felis', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.', '2018-02-24 15:51:59', '2018-03-24 15:51:59', '2018-04-08 15:51:59', 10023, 'amsterdam-orange.png', null, null, 'Feedbug', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (323, 1, 7021, 'sed nisl nunc rhoncus', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', '2018-01-29 06:57:34', '2018-02-28 06:57:34', '2018-03-15 06:57:34', 40019, 'chicago-red.png', null, null, 'Mycat', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (324, 3, 746, 'volutpat sapien arcu sed augue aliquam', 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', '2018-04-30 05:23:40', '2018-05-30 05:23:40', '2018-06-14 05:23:40', 10005, 'chicago-red.png', null, null, 'Oyope', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (325, 4, 1765, 'ut dolor morbi vel lectus in', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', '2018-06-04 02:45:32', '2018-07-04 02:45:32', '2018-07-19 02:45:32', 5020, 'berlin-blue.png', null, null, 'Brightbean', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (326, 1, 6099, 'rhoncus mauris enim', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', '2018-10-08 07:15:13', '2018-11-08 07:15:13', '2018-11-23 07:15:13', 40002, 'aberdeen-green.png', null, null, 'Voonte', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (327, 2, 6238, 'purus phasellus in felis donec semper', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', '2018-11-30 14:23:36', '2018-12-30 14:23:36', '2019-01-14 14:23:36', 20002, 'berlin-blue.png', null, null, 'Flashset', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (328, 2, 796, 'imperdiet sapien urna pretium', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.', '2018-10-03 07:11:51', '2018-11-03 07:11:51', '2018-11-18 07:11:51', 20032, 'chicago-red.png', null, null, 'Dablist', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (329, 2, 4550, 'venenatis lacinia aenean sit amet', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', '2018-03-23 12:17:03', '2018-04-23 12:17:03', '2018-05-08 12:17:03', 20012, 'berlin-blue.png', null, null, 'Gevee', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (330, 2, 1807, 'tempus vel pede', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.', '2018-12-28 05:53:14', '2019-01-28 05:53:14', '2019-02-12 05:53:14', 20019, 'chicago-red.png', null, null, 'Oodoo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (331, 3, 6678, 'habitasse platea dictumst morbi vestibulum velit', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', '2018-06-22 23:45:27', '2018-07-22 23:45:27', '2018-08-06 23:45:27', 10003, 'aberdeen-green.png', null, null, 'Yambee', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (332, 4, 4315, 'nulla tempus vivamus', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', '2018-09-22 23:45:39', '2018-10-22 23:45:39', '2018-11-06 23:45:39', 5004, 'aberdeen-green.png', null, null, 'Thoughtmix', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (333, 1, 5721, 'adipiscing lorem vitae mattis nibh ligula', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.

Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.', '2018-01-26 09:36:27', '2018-02-26 09:36:27', '2018-03-13 09:36:27', 40050, 'amsterdam-orange.png', null, null, 'Twinder', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (334, 3, 2086, 'elementum nullam varius nulla', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.', '2018-04-24 02:31:26', '2018-05-24 02:31:26', '2018-06-08 02:31:26', 10016, 'berlin-blue.png', null, null, 'Jaxspan', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (335, 3, 585, 'eget semper rutrum nulla', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', '2018-02-10 02:35:58', '2018-03-10 02:35:58', '2018-03-25 02:35:58', 10026, 'aberdeen-green.png', null, null, 'Thoughtmix', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (336, 2, 3363, 'primis in faucibus orci luctus et', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', '2018-11-01 04:28:19', '2018-12-01 04:28:19', '2018-12-16 04:28:19', 20030, 'amsterdam-orange.png', null, null, 'Twitterbridge', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (337, 3, 3988, 'erat volutpat in congue etiam justo', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.', '2018-10-16 00:58:38', '2018-11-16 00:58:38', '2018-12-01 00:58:38', 10022, 'amsterdam-orange.png', null, null, 'Devify', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (338, 1, 2103, 'quisque id justo', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.', '2018-11-08 17:59:54', '2018-12-08 17:59:54', '2018-12-23 17:59:54', 40046, 'berlin-blue.png', null, null, 'Rhynoodle', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (339, 4, 7197, 'erat volutpat in congue etiam', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', '2018-10-06 01:14:53', '2018-11-06 01:14:53', '2018-11-21 01:14:53', 5030, 'amsterdam-orange.png', null, null, 'Wordpedia', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (340, 4, 3284, 'faucibus cursus urna', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', '2018-10-01 23:17:01', '2018-11-01 23:17:01', '2018-11-16 23:17:01', 5007, 'chicago-red.png', null, null, 'Gabtype', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (341, 4, 4525, 'nam tristique tortor', 'Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.', '2018-06-23 16:54:19', '2018-07-23 16:54:19', '2018-08-07 16:54:19', 5033, 'berlin-blue.png', null, null, 'Thoughtbridge', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (342, 1, 4996, 'cursus id turpis', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', '2018-03-12 05:16:09', '2018-04-12 05:16:09', '2018-04-27 05:16:09', 40022, 'berlin-blue.png', null, null, 'Avamba', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (343, 1, 3577, 'vestibulum sed magna at', 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', '2018-08-18 05:19:44', '2018-09-18 05:19:44', '2018-10-03 05:19:44', 40015, 'berlin-blue.png', null, null, 'Babbleset', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (344, 1, 2299, 'orci eget orci vehicula condimentum curabitur', 'Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.', '2018-03-07 20:54:35', '2018-04-07 20:54:35', '2018-04-22 20:54:35', 40025, 'berlin-blue.png', null, null, 'Omba', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (345, 3, 1564, 'faucibus cursus urna', 'Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.', '2018-12-30 23:03:05', '2019-01-30 23:03:05', '2019-02-14 23:03:05', 10009, 'aberdeen-green.png', null, null, 'Myworks', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (346, 4, 6144, 'morbi quis tortor id nulla', 'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', '2018-02-08 19:32:58', '2018-03-08 19:32:58', '2018-03-23 19:32:58', 5028, 'aberdeen-green.png', null, null, 'Plambee', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (347, 2, 4396, 'vestibulum velit id', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', '2018-05-25 08:47:33', '2018-06-25 08:47:33', '2018-07-10 08:47:33', 20023, 'chicago-red.png', null, null, 'Devpulse', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (348, 3, 6205, 'lacinia erat vestibulum sed', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', '2018-12-19 06:35:37', '2019-01-19 06:35:37', '2019-02-03 06:35:37', 10022, 'aberdeen-green.png', null, null, 'Skimia', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (349, 1, 7177, 'a pede posuere', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', '2018-01-05 04:17:36', '2018-02-05 04:17:36', '2018-02-20 04:17:36', 40018, 'aberdeen-green.png', null, null, 'Yata', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (350, 3, 7720, 'mauris sit amet eros suspendisse', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.', '2018-04-15 01:20:04', '2018-05-15 01:20:04', '2018-05-30 01:20:04', 10039, 'aberdeen-green.png', null, null, 'Wordpedia', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (351, 1, 3906, 'est lacinia nisi venenatis tristique fusce', 'In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.', '2018-05-01 05:26:21', '2018-06-01 05:26:21', '2018-06-16 05:26:21', 40012, 'chicago-red.png', null, null, 'Eabox', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (352, 1, 5231, 'odio elementum eu', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', '2018-01-15 14:10:09', '2018-02-15 14:10:09', '2018-03-02 14:10:09', 40004, 'amsterdam-orange.png', null, null, 'Skyndu', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (353, 2, 2382, 'vestibulum ac est lacinia nisi', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', '2018-06-10 03:56:47', '2018-07-10 03:56:47', '2018-07-25 03:56:47', 20030, 'berlin-blue.png', null, null, 'Dabtype', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (354, 1, 6431, 'vel ipsum praesent blandit lacinia', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', '2018-02-25 02:08:50', '2018-03-25 02:08:50', '2018-04-09 02:08:50', 40046, 'chicago-red.png', null, null, 'Linkbuzz', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (355, 4, 2993, 'ridiculus mus etiam vel augue', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', '2018-10-26 22:09:00', '2018-11-26 22:09:00', '2018-12-11 22:09:00', 5014, 'amsterdam-orange.png', null, null, 'JumpXS', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (356, 1, 4136, 'curabitur gravida nisi at nibh', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', '2018-09-13 20:52:36', '2018-10-13 20:52:36', '2018-10-28 20:52:36', 40018, 'berlin-blue.png', null, null, 'Blogspan', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (357, 2, 365, 'ut odio cras mi pede', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', '2018-04-23 12:51:01', '2018-05-23 12:51:01', '2018-06-07 12:51:01', 20021, 'aberdeen-green.png', null, null, 'Tagchat', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (358, 4, 2363, 'lacus morbi quis tortor id', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', '2018-12-01 10:57:18', '2019-01-01 10:57:18', '2019-01-16 10:57:18', 5050, 'berlin-blue.png', null, null, 'Skyvu', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (359, 1, 921, 'ac neque duis bibendum morbi non', 'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', '2018-04-26 12:29:30', '2018-05-26 12:29:30', '2018-06-10 12:29:30', 40008, 'amsterdam-orange.png', null, null, 'Eamia', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (360, 2, 3701, 'phasellus in felis donec', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', '2018-10-14 13:40:46', '2018-11-14 13:40:46', '2018-11-29 13:40:46', 20033, 'amsterdam-orange.png', null, null, 'Camimbo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (361, 1, 6979, 'neque sapien placerat ante nulla', 'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', '2018-09-11 17:23:59', '2018-10-11 17:23:59', '2018-10-26 17:23:59', 40033, 'chicago-red.png', null, null, 'Meedoo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (362, 2, 6838, 'egestas metus aenean fermentum donec', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.', '2018-12-13 14:45:52', '2019-01-13 14:45:52', '2019-01-28 14:45:52', 20008, 'aberdeen-green.png', null, null, 'Yakidoo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (363, 2, 6641, 'nisi nam ultrices libero non', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', '2018-10-25 01:22:34', '2018-11-25 01:22:34', '2018-12-10 01:22:34', 20045, 'amsterdam-orange.png', null, null, 'Roodel', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (364, 1, 7123, 'tortor risus dapibus augue vel accumsan', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', '2018-05-02 02:40:58', '2018-06-02 02:40:58', '2018-06-17 02:40:58', 40012, 'aberdeen-green.png', null, null, 'Camimbo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (365, 2, 6959, 'in congue etiam', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', '2018-10-11 19:37:37', '2018-11-11 19:37:37', '2018-11-26 19:37:37', 20042, 'amsterdam-orange.png', null, null, 'Devbug', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (366, 3, 3331, 'non quam nec', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', '2018-04-04 14:23:58', '2018-05-04 14:23:58', '2018-05-19 14:23:58', 10037, 'aberdeen-green.png', null, null, 'Topiczoom', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (367, 2, 7065, 'sagittis dui vel nisl', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.', '2018-10-25 21:50:24', '2018-11-25 21:50:24', '2018-12-10 21:50:24', 20019, 'aberdeen-green.png', null, null, 'Gevee', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (368, 4, 5635, 'aliquam quis turpis eget', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.', '2018-06-24 10:20:47', '2018-07-24 10:20:47', '2018-08-08 10:20:47', 5025, 'aberdeen-green.png', null, null, 'Zoomdog', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (369, 4, 1312, 'sed nisl nunc rhoncus dui vel', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', '2018-08-10 06:52:47', '2018-09-10 06:52:47', '2018-09-25 06:52:47', 5033, 'chicago-red.png', null, null, 'Topiclounge', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (370, 3, 6764, 'nam tristique tortor eu', 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', '2018-10-01 11:32:30', '2018-11-01 11:32:30', '2018-11-16 11:32:30', 10027, 'amsterdam-orange.png', null, null, 'Wikizz', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (371, 3, 1718, 'sapien placerat ante nulla', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.', '2018-05-15 20:48:58', '2018-06-15 20:48:58', '2018-06-30 20:48:58', 10049, 'chicago-red.png', null, null, 'Oyondu', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (372, 2, 5095, 'amet turpis elementum ligula', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', '2018-05-04 10:49:00', '2018-06-04 10:49:00', '2018-06-19 10:49:00', 20040, 'amsterdam-orange.png', null, null, 'Meejo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (373, 3, 4960, 'montes nascetur ridiculus mus etiam vel', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.', '2018-05-25 10:50:35', '2018-06-25 10:50:35', '2018-07-10 10:50:35', 10010, 'amsterdam-orange.png', null, null, 'Topicware', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (374, 2, 6535, 'lectus pellentesque eget nunc', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', '2018-10-18 16:59:43', '2018-11-18 16:59:43', '2018-12-03 16:59:43', 20025, 'amsterdam-orange.png', null, null, 'DabZ', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (375, 2, 5877, 'neque duis bibendum morbi', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.', '2018-03-30 06:11:26', '2018-04-30 06:11:26', '2018-05-15 06:11:26', 20031, 'chicago-red.png', null, null, 'Lazzy', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (376, 3, 2174, 'ut rhoncus aliquet pulvinar sed nisl', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.', '2018-02-05 16:18:28', '2018-03-05 16:18:28', '2018-03-20 16:18:28', 10005, 'chicago-red.png', null, null, 'Voonyx', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (377, 2, 370, 'turpis sed ante vivamus', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.', '2018-01-26 09:12:42', '2018-02-26 09:12:42', '2018-03-13 09:12:42', 20005, 'aberdeen-green.png', null, null, 'Realcube', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (378, 2, 128, 'justo aliquam quis turpis', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', '2018-02-18 09:07:04', '2018-03-18 09:07:04', '2018-04-02 09:07:04', 20037, 'berlin-blue.png', null, null, 'Thoughtstorm', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (379, 3, 669, 'elementum in hac habitasse platea', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', '2018-06-16 11:08:58', '2018-07-16 11:08:58', '2018-07-31 11:08:58', 10010, 'amsterdam-orange.png', null, null, 'Topicshots', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (380, 2, 2682, 'mollis molestie lorem quisque ut', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', '2018-01-19 07:18:18', '2018-02-19 07:18:18', '2018-03-06 07:18:18', 20009, 'amsterdam-orange.png', null, null, 'Lazz', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (381, 2, 991, 'mi nulla ac enim in tempor', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', '2018-03-05 19:45:58', '2018-04-05 19:45:58', '2018-04-20 19:45:58', 20050, 'amsterdam-orange.png', null, null, 'Tekfly', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (382, 1, 2224, 'odio justo sollicitudin ut', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', '2018-08-06 18:29:57', '2018-09-06 18:29:57', '2018-09-21 18:29:57', 40047, 'chicago-red.png', null, null, 'Buzzshare', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (383, 4, 7561, 'justo nec condimentum neque sapien', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.', '2018-09-20 07:29:30', '2018-10-20 07:29:30', '2018-11-04 07:29:30', 5041, 'aberdeen-green.png', null, null, 'Skinix', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (384, 2, 597, 'duis bibendum felis sed interdum', 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', '2018-05-22 06:52:28', '2018-06-22 06:52:28', '2018-07-07 06:52:28', 20012, 'chicago-red.png', null, null, 'Roodel', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (385, 2, 3009, 'et eros vestibulum ac', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', '2018-11-06 04:02:10', '2018-12-06 04:02:10', '2018-12-21 04:02:10', 20004, 'aberdeen-green.png', null, null, 'Kwimbee', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (386, 2, 2650, 'non velit donec diam neque', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', '2018-01-31 12:02:41', '2018-02-28 12:02:41', '2018-03-15 12:02:41', 20013, 'berlin-blue.png', null, null, 'Roomm', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (387, 3, 4904, 'quis tortor id nulla ultrices', 'In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', '2018-09-16 02:40:56', '2018-10-16 02:40:56', '2018-10-31 02:40:56', 10043, 'amsterdam-orange.png', null, null, 'Youfeed', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (388, 2, 3937, 'sed justo pellentesque viverra', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.', '2018-04-23 10:57:32', '2018-05-23 10:57:32', '2018-06-07 10:57:32', 20015, 'aberdeen-green.png', null, null, 'Tanoodle', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (389, 2, 1818, 'tortor id nulla ultrices aliquet maecenas', 'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', '2018-10-24 00:23:44', '2018-11-24 00:23:44', '2018-12-09 00:23:44', 20050, 'amsterdam-orange.png', null, null, 'Zooxo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (390, 3, 1779, 'ut ultrices vel augue vestibulum ante', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', '2018-12-26 14:11:07', '2019-01-26 14:11:07', '2019-02-10 14:11:07', 10002, 'amsterdam-orange.png', null, null, 'Yombu', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (391, 3, 2792, 'proin leo odio porttitor id consequat', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.', '2018-02-03 23:57:22', '2018-03-03 23:57:22', '2018-03-18 23:57:22', 10043, 'aberdeen-green.png', null, null, 'Trilith', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (392, 2, 156, 'eu mi nulla ac enim', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', '2018-01-02 05:41:54', '2018-02-02 05:41:54', '2018-02-17 05:41:54', 20022, 'amsterdam-orange.png', null, null, 'Buzzshare', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (393, 3, 3479, 'ante vivamus tortor', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', '2018-02-24 15:43:15', '2018-03-24 15:43:15', '2018-04-08 15:43:15', 10033, 'amsterdam-orange.png', null, null, 'Trilia', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (394, 1, 5232, 'justo nec condimentum neque sapien', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', '2018-08-24 14:59:30', '2018-09-24 14:59:30', '2018-10-09 14:59:30', 40008, 'berlin-blue.png', null, null, 'Skyba', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (395, 3, 7934, 'nullam sit amet turpis', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.', '2018-01-20 09:22:34', '2018-02-20 09:22:34', '2018-03-07 09:22:34', 10034, 'berlin-blue.png', null, null, 'Brightdog', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (396, 4, 2399, 'placerat praesent blandit nam', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.', '2018-09-11 01:09:06', '2018-10-11 01:09:06', '2018-10-26 01:09:06', 5047, 'berlin-blue.png', null, null, 'Meetz', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (397, 3, 7409, 'interdum mauris ullamcorper', 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', '2018-02-27 12:54:12', '2018-03-27 12:54:12', '2018-04-11 12:54:12', 10008, 'aberdeen-green.png', null, null, 'Wikido', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (398, 1, 5042, 'cum sociis natoque penatibus et', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', '2018-12-20 22:50:18', '2019-01-20 22:50:18', '2019-02-04 22:50:18', 40000, 'berlin-blue.png', null, null, 'Realmix', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (399, 2, 7788, 'in eleifend quam a odio in', 'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.', '2018-12-11 19:58:19', '2019-01-11 19:58:19', '2019-01-26 19:58:19', 20000, 'berlin-blue.png', null, null, 'Realbridge', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (400, 2, 7303, 'neque sapien placerat', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', '2018-01-01 02:19:46', '2018-02-01 02:19:46', '2018-02-16 02:19:46', 20009, 'berlin-blue.png', null, null, 'Twimm', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (401, 1, 761, 'sed accumsan felis ut', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.', '2018-03-25 10:08:18', '2018-04-25 10:08:18', '2018-05-10 10:08:18', 40016, 'berlin-blue.png', null, null, 'Pixoboo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (402, 2, 4113, 'erat eros viverra eget', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.', '2018-12-23 05:24:02', '2019-01-23 05:24:02', '2019-02-07 05:24:02', 20031, 'berlin-blue.png', null, null, 'Photospace', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (403, 2, 7348, 'eu tincidunt in leo maecenas pulvinar', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', '2018-12-13 13:05:08', '2019-01-13 13:05:08', '2019-01-28 13:05:08', 20031, 'aberdeen-green.png', null, null, 'Oyope', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (404, 3, 1101, 'ullamcorper purus sit amet nulla quisque', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.', '2018-12-14 00:08:33', '2019-01-14 00:08:33', '2019-01-29 00:08:33', 10043, 'berlin-blue.png', null, null, 'Zava', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (405, 4, 4967, 'at dolor quis odio consequat', 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.

Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', '2018-04-19 11:03:49', '2018-05-19 11:03:49', '2018-06-03 11:03:49', 5037, 'berlin-blue.png', null, null, 'Tagfeed', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (406, 1, 7721, 'venenatis non sodales sed tincidunt', 'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', '2018-06-11 01:30:14', '2018-07-11 01:30:14', '2018-07-26 01:30:14', 40032, 'chicago-red.png', null, null, 'Kanoodle', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (407, 3, 1683, 'ac nibh fusce lacus', 'In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.', '2018-04-03 10:39:18', '2018-05-03 10:39:18', '2018-05-18 10:39:18', 10004, 'aberdeen-green.png', null, null, 'Midel', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (408, 1, 2590, 'pede morbi porttitor lorem', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', '2018-06-11 06:48:25', '2018-07-11 06:48:25', '2018-07-26 06:48:25', 40001, 'aberdeen-green.png', null, null, 'Fivebridge', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (409, 4, 5076, 'nulla dapibus dolor vel est', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', '2018-07-13 09:26:30', '2018-08-13 09:26:30', '2018-08-28 09:26:30', 5021, 'aberdeen-green.png', null, null, 'Rhycero', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (410, 2, 4096, 'primis in faucibus orci', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', '2018-07-08 19:36:12', '2018-08-08 19:36:12', '2018-08-23 19:36:12', 20033, 'berlin-blue.png', null, null, 'Digitube', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (411, 3, 4529, 'vel pede morbi porttitor', 'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', '2018-02-16 08:04:59', '2018-03-16 08:04:59', '2018-03-31 08:04:59', 10038, 'amsterdam-orange.png', null, null, 'Dynabox', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (412, 2, 7585, 'vel pede morbi porttitor', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.', '2018-12-06 00:44:21', '2019-01-06 00:44:21', '2019-01-21 00:44:21', 20044, 'amsterdam-orange.png', null, null, 'Vinder', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (413, 2, 2810, 'nulla sed accumsan', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', '2018-06-12 10:31:24', '2018-07-12 10:31:24', '2018-07-27 10:31:24', 20042, 'berlin-blue.png', null, null, 'Devshare', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (414, 1, 2826, 'augue quam sollicitudin vitae consectetuer', 'In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', '2018-08-23 05:18:25', '2018-09-23 05:18:25', '2018-10-08 05:18:25', 40032, 'berlin-blue.png', null, null, 'Wikivu', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (415, 1, 6027, 'non velit nec nisi vulputate', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', '2018-09-02 12:51:26', '2018-10-02 12:51:26', '2018-10-17 12:51:26', 40008, 'chicago-red.png', null, null, 'Skynoodle', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (416, 2, 7729, 'a nibh in quis', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.

Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.

Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', '2018-09-12 12:44:28', '2018-10-12 12:44:28', '2018-10-27 12:44:28', 20039, 'chicago-red.png', null, null, 'Twitterbeat', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (417, 3, 7006, 'etiam justo etiam pretium iaculis', 'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', '2018-06-03 14:55:03', '2018-07-03 14:55:03', '2018-07-18 14:55:03', 10011, 'chicago-red.png', null, null, 'Riffpath', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (418, 4, 7357, 'consectetuer adipiscing elit proin', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.', '2018-06-12 01:11:04', '2018-07-12 01:11:04', '2018-07-27 01:11:04', 5017, 'aberdeen-green.png', null, null, 'Brainbox', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (419, 3, 4778, 'ac nulla sed vel enim', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', '2018-06-08 22:43:35', '2018-07-08 22:43:35', '2018-07-23 22:43:35', 10020, 'berlin-blue.png', null, null, 'Trudoo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (420, 2, 1865, 'quam nec dui', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', '2018-06-03 00:24:44', '2018-07-03 00:24:44', '2018-07-18 00:24:44', 20022, 'chicago-red.png', null, null, 'Rhyloo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (421, 2, 2538, 'justo maecenas rhoncus', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', '2018-08-11 07:21:22', '2018-09-11 07:21:22', '2018-09-26 07:21:22', 20026, 'chicago-red.png', null, null, 'Topicshots', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (422, 3, 5754, 'vulputate vitae nisl aenean lectus pellentesque', 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.', '2018-11-06 08:44:59', '2018-12-06 08:44:59', '2018-12-21 08:44:59', 10039, 'amsterdam-orange.png', null, null, 'Edgeify', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (423, 2, 6389, 'volutpat eleifend donec ut dolor morbi', 'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', '2018-02-09 15:43:32', '2018-03-09 15:43:32', '2018-03-24 15:43:32', 20022, 'chicago-red.png', null, null, 'Trudoo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (424, 3, 5613, 'consequat lectus in est risus', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', '2018-04-04 01:02:08', '2018-05-04 01:02:08', '2018-05-19 01:02:08', 10013, 'berlin-blue.png', null, null, 'Devify', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (425, 3, 8114, 'aenean sit amet justo morbi', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.', '2018-11-13 18:15:14', '2018-12-13 18:15:14', '2018-12-28 18:15:14', 10039, 'chicago-red.png', null, null, 'Oyoyo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (426, 2, 5474, 'parturient montes nascetur ridiculus mus etiam', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', '2018-07-09 17:15:11', '2018-08-09 17:15:11', '2018-08-24 17:15:11', 20038, 'amsterdam-orange.png', null, null, 'Podcat', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (427, 2, 2258, 'id sapien in sapien iaculis congue', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', '2018-06-12 13:51:14', '2018-07-12 13:51:14', '2018-07-27 13:51:14', 20039, 'amsterdam-orange.png', null, null, 'Vinder', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (428, 1, 366, 'ac nulla sed', 'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', '2018-07-30 04:08:35', '2018-08-30 04:08:35', '2018-09-14 04:08:35', 40027, 'chicago-red.png', null, null, 'Trilith', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (429, 4, 4379, 'dapibus duis at velit eu est', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', '2018-05-05 17:07:30', '2018-06-05 17:07:30', '2018-06-20 17:07:30', 5036, 'aberdeen-green.png', null, null, 'Jetpulse', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (430, 2, 4926, 'habitasse platea dictumst maecenas', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.', '2018-11-19 08:34:30', '2018-12-19 08:34:30', '2019-01-03 08:34:30', 20004, 'aberdeen-green.png', null, null, 'Photojam', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (431, 3, 5337, 'non velit donec diam neque vestibulum', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', '2018-04-24 23:31:06', '2018-05-24 23:31:06', '2018-06-08 23:31:06', 10001, 'chicago-red.png', null, null, 'Skiba', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (432, 3, 6784, 'turpis adipiscing lorem vitae mattis nibh', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', '2018-06-13 04:03:08', '2018-07-13 04:03:08', '2018-07-28 04:03:08', 10038, 'aberdeen-green.png', null, null, 'Wikizz', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (433, 2, 1018, 'ipsum ac tellus semper interdum mauris', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', '2018-12-03 06:52:09', '2019-01-03 06:52:09', '2019-01-18 06:52:09', 20049, 'berlin-blue.png', null, null, 'Brightbean', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (434, 4, 7498, 'nam dui proin', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', '2018-07-26 22:05:02', '2018-08-26 22:05:02', '2018-09-10 22:05:02', 5036, 'berlin-blue.png', null, null, 'Yadel', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (435, 4, 4569, 'vestibulum ante ipsum primis in faucibus', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.', '2018-09-18 05:46:41', '2018-10-18 05:46:41', '2018-11-02 05:46:41', 5027, 'chicago-red.png', null, null, 'Talane', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (436, 3, 7082, 'nulla suscipit ligula in', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.', '2018-07-16 05:23:56', '2018-08-16 05:23:56', '2018-08-31 05:23:56', 10009, 'chicago-red.png', null, null, 'Yata', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (437, 1, 7930, 'nullam varius nulla facilisi cras', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', '2018-09-22 23:04:54', '2018-10-22 23:04:54', '2018-11-06 23:04:54', 40033, 'chicago-red.png', null, null, 'Twitterbeat', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (438, 1, 1093, 'et magnis dis parturient montes', 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', '2018-07-02 07:49:24', '2018-08-02 07:49:24', '2018-08-17 07:49:24', 40043, 'berlin-blue.png', null, null, 'Feedbug', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (439, 3, 3706, 'auctor gravida sem praesent', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', '2018-10-27 18:10:04', '2018-11-27 18:10:04', '2018-12-12 18:10:04', 10045, 'amsterdam-orange.png', null, null, 'Blogspan', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (440, 2, 7835, 'ligula in lacus curabitur at ipsum', 'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.

Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', '2018-08-24 17:51:49', '2018-09-24 17:51:49', '2018-10-09 17:51:49', 20020, 'amsterdam-orange.png', null, null, 'Mydo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (441, 3, 1251, 'enim lorem ipsum dolor sit amet', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', '2018-10-07 20:28:56', '2018-11-07 20:28:56', '2018-11-22 20:28:56', 10023, 'berlin-blue.png', null, null, 'Twinder', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (442, 2, 4394, 'in est risus auctor sed tristique', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.', '2018-03-04 06:47:21', '2018-04-04 06:47:21', '2018-04-19 06:47:21', 20032, 'berlin-blue.png', null, null, 'Agivu', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (443, 1, 5317, 'pretium nisl ut volutpat sapien', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', '2018-03-21 15:44:32', '2018-04-21 15:44:32', '2018-05-06 15:44:32', 40008, 'amsterdam-orange.png', null, null, 'Centizu', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (444, 3, 6930, 'sagittis nam congue risus', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', '2018-09-22 18:37:13', '2018-10-22 18:37:13', '2018-11-06 18:37:13', 10015, 'aberdeen-green.png', null, null, 'Oozz', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (445, 1, 750, 'eros vestibulum ac est lacinia nisi', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.', '2018-09-10 22:47:32', '2018-10-10 22:47:32', '2018-10-25 22:47:32', 40046, 'amsterdam-orange.png', null, null, 'Flashspan', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (446, 2, 2754, 'nonummy maecenas tincidunt lacus at', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.', '2018-04-09 23:45:48', '2018-05-09 23:45:48', '2018-05-24 23:45:48', 20007, 'aberdeen-green.png', null, null, 'Skalith', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (447, 3, 3828, 'magna at nunc commodo placerat praesent', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', '2018-09-19 23:10:48', '2018-10-19 23:10:48', '2018-11-03 23:10:48', 10006, 'aberdeen-green.png', null, null, 'Gabtune', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (448, 4, 3232, 'phasellus sit amet erat nulla tempus', 'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', '2018-07-13 16:56:19', '2018-08-13 16:56:19', '2018-08-28 16:56:19', 5024, 'berlin-blue.png', null, null, 'Feedfire', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (449, 1, 5654, 'ultrices enim lorem', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', '2018-08-20 23:44:54', '2018-09-20 23:44:54', '2018-10-05 23:44:54', 40031, 'aberdeen-green.png', null, null, 'Skyvu', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (450, 4, 237, 'magna vulputate luctus cum', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', '2018-05-17 18:41:53', '2018-06-17 18:41:53', '2018-07-02 18:41:53', 5030, 'aberdeen-green.png', null, null, 'Eidel', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (451, 2, 943, 'in felis eu', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', '2018-07-19 15:37:41', '2018-08-19 15:37:41', '2018-09-03 15:37:41', 20001, 'amsterdam-orange.png', null, null, 'Topicshots', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (452, 1, 5451, 'mauris viverra diam vitae quam suspendisse', 'Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', '2018-08-31 13:11:47', '2018-09-30 13:11:47', '2018-10-15 13:11:47', 40006, 'berlin-blue.png', null, null, 'Skynoodle', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (453, 4, 1882, 'in congue etiam justo etiam pretium', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.

Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.

Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', '2018-10-09 11:21:04', '2018-11-09 11:21:04', '2018-11-24 11:21:04', 5003, 'chicago-red.png', null, null, 'Npath', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (454, 3, 2065, 'vestibulum ante ipsum primis in', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', '2018-08-02 23:43:27', '2018-09-02 23:43:27', '2018-09-17 23:43:27', 10026, 'chicago-red.png', null, null, 'BlogXS', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (455, 4, 3221, 'cras pellentesque volutpat', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.', '2018-02-10 04:59:41', '2018-03-10 04:59:41', '2018-03-25 04:59:41', 5009, 'berlin-blue.png', null, null, 'Thoughtmix', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (456, 4, 4780, 'potenti nullam porttitor lacus at', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', '2018-03-30 05:21:34', '2018-04-30 05:21:34', '2018-05-15 05:21:34', 5050, 'berlin-blue.png', null, null, 'Photojam', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (457, 1, 3446, 'ligula nec sem duis', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', '2018-12-21 20:14:00', '2019-01-21 20:14:00', '2019-02-05 20:14:00', 40021, 'berlin-blue.png', null, null, 'Ntag', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (458, 3, 4760, 'tellus nisi eu orci', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', '2018-01-28 15:16:07', '2018-02-28 15:16:07', '2018-03-15 15:16:07', 10002, 'berlin-blue.png', null, null, 'Devshare', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (459, 3, 7218, 'suspendisse ornare consequat', 'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', '2018-06-10 01:07:32', '2018-07-10 01:07:32', '2018-07-25 01:07:32', 10043, 'chicago-red.png', null, null, 'Aibox', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (460, 1, 1183, 'donec quis orci eget orci vehicula', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', '2018-04-01 21:10:58', '2018-05-01 21:10:58', '2018-05-16 21:10:58', 40022, 'amsterdam-orange.png', null, null, 'Jabbercube', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (461, 1, 5283, 'elementum pellentesque quisque porta volutpat erat', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.', '2018-11-30 12:33:16', '2018-12-30 12:33:16', '2019-01-14 12:33:16', 40020, 'chicago-red.png', null, null, 'Twinder', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (462, 4, 1627, 'amet turpis elementum ligula vehicula', 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.

Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', '2018-02-12 10:04:49', '2018-03-12 10:04:49', '2018-03-27 10:04:49', 5015, 'chicago-red.png', null, null, 'Eimbee', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (463, 3, 8068, 'justo etiam pretium iaculis justo', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', '2018-11-26 03:27:42', '2018-12-26 03:27:42', '2019-01-10 03:27:42', 10035, 'aberdeen-green.png', null, null, 'Gigabox', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (464, 1, 7648, 'ultrices enim lorem', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', '2018-12-11 08:43:04', '2019-01-11 08:43:04', '2019-01-26 08:43:04', 40001, 'amsterdam-orange.png', null, null, 'Demimbu', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (465, 2, 5843, 'nullam porttitor lacus at', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.', '2018-08-24 20:52:38', '2018-09-24 20:52:38', '2018-10-09 20:52:38', 20035, 'amsterdam-orange.png', null, null, 'Divape', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (466, 2, 5933, 'vel pede morbi porttitor lorem', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', '2018-02-23 18:13:09', '2018-03-23 18:13:09', '2018-04-07 18:13:09', 20031, 'berlin-blue.png', null, null, 'Realcube', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (467, 3, 3506, 'cursus urna ut tellus nulla ut', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.', '2018-01-26 11:05:55', '2018-02-26 11:05:55', '2018-03-13 11:05:55', 10039, 'chicago-red.png', null, null, 'Talane', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (468, 2, 6170, 'mauris non ligula pellentesque ultrices phasellus', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.', '2018-03-22 07:09:22', '2018-04-22 07:09:22', '2018-05-07 07:09:22', 20050, 'aberdeen-green.png', null, null, 'Zoozzy', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (469, 2, 7066, 'aliquam convallis nunc proin at', 'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.', '2018-02-07 23:00:09', '2018-03-07 23:00:09', '2018-03-22 23:00:09', 20001, 'amsterdam-orange.png', null, null, 'Skivee', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (470, 4, 4083, 'in sapien iaculis congue vivamus', 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', '2018-11-21 03:21:42', '2018-12-21 03:21:42', '2019-01-05 03:21:42', 5011, 'chicago-red.png', null, null, 'Zoomlounge', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (471, 1, 1296, 'nulla neque libero convallis eget', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.', '2018-03-28 14:14:05', '2018-04-28 14:14:05', '2018-05-13 14:14:05', 40008, 'aberdeen-green.png', null, null, 'Bluejam', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (472, 1, 4950, 'interdum mauris non ligula', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', '2018-08-17 04:41:19', '2018-09-17 04:41:19', '2018-10-02 04:41:19', 40050, 'aberdeen-green.png', null, null, 'Latz', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (473, 4, 3374, 'nisl duis ac nibh fusce', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.', '2018-06-11 08:01:17', '2018-07-11 08:01:17', '2018-07-26 08:01:17', 5036, 'berlin-blue.png', null, null, 'Tagopia', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (474, 3, 3908, 'mollis molestie lorem quisque ut', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', '2018-02-16 02:54:18', '2018-03-16 02:54:18', '2018-03-31 02:54:18', 10030, 'amsterdam-orange.png', null, null, 'Oyoyo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (475, 4, 5384, 'a pede posuere nonummy integer non', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', '2018-05-24 18:25:40', '2018-06-24 18:25:40', '2018-07-09 18:25:40', 5023, 'amsterdam-orange.png', null, null, 'Buzzbean', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (476, 3, 3442, 'in quis justo maecenas', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', '2018-11-17 23:22:49', '2018-12-17 23:22:49', '2019-01-01 23:22:49', 10010, 'aberdeen-green.png', null, null, 'Tagpad', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (477, 3, 5515, 'velit donec diam neque', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', '2018-02-19 23:36:06', '2018-03-19 23:36:06', '2018-04-03 23:36:06', 10004, 'chicago-red.png', null, null, 'Babbleset', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (478, 1, 2644, 'morbi sem mauris laoreet ut', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.', '2018-07-31 07:58:02', '2018-08-31 07:58:02', '2018-09-15 07:58:02', 40034, 'berlin-blue.png', null, null, 'Dazzlesphere', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (479, 3, 393, 'lacinia nisi venenatis', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.', '2018-08-02 16:06:24', '2018-09-02 16:06:24', '2018-09-17 16:06:24', 10013, 'aberdeen-green.png', null, null, 'Kwilith', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (480, 2, 7185, 'platea dictumst morbi vestibulum', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', '2018-09-15 06:09:05', '2018-10-15 06:09:05', '2018-10-30 06:09:05', 20050, 'berlin-blue.png', null, null, 'Mynte', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (481, 3, 622, 'turpis adipiscing lorem vitae', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', '2018-10-19 08:37:14', '2018-11-19 08:37:14', '2018-12-04 08:37:14', 10037, 'berlin-blue.png', null, null, 'Edgetag', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (482, 1, 3746, 'rutrum at lorem integer tincidunt ante', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', '2018-05-15 09:13:14', '2018-06-15 09:13:14', '2018-06-30 09:13:14', 40015, 'chicago-red.png', null, null, 'Fiveclub', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (483, 1, 680, 'tincidunt nulla mollis molestie lorem quisque', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', '2018-03-19 22:43:58', '2018-04-19 22:43:58', '2018-05-04 22:43:58', 40014, 'aberdeen-green.png', null, null, 'Thoughtstorm', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (484, 4, 1651, 'lacinia sapien quis libero', 'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.

Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', '2018-10-02 20:54:42', '2018-11-02 20:54:42', '2018-11-17 20:54:42', 5004, 'chicago-red.png', null, null, 'Gigaclub', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (485, 2, 6773, 'suspendisse potenti in eleifend quam', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', '2018-04-02 01:52:08', '2018-05-02 01:52:08', '2018-05-17 01:52:08', 20003, 'amsterdam-orange.png', null, null, 'BlogXS', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (486, 2, 4809, 'metus arcu adipiscing', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', '2018-09-22 16:47:47', '2018-10-22 16:47:47', '2018-11-06 16:47:47', 20020, 'chicago-red.png', null, null, 'Riffwire', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (487, 2, 3834, 'eu mi nulla ac enim', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', '2018-05-03 00:33:34', '2018-06-03 00:33:34', '2018-06-18 00:33:34', 20006, 'aberdeen-green.png', null, null, 'Flashset', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (488, 4, 6224, 'erat curabitur gravida', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.', '2018-02-06 18:23:39', '2018-03-06 18:23:39', '2018-03-21 18:23:39', 5017, 'aberdeen-green.png', null, null, 'Yambee', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (489, 4, 2249, 'dignissim vestibulum vestibulum ante', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', '2018-02-25 06:20:26', '2018-03-25 06:20:26', '2018-04-09 06:20:26', 5048, 'chicago-red.png', null, null, 'Twitterworks', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (490, 1, 6522, 'nulla eget eros elementum pellentesque', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', '2018-09-28 14:30:34', '2018-10-28 14:30:34', '2018-11-12 14:30:34', 40014, 'amsterdam-orange.png', null, null, 'Midel', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (491, 3, 272, 'dui maecenas tristique est et tempus', 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', '2018-11-09 07:30:15', '2018-12-09 07:30:15', '2018-12-24 07:30:15', 10032, 'chicago-red.png', null, null, 'Yambee', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (492, 1, 6035, 'bibendum felis sed interdum venenatis turpis', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', '2018-01-24 11:31:08', '2018-02-24 11:31:08', '2018-03-11 11:31:08', 40032, 'amsterdam-orange.png', null, null, 'Kwilith', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (493, 2, 3333, 'ut volutpat sapien arcu sed augue', 'In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', '2018-05-06 23:53:02', '2018-06-06 23:53:02', '2018-06-21 23:53:02', 20017, 'aberdeen-green.png', null, null, 'Zoomzone', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (494, 3, 3549, 'vitae quam suspendisse potenti', 'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', '2018-04-23 13:14:52', '2018-05-23 13:14:52', '2018-06-07 13:14:52', 10020, 'berlin-blue.png', null, null, 'Babblestorm', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (495, 4, 4857, 'justo etiam pretium iaculis justo in', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', '2018-08-21 02:29:41', '2018-09-21 02:29:41', '2018-10-06 02:29:41', 5050, 'berlin-blue.png', null, null, 'Realpoint', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (496, 1, 2149, 'ut nunc vestibulum ante', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', '2018-08-09 16:44:02', '2018-09-09 16:44:02', '2018-09-24 16:44:02', 40037, 'amsterdam-orange.png', null, null, 'Tagtune', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (497, 4, 1538, 'lobortis ligula sit amet eleifend pede', 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', '2018-06-27 12:29:10', '2018-07-27 12:29:10', '2018-08-11 12:29:10', 5012, 'aberdeen-green.png', null, null, 'Quire', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (498, 3, 1364, 'a ipsum integer a nibh', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.', '2018-11-11 21:54:40', '2018-12-11 21:54:40', '2018-12-26 21:54:40', 10019, 'berlin-blue.png', null, null, 'Eare', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (499, 1, 6789, 'iaculis diam erat', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.', '2018-08-30 05:54:09', '2018-09-30 05:54:09', '2018-10-15 05:54:09', 40046, 'aberdeen-green.png', null, null, 'Fatz', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (500, 3, 4571, 'posuere nonummy integer non velit donec', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', '2018-10-07 20:39:18', '2018-11-07 20:39:18', '2018-11-22 20:39:18', 10015, 'berlin-blue.png', null, null, 'Oyoloo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (501, 2, 6120, 'nunc proin at turpis a pede', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', '2018-08-23 01:50:39', '2018-09-23 01:50:39', '2018-10-08 01:50:39', 20038, 'berlin-blue.png', null, null, 'Flashdog', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (502, 1, 782, 'in tempus sit amet sem fusce', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.', '2018-02-09 21:22:48', '2018-03-09 21:22:48', '2018-03-24 21:22:48', 40023, 'berlin-blue.png', null, null, 'Twimbo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (503, 1, 1668, 'amet eleifend pede', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', '2018-04-02 15:25:52', '2018-05-02 15:25:52', '2018-05-17 15:25:52', 40028, 'aberdeen-green.png', null, null, 'Ntag', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (504, 1, 8086, 'eros vestibulum ac est', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', '2018-12-17 22:25:35', '2019-01-17 22:25:35', '2019-02-01 22:25:35', 40024, 'berlin-blue.png', null, null, 'Voonte', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (505, 2, 650, 'sit amet justo morbi ut odio', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', '2018-10-31 04:09:44', '2018-11-30 04:09:44', '2018-12-15 04:09:44', 20040, 'amsterdam-orange.png', null, null, 'Quinu', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (506, 2, 6928, 'purus sit amet', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', '2018-05-13 12:59:32', '2018-06-13 12:59:32', '2018-06-28 12:59:32', 20023, 'aberdeen-green.png', null, null, 'Wikivu', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (507, 3, 1855, 'fusce consequat nulla', 'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', '2018-12-12 20:05:07', '2019-01-12 20:05:07', '2019-01-27 20:05:07', 10015, 'aberdeen-green.png', null, null, 'Zoonder', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (508, 4, 5097, 'lectus vestibulum quam', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', '2018-04-03 14:49:41', '2018-05-03 14:49:41', '2018-05-18 14:49:41', 5000, 'aberdeen-green.png', null, null, 'Yakijo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (509, 1, 6865, 'sed justo pellentesque viverra pede', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', '2018-01-22 12:54:15', '2018-02-22 12:54:15', '2018-03-09 12:54:15', 40039, 'amsterdam-orange.png', null, null, 'Babblestorm', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (510, 4, 1217, 'nam nulla integer', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.', '2018-01-10 20:55:00', '2018-02-10 20:55:00', '2018-02-25 20:55:00', 5022, 'chicago-red.png', null, null, 'Meembee', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (511, 3, 5581, 'in faucibus orci', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', '2018-08-13 16:39:09', '2018-09-13 16:39:09', '2018-09-28 16:39:09', 10023, 'chicago-red.png', null, null, 'Brainbox', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (512, 1, 1978, 'eros elementum pellentesque quisque porta', 'In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', '2018-05-27 10:07:12', '2018-06-27 10:07:12', '2018-07-12 10:07:12', 40029, 'aberdeen-green.png', null, null, 'Kwinu', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (513, 1, 3074, 'est phasellus sit amet erat', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', '2018-11-06 06:53:56', '2018-12-06 06:53:56', '2018-12-21 06:53:56', 40036, 'chicago-red.png', null, null, 'Dablist', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (514, 4, 4104, 'ante vestibulum ante ipsum primis in', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', '2018-01-11 19:06:23', '2018-02-11 19:06:23', '2018-02-26 19:06:23', 5049, 'berlin-blue.png', null, null, 'Jazzy', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (515, 3, 3377, 'augue quam sollicitudin vitae consectetuer eget', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.', '2018-02-21 23:35:34', '2018-03-21 23:35:34', '2018-04-05 23:35:34', 10019, 'berlin-blue.png', null, null, 'Mydeo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (516, 2, 7570, 'libero rutrum ac lobortis', 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.', '2018-07-26 21:15:13', '2018-08-26 21:15:13', '2018-09-10 21:15:13', 20021, 'berlin-blue.png', null, null, 'Livetube', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (517, 3, 6586, 'varius nulla facilisi cras non', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.', '2018-03-18 04:58:17', '2018-04-18 04:58:17', '2018-05-03 04:58:17', 10030, 'aberdeen-green.png', null, null, 'Realpoint', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (518, 3, 1592, 'leo odio condimentum id luctus nec', 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', '2018-05-23 10:59:08', '2018-06-23 10:59:08', '2018-07-08 10:59:08', 10023, 'aberdeen-green.png', null, null, 'Brightbean', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (519, 4, 3765, 'curae mauris viverra diam vitae', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', '2018-06-04 20:49:06', '2018-07-04 20:49:06', '2018-07-19 20:49:06', 5014, 'berlin-blue.png', null, null, 'Jazzy', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (520, 1, 7318, 'at feugiat non pretium quis', 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', '2018-11-24 01:43:17', '2018-12-24 01:43:17', '2019-01-08 01:43:17', 40039, 'amsterdam-orange.png', null, null, 'Shuffletag', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (521, 1, 2242, 'consequat nulla nisl nunc nisl duis', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', '2018-01-16 11:16:38', '2018-02-16 11:16:38', '2018-03-03 11:16:38', 40032, 'berlin-blue.png', null, null, 'Yombu', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (522, 2, 3831, 'lacinia aenean sit amet', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', '2018-08-29 20:32:53', '2018-09-29 20:32:53', '2018-10-14 20:32:53', 20013, 'aberdeen-green.png', null, null, 'Yoveo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (523, 1, 6139, 'nisi venenatis tristique fusce congue', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', '2018-10-27 06:03:06', '2018-11-27 06:03:06', '2018-12-12 06:03:06', 40009, 'amsterdam-orange.png', null, null, 'Flashdog', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (524, 3, 5938, 'urna ut tellus nulla ut erat', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.

Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', '2018-10-13 09:21:26', '2018-11-13 09:21:26', '2018-11-28 09:21:26', 10025, 'chicago-red.png', null, null, 'Oba', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (525, 3, 6679, 'turpis elementum ligula vehicula consequat', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', '2018-08-28 05:24:19', '2018-09-28 05:24:19', '2018-10-13 05:24:19', 10038, 'aberdeen-green.png', null, null, 'Flipbug', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (526, 1, 6331, 'nisl duis bibendum', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', '2018-08-19 02:08:52', '2018-09-19 02:08:52', '2018-10-04 02:08:52', 40021, 'amsterdam-orange.png', null, null, 'Gigashots', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (527, 1, 6214, 'luctus rutrum nulla', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.', '2018-11-03 12:16:54', '2018-12-03 12:16:54', '2018-12-18 12:16:54', 40023, 'berlin-blue.png', null, null, 'Gabvine', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (528, 2, 7783, 'integer aliquet massa id lobortis', 'In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.', '2018-04-14 12:58:52', '2018-05-14 12:58:52', '2018-05-29 12:58:52', 20007, 'amsterdam-orange.png', null, null, 'Brightbean', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (529, 2, 2746, 'nibh fusce lacus purus', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', '2018-02-06 07:18:03', '2018-03-06 07:18:03', '2018-03-21 07:18:03', 20041, 'amsterdam-orange.png', null, null, 'Feedfire', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (530, 2, 7493, 'justo eu massa donec dapibus duis', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', '2018-06-22 07:44:25', '2018-07-22 07:44:25', '2018-08-06 07:44:25', 20000, 'chicago-red.png', null, null, 'Trudeo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (531, 2, 3087, 'purus sit amet nulla quisque', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', '2018-07-10 11:53:49', '2018-08-10 11:53:49', '2018-08-25 11:53:49', 20003, 'berlin-blue.png', null, null, 'Rhyzio', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (532, 2, 8109, 'lectus in est risus', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', '2018-11-27 23:27:46', '2018-12-27 23:27:46', '2019-01-11 23:27:46', 20049, 'chicago-red.png', null, null, 'Abata', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (533, 3, 1332, 'libero rutrum ac', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', '2018-09-25 00:51:58', '2018-10-25 00:51:58', '2018-11-09 00:51:58', 10038, 'berlin-blue.png', null, null, 'Devpoint', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (534, 3, 1831, 'pellentesque ultrices mattis', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', '2018-05-27 01:14:01', '2018-06-27 01:14:01', '2018-07-12 01:14:01', 10011, 'chicago-red.png', null, null, 'Skinder', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (535, 3, 1149, 'imperdiet nullam orci pede', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', '2018-10-08 05:35:44', '2018-11-08 05:35:44', '2018-11-23 05:35:44', 10037, 'berlin-blue.png', null, null, 'Dabvine', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (536, 2, 940, 'et commodo vulputate justo in blandit', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', '2018-01-08 23:24:54', '2018-02-08 23:24:54', '2018-02-23 23:24:54', 20050, 'berlin-blue.png', null, null, 'Jayo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (537, 1, 2293, 'congue etiam justo etiam pretium iaculis', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', '2018-05-11 17:52:18', '2018-06-11 17:52:18', '2018-06-26 17:52:18', 40035, 'chicago-red.png', null, null, 'Thoughtmix', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (538, 2, 4943, 'sit amet nunc', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.', '2018-09-18 08:32:59', '2018-10-18 08:32:59', '2018-11-02 08:32:59', 20036, 'aberdeen-green.png', null, null, 'Leenti', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (539, 4, 5999, 'vulputate nonummy maecenas tincidunt lacus', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', '2018-02-28 10:34:13', '2018-03-28 10:34:13', '2018-04-12 10:34:13', 5028, 'amsterdam-orange.png', null, null, 'Trilith', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (540, 1, 6149, 'pulvinar lobortis est phasellus sit', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', '2018-11-03 01:48:07', '2018-12-03 01:48:07', '2018-12-18 01:48:07', 40041, 'aberdeen-green.png', null, null, 'Yombu', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (541, 2, 3383, 'tristique in tempus sit amet sem', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', '2018-06-29 02:34:47', '2018-07-29 02:34:47', '2018-08-13 02:34:47', 20043, 'amsterdam-orange.png', null, null, 'Skipfire', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (542, 4, 857, 'magna ac consequat metus sapien', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.

Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.

Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.', '2018-12-10 05:16:06', '2019-01-10 05:16:06', '2019-01-25 05:16:06', 5048, 'aberdeen-green.png', null, null, 'Gigashots', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (543, 3, 7222, 'mauris viverra diam vitae', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.', '2018-02-23 05:37:13', '2018-03-23 05:37:13', '2018-04-07 05:37:13', 10000, 'amsterdam-orange.png', null, null, 'Blogtags', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (544, 3, 3522, 'vestibulum sagittis sapien', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.', '2018-11-27 01:29:20', '2018-12-27 01:29:20', '2019-01-11 01:29:20', 10034, 'amsterdam-orange.png', null, null, 'Oloo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (545, 2, 2187, 'odio porttitor id', 'Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.', '2018-01-15 06:04:12', '2018-02-15 06:04:12', '2018-03-02 06:04:12', 20020, 'chicago-red.png', null, null, 'Divavu', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (546, 1, 7836, 'morbi a ipsum', 'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.

Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', '2018-03-30 11:51:06', '2018-04-30 11:51:06', '2018-05-15 11:51:06', 40004, 'berlin-blue.png', null, null, 'Twimbo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (547, 3, 1309, 'dolor sit amet consectetuer adipiscing elit', 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', '2018-05-28 00:52:32', '2018-06-28 00:52:32', '2018-07-13 00:52:32', 10032, 'chicago-red.png', null, null, 'Youopia', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (548, 4, 2466, 'ut tellus nulla', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.', '2018-10-24 12:19:21', '2018-11-24 12:19:21', '2018-12-09 12:19:21', 5037, 'aberdeen-green.png', null, null, 'Edgeblab', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (549, 3, 5851, 'cubilia curae donec pharetra magna vestibulum', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', '2018-12-09 08:41:58', '2019-01-09 08:41:58', '2019-01-24 08:41:58', 10027, 'berlin-blue.png', null, null, 'Zava', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (550, 1, 530, 'lobortis ligula sit', 'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.

Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', '2018-03-03 16:45:40', '2018-04-03 16:45:40', '2018-04-18 16:45:40', 40032, 'chicago-red.png', null, null, 'Babbleopia', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (551, 3, 2532, 'amet eros suspendisse', 'Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', '2018-05-19 03:03:04', '2018-06-19 03:03:04', '2018-07-04 03:03:04', 10025, 'berlin-blue.png', null, null, 'Shuffletag', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (552, 4, 1323, 'leo pellentesque ultrices', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', '2018-12-19 15:26:59', '2019-01-19 15:26:59', '2019-02-03 15:26:59', 5037, 'amsterdam-orange.png', null, null, 'Zoombox', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (553, 1, 7444, 'tempor turpis nec euismod scelerisque', 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.

Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', '2018-06-09 16:38:20', '2018-07-09 16:38:20', '2018-07-24 16:38:20', 40002, 'amsterdam-orange.png', null, null, 'Photobean', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (554, 4, 630, 'commodo vulputate justo in blandit', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.', '2018-03-29 09:54:20', '2018-04-29 09:54:20', '2018-05-14 09:54:20', 5037, 'berlin-blue.png', null, null, 'Yabox', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (555, 4, 3695, 'ut erat id', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', '2018-12-12 05:22:03', '2019-01-12 05:22:03', '2019-01-27 05:22:03', 5049, 'aberdeen-green.png', null, null, 'Eabox', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (556, 2, 6557, 'aliquam convallis nunc proin', 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.', '2018-05-07 21:28:04', '2018-06-07 21:28:04', '2018-06-22 21:28:04', 20019, 'amsterdam-orange.png', null, null, 'Yakidoo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (557, 3, 5588, 'quis turpis sed ante vivamus', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', '2018-04-17 12:17:16', '2018-05-17 12:17:16', '2018-06-01 12:17:16', 10022, 'aberdeen-green.png', null, null, 'Fadeo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (558, 4, 7748, 'tellus nulla ut erat id', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', '2018-06-24 08:33:11', '2018-07-24 08:33:11', '2018-08-08 08:33:11', 5050, 'amsterdam-orange.png', null, null, 'Brainlounge', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (559, 3, 1644, 'turpis enim blandit mi in', 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', '2018-01-09 03:27:37', '2018-02-09 03:27:37', '2018-02-24 03:27:37', 10013, 'aberdeen-green.png', null, null, 'Babbleblab', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (560, 4, 3512, 'cras in purus eu magna', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', '2018-03-28 23:34:04', '2018-04-28 23:34:04', '2018-05-13 23:34:04', 5024, 'amsterdam-orange.png', null, null, 'Twitterworks', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (561, 2, 5571, 'vestibulum ante ipsum primis in', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', '2018-10-12 04:04:05', '2018-11-12 04:04:05', '2018-11-27 04:04:05', 20036, 'berlin-blue.png', null, null, 'Roomm', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (562, 1, 3955, 'molestie hendrerit at vulputate vitae nisl', 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.', '2018-05-07 07:17:56', '2018-06-07 07:17:56', '2018-06-22 07:17:56', 40003, 'aberdeen-green.png', null, null, 'Vinder', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (563, 4, 7044, 'vel dapibus at diam', 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.', '2018-06-15 03:22:15', '2018-07-15 03:22:15', '2018-07-30 03:22:15', 5008, 'amsterdam-orange.png', null, null, 'Divavu', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (564, 4, 6758, 'curabitur gravida nisi at', 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.', '2018-03-13 04:26:02', '2018-04-13 04:26:02', '2018-04-28 04:26:02', 5035, 'amsterdam-orange.png', null, null, 'Browsezoom', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (565, 4, 4146, 'placerat praesent blandit nam nulla integer', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', '2018-11-09 00:11:17', '2018-12-09 00:11:17', '2018-12-24 00:11:17', 5035, 'amsterdam-orange.png', null, null, 'Ainyx', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (566, 2, 744, 'proin interdum mauris non', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', '2018-07-22 20:36:23', '2018-08-22 20:36:23', '2018-09-06 20:36:23', 20014, 'berlin-blue.png', null, null, 'Fatz', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (567, 2, 2147, 'justo in blandit ultrices enim', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', '2018-02-20 05:42:37', '2018-03-20 05:42:37', '2018-04-04 05:42:37', 20046, 'aberdeen-green.png', null, null, 'Topdrive', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (568, 4, 4648, 'ipsum ac tellus semper interdum mauris', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', '2018-06-18 15:57:50', '2018-07-18 15:57:50', '2018-08-02 15:57:50', 5028, 'aberdeen-green.png', null, null, 'Linkbridge', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (569, 1, 1070, 'feugiat et eros', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.', '2018-10-01 03:45:07', '2018-11-01 03:45:07', '2018-11-16 03:45:07', 40024, 'chicago-red.png', null, null, 'Dabfeed', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (570, 1, 4785, 'ac nulla sed vel enim sit', 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', '2018-12-15 14:02:12', '2019-01-15 14:02:12', '2019-01-30 14:02:12', 40011, 'berlin-blue.png', null, null, 'Vinte', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (571, 2, 1545, 'egestas metus aenean fermentum donec', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', '2018-01-22 14:08:11', '2018-02-22 14:08:11', '2018-03-09 14:08:11', 20001, 'chicago-red.png', null, null, 'Quatz', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (572, 4, 6697, 'vel nisl duis ac nibh fusce', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.', '2018-01-25 21:41:16', '2018-02-25 21:41:16', '2018-03-12 21:41:16', 5040, 'berlin-blue.png', null, null, 'Leenti', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (573, 2, 6093, 'accumsan felis ut at dolor quis', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', '2018-02-24 09:34:34', '2018-03-24 09:34:34', '2018-04-08 09:34:34', 20012, 'aberdeen-green.png', null, null, 'Zazio', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (574, 1, 6034, 'amet nulla quisque', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.', '2018-09-24 11:00:39', '2018-10-24 11:00:39', '2018-11-08 11:00:39', 40000, 'berlin-blue.png', null, null, 'Photolist', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (575, 2, 5196, 'ipsum aliquam non mauris morbi non', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', '2018-08-06 11:40:54', '2018-09-06 11:40:54', '2018-09-21 11:40:54', 20033, 'chicago-red.png', null, null, 'Miboo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (576, 3, 592, 'suscipit a feugiat et eros vestibulum', 'In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.', '2018-09-20 01:45:18', '2018-10-20 01:45:18', '2018-11-04 01:45:18', 10047, 'aberdeen-green.png', null, null, 'Zoonder', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (577, 3, 5965, 'ac diam cras pellentesque volutpat', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', '2018-05-11 00:01:19', '2018-06-11 00:01:19', '2018-06-26 00:01:19', 10001, 'aberdeen-green.png', null, null, 'Eimbee', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (578, 1, 7024, 'cras mi pede', 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', '2018-06-03 01:16:32', '2018-07-03 01:16:32', '2018-07-18 01:16:32', 40012, 'chicago-red.png', null, null, 'DabZ', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (579, 1, 2731, 'ipsum aliquam non', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.

Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.

Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.', '2018-04-05 14:19:16', '2018-05-05 14:19:16', '2018-05-20 14:19:16', 40011, 'amsterdam-orange.png', null, null, 'Gabspot', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (580, 1, 5955, 'at dolor quis odio consequat varius', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', '2018-12-08 16:59:02', '2019-01-08 16:59:02', '2019-01-23 16:59:02', 40038, 'amsterdam-orange.png', null, null, 'Gabvine', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (581, 2, 1241, 'id sapien in', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.', '2018-08-08 01:15:41', '2018-09-08 01:15:41', '2018-09-23 01:15:41', 20038, 'chicago-red.png', null, null, 'Kamba', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (582, 3, 6270, 'donec pharetra magna', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.', '2018-07-28 08:48:17', '2018-08-28 08:48:17', '2018-09-12 08:48:17', 10019, 'aberdeen-green.png', null, null, 'Muxo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (583, 3, 5983, 'ligula vehicula consequat', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', '2018-01-06 16:27:42', '2018-02-06 16:27:42', '2018-02-21 16:27:42', 10041, 'chicago-red.png', null, null, 'Pixope', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (584, 2, 1026, 'quam pede lobortis ligula sit amet', 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', '2018-04-18 11:02:02', '2018-05-18 11:02:02', '2018-06-02 11:02:02', 20041, 'amsterdam-orange.png', null, null, 'Rhybox', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (585, 1, 3926, 'turpis sed ante vivamus tortor', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', '2018-04-10 07:27:00', '2018-05-10 07:27:00', '2018-05-25 07:27:00', 40045, 'chicago-red.png', null, null, 'Divape', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (586, 3, 6607, 'placerat ante nulla justo aliquam', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', '2018-02-28 02:53:31', '2018-03-28 02:53:31', '2018-04-12 02:53:31', 10018, 'aberdeen-green.png', null, null, 'Bluejam', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (587, 1, 6351, 'eu massa donec', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', '2018-08-18 10:38:34', '2018-09-18 10:38:34', '2018-10-03 10:38:34', 40013, 'aberdeen-green.png', null, null, 'Zava', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (588, 2, 1748, 'nisl duis bibendum felis sed', 'Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', '2018-03-06 04:33:10', '2018-04-06 04:33:10', '2018-04-21 04:33:10', 20050, 'chicago-red.png', null, null, 'Feedfire', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (589, 3, 6698, 'cum sociis natoque penatibus et magnis', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', '2018-10-13 05:44:50', '2018-11-13 05:44:50', '2018-11-28 05:44:50', 10036, 'chicago-red.png', null, null, 'Trudeo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (590, 4, 4970, 'sollicitudin vitae consectetuer eget rutrum at', 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.', '2018-07-16 01:30:27', '2018-08-16 01:30:27', '2018-08-31 01:30:27', 5027, 'amsterdam-orange.png', null, null, 'Twitterlist', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (591, 1, 7622, 'fermentum donec ut mauris', 'In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.', '2018-04-26 10:53:28', '2018-05-26 10:53:28', '2018-06-10 10:53:28', 40014, 'berlin-blue.png', null, null, 'Feednation', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (592, 1, 1665, 'lacinia aenean sit amet', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', '2018-05-19 11:02:25', '2018-06-19 11:02:25', '2018-07-04 11:02:25', 40002, 'amsterdam-orange.png', null, null, 'Fiveclub', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (593, 3, 7296, 'convallis duis consequat dui nec', 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.', '2018-12-05 12:00:44', '2019-01-05 12:00:44', '2019-01-20 12:00:44', 10025, 'berlin-blue.png', null, null, 'Layo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (594, 2, 5281, 'sed lacus morbi sem', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.', '2018-03-25 01:49:50', '2018-04-25 01:49:50', '2018-05-10 01:49:50', 20049, 'aberdeen-green.png', null, null, 'Chatterpoint', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (595, 1, 4858, 'nullam molestie nibh in', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', '2018-06-21 05:33:42', '2018-07-21 05:33:42', '2018-08-05 05:33:42', 40005, 'berlin-blue.png', null, null, 'Wordtune', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (596, 3, 3737, 'curae nulla dapibus dolor vel', 'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', '2018-10-13 07:01:08', '2018-11-13 07:01:08', '2018-11-28 07:01:08', 10038, 'berlin-blue.png', null, null, 'Skipfire', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (597, 3, 2126, 'maecenas pulvinar lobortis est phasellus', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', '2018-11-04 07:20:56', '2018-12-04 07:20:56', '2018-12-19 07:20:56', 10015, 'chicago-red.png', null, null, 'Voolia', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (598, 2, 6444, 'aenean auctor gravida sem praesent id', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', '2018-07-16 21:00:45', '2018-08-16 21:00:45', '2018-08-31 21:00:45', 20035, 'amsterdam-orange.png', null, null, 'Twitternation', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (599, 3, 1926, 'tempus vel pede', 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.', '2018-07-07 02:05:32', '2018-08-07 02:05:32', '2018-08-22 02:05:32', 10044, 'berlin-blue.png', null, null, 'Realblab', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (600, 2, 5119, 'ligula vehicula consequat morbi', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', '2018-04-08 09:55:40', '2018-05-08 09:55:40', '2018-05-23 09:55:40', 20006, 'chicago-red.png', null, null, 'Rhycero', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (601, 3, 1354, 'nisl nunc rhoncus dui', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', '2018-12-18 23:18:40', '2019-01-18 23:18:40', '2019-02-02 23:18:40', 10020, 'amsterdam-orange.png', null, null, 'Jamia', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (602, 4, 6678, 'posuere cubilia curae mauris', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', '2018-08-29 10:36:11', '2018-09-29 10:36:11', '2018-10-14 10:36:11', 5042, 'berlin-blue.png', null, null, 'Ainyx', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (603, 4, 5718, 'vulputate justo in', 'In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.', '2018-03-11 20:56:24', '2018-04-11 20:56:24', '2018-04-26 20:56:24', 5031, 'berlin-blue.png', null, null, 'Agivu', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (604, 1, 830, 'platea dictumst maecenas', 'Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.', '2018-08-01 22:15:34', '2018-09-01 22:15:34', '2018-09-16 22:15:34', 40027, 'chicago-red.png', null, null, 'Jayo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (605, 1, 6064, 'viverra eget congue eget semper', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', '2018-05-09 00:45:48', '2018-06-09 00:45:48', '2018-06-24 00:45:48', 40016, 'chicago-red.png', null, null, 'Flashpoint', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (606, 4, 1674, 'a suscipit nulla elit ac', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', '2018-07-02 12:04:42', '2018-08-02 12:04:42', '2018-08-17 12:04:42', 5011, 'amsterdam-orange.png', null, null, 'Jaxspan', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (607, 4, 7339, 'integer aliquet massa id lobortis', 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.', '2018-06-18 13:18:12', '2018-07-18 13:18:12', '2018-08-02 13:18:12', 5050, 'amsterdam-orange.png', null, null, 'Eazzy', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (608, 3, 7570, 'pede lobortis ligula sit', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', '2018-08-16 10:11:48', '2018-09-16 10:11:48', '2018-10-01 10:11:48', 10047, 'aberdeen-green.png', null, null, 'Trudoo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (609, 2, 7863, 'in lectus pellentesque at', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', '2018-03-30 03:01:34', '2018-04-30 03:01:34', '2018-05-15 03:01:34', 20021, 'aberdeen-green.png', null, null, 'Yoveo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (610, 2, 6136, 'mus vivamus vestibulum sagittis', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.', '2018-07-20 23:22:36', '2018-08-20 23:22:36', '2018-09-04 23:22:36', 20007, 'amsterdam-orange.png', null, null, 'Tagfeed', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (611, 3, 7504, 'pretium nisl ut volutpat sapien', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', '2018-09-23 05:24:10', '2018-10-23 05:24:10', '2018-11-07 05:24:10', 10005, 'chicago-red.png', null, null, 'Feedfish', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (612, 2, 3022, 'sit amet nulla quisque arcu libero', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.', '2018-05-14 11:06:47', '2018-06-14 11:06:47', '2018-06-29 11:06:47', 20039, 'chicago-red.png', null, null, 'Mudo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (613, 1, 6795, 'duis ac nibh fusce', 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', '2018-06-03 12:36:35', '2018-07-03 12:36:35', '2018-07-18 12:36:35', 40025, 'chicago-red.png', null, null, 'Roomm', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (614, 1, 1040, 'dictumst morbi vestibulum velit id', 'In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', '2018-03-07 14:48:20', '2018-04-07 14:48:20', '2018-04-22 14:48:20', 40010, 'berlin-blue.png', null, null, 'Buzzster', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (615, 1, 5051, 'nulla ac enim in tempor turpis', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.', '2018-01-23 04:13:14', '2018-02-23 04:13:14', '2018-03-10 04:13:14', 40008, 'chicago-red.png', null, null, 'Kazu', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (616, 2, 7311, 'vulputate luctus cum sociis', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', '2018-11-16 01:34:51', '2018-12-16 01:34:51', '2018-12-31 01:34:51', 20002, 'chicago-red.png', null, null, 'Feedspan', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (617, 3, 2807, 'justo sollicitudin ut suscipit', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', '2018-09-13 18:16:39', '2018-10-13 18:16:39', '2018-10-28 18:16:39', 10000, 'amsterdam-orange.png', null, null, 'Zazio', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (618, 4, 6089, 'ac diam cras', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', '2018-01-16 12:12:59', '2018-02-16 12:12:59', '2018-03-03 12:12:59', 5045, 'amsterdam-orange.png', null, null, 'Kwilith', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (619, 2, 4558, 'nisl duis ac nibh fusce lacus', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', '2018-03-17 12:59:52', '2018-04-17 12:59:52', '2018-05-02 12:59:52', 20037, 'chicago-red.png', null, null, 'Blognation', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (620, 3, 3076, 'sapien quis libero nullam sit', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', '2018-05-09 16:38:06', '2018-06-09 16:38:06', '2018-06-24 16:38:06', 10050, 'aberdeen-green.png', null, null, 'Zoomlounge', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (621, 1, 412, 'dapibus duis at velit eu', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', '2018-01-09 11:43:14', '2018-02-09 11:43:14', '2018-02-24 11:43:14', 40046, 'chicago-red.png', null, null, 'Voolia', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (622, 2, 5316, 'duis consequat dui nec', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.', '2018-10-15 16:42:52', '2018-11-15 16:42:52', '2018-11-30 16:42:52', 20007, 'berlin-blue.png', null, null, 'Realblab', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (623, 2, 7313, 'sapien non mi', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', '2018-08-07 06:45:10', '2018-09-07 06:45:10', '2018-09-22 06:45:10', 20004, 'chicago-red.png', null, null, 'Yabox', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (624, 4, 1213, 'sapien cum sociis natoque', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', '2018-01-24 01:14:51', '2018-02-24 01:14:51', '2018-03-11 01:14:51', 5001, 'aberdeen-green.png', null, null, 'Yambee', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (625, 2, 2816, 'magna vestibulum aliquet ultrices', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.', '2018-04-07 09:58:59', '2018-05-07 09:58:59', '2018-05-22 09:58:59', 20010, 'berlin-blue.png', null, null, 'Jabbercube', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (626, 1, 3944, 'quisque porta volutpat erat quisque', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.

Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', '2018-09-12 12:35:08', '2018-10-12 12:35:08', '2018-10-27 12:35:08', 40033, 'chicago-red.png', null, null, 'Realblab', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (627, 4, 7818, 'venenatis lacinia aenean sit', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.', '2018-06-06 18:29:38', '2018-07-06 18:29:38', '2018-07-21 18:29:38', 5041, 'berlin-blue.png', null, null, 'Voolith', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (628, 4, 1744, 'ipsum primis in faucibus orci luctus', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.', '2018-04-13 08:07:42', '2018-05-13 08:07:42', '2018-05-28 08:07:42', 5039, 'amsterdam-orange.png', null, null, 'Kwideo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (629, 4, 1818, 'quis augue luctus tincidunt nulla mollis', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', '2018-11-24 16:42:55', '2018-12-24 16:42:55', '2019-01-08 16:42:55', 5022, 'chicago-red.png', null, null, 'Fivebridge', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (630, 1, 6361, 'in hac habitasse', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', '2018-12-22 10:00:15', '2019-01-22 10:00:15', '2019-02-06 10:00:15', 40020, 'aberdeen-green.png', null, null, 'Twitternation', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (631, 1, 6352, 'montes nascetur ridiculus mus etiam', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', '2018-10-08 11:51:30', '2018-11-08 11:51:30', '2018-11-23 11:51:30', 40033, 'chicago-red.png', null, null, 'Yadel', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (632, 3, 4588, 'duis at velit eu est', 'In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', '2018-09-10 00:32:47', '2018-10-10 00:32:47', '2018-10-25 00:32:47', 10037, 'chicago-red.png', null, null, 'Meeveo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (633, 4, 5583, 'duis bibendum felis', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', '2018-10-27 00:19:31', '2018-11-27 00:19:31', '2018-12-12 00:19:31', 5011, 'aberdeen-green.png', null, null, 'Wikivu', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (634, 4, 3533, 'viverra dapibus nulla suscipit', 'In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', '2018-07-13 06:05:55', '2018-08-13 06:05:55', '2018-08-28 06:05:55', 5015, 'chicago-red.png', null, null, 'Shufflester', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (635, 1, 4530, 'viverra pede ac', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', '2018-08-05 07:53:03', '2018-09-05 07:53:03', '2018-09-20 07:53:03', 40019, 'chicago-red.png', null, null, 'Jaxspan', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (636, 4, 6913, 'sapien sapien non', 'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', '2018-11-07 13:13:44', '2018-12-07 13:13:44', '2018-12-22 13:13:44', 5009, 'berlin-blue.png', null, null, 'Layo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (637, 1, 1771, 'nisl nunc nisl duis', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.', '2018-12-11 23:14:39', '2019-01-11 23:14:39', '2019-01-26 23:14:39', 40048, 'chicago-red.png', null, null, 'Shuffletag', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (638, 3, 1590, 'vulputate justo in blandit ultrices enim', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', '2018-08-02 17:53:38', '2018-09-02 17:53:38', '2018-09-17 17:53:38', 10029, 'chicago-red.png', null, null, 'Blognation', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (639, 2, 1211, 'amet consectetuer adipiscing elit proin risus', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', '2018-06-16 18:48:29', '2018-07-16 18:48:29', '2018-07-31 18:48:29', 20015, 'aberdeen-green.png', null, null, 'Buzzbean', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (640, 1, 6723, 'pellentesque ultrices mattis odio donec', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.

Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', '2018-07-11 01:27:06', '2018-08-11 01:27:06', '2018-08-26 01:27:06', 40016, 'berlin-blue.png', null, null, 'Avavee', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (641, 4, 3996, 'vivamus vel nulla eget eros', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', '2018-08-02 02:15:45', '2018-09-02 02:15:45', '2018-09-17 02:15:45', 5009, 'aberdeen-green.png', null, null, 'Jabbercube', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (642, 2, 4632, 'consequat ut nulla sed accumsan felis', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', '2018-10-13 07:06:10', '2018-11-13 07:06:10', '2018-11-28 07:06:10', 20035, 'chicago-red.png', null, null, 'Yacero', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (643, 1, 4149, 'in libero ut', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', '2018-03-15 22:54:53', '2018-04-15 22:54:53', '2018-04-30 22:54:53', 40025, 'amsterdam-orange.png', null, null, 'Devcast', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (644, 1, 2309, 'vel sem sed sagittis', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', '2018-03-05 13:46:53', '2018-04-05 13:46:53', '2018-04-20 13:46:53', 40045, 'chicago-red.png', null, null, 'Voonyx', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (645, 3, 7313, 'adipiscing elit proin risus', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', '2018-02-12 07:41:12', '2018-03-12 07:41:12', '2018-03-27 07:41:12', 10010, 'berlin-blue.png', null, null, 'Edgeify', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (646, 4, 6287, 'vehicula condimentum curabitur in', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', '2018-10-07 08:04:16', '2018-11-07 08:04:16', '2018-11-22 08:04:16', 5048, 'aberdeen-green.png', null, null, 'Teklist', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (647, 4, 1013, 'elementum ligula vehicula', 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.', '2018-11-04 12:18:02', '2018-12-04 12:18:02', '2018-12-19 12:18:02', 5034, 'amsterdam-orange.png', null, null, 'Jabbercube', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (648, 2, 2014, 'gravida sem praesent id massa', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.

Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.

Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', '2018-02-09 06:53:53', '2018-03-09 06:53:53', '2018-03-24 06:53:53', 20027, 'amsterdam-orange.png', null, null, 'Skipstorm', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (649, 4, 6122, 'mus etiam vel augue', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', '2018-01-15 01:28:58', '2018-02-15 01:28:58', '2018-03-02 01:28:58', 5002, 'aberdeen-green.png', null, null, 'Zoovu', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (650, 1, 2979, 'vel accumsan tellus nisi eu', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.', '2018-04-17 04:31:59', '2018-05-17 04:31:59', '2018-06-01 04:31:59', 40034, 'berlin-blue.png', null, null, 'Linkbuzz', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (651, 3, 2284, 'nullam porttitor lacus at turpis donec', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', '2018-12-12 23:55:14', '2019-01-12 23:55:14', '2019-01-27 23:55:14', 10019, 'aberdeen-green.png', null, null, 'Flashpoint', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (652, 4, 7774, 'sapien varius ut blandit non interdum', 'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.

Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', '2018-12-25 18:12:00', '2019-01-25 18:12:00', '2019-02-09 18:12:00', 5030, 'amsterdam-orange.png', null, null, 'Avamm', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (653, 3, 3980, 'quis turpis sed ante', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', '2018-01-01 03:11:57', '2018-02-01 03:11:57', '2018-02-16 03:11:57', 10001, 'chicago-red.png', null, null, 'Avamba', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (654, 1, 2936, 'tempus sit amet sem', 'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', '2018-01-09 06:33:03', '2018-02-09 06:33:03', '2018-02-24 06:33:03', 40023, 'aberdeen-green.png', null, null, 'Avamm', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (655, 3, 7165, 'eleifend pede libero quis', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', '2018-08-30 14:47:01', '2018-09-30 14:47:01', '2018-10-15 14:47:01', 10032, 'berlin-blue.png', null, null, 'Meejo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (656, 4, 6783, 'vel nisl duis', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', '2018-03-24 08:12:22', '2018-04-24 08:12:22', '2018-05-09 08:12:22', 5000, 'berlin-blue.png', null, null, 'Gigashots', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (657, 3, 140, 'sit amet lobortis sapien sapien non', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', '2018-12-11 18:41:44', '2019-01-11 18:41:44', '2019-01-26 18:41:44', 10037, 'chicago-red.png', null, null, 'Gabvine', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (658, 4, 2440, 'cubilia curae mauris viverra diam vitae', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.', '2018-06-19 00:12:05', '2018-07-19 00:12:05', '2018-08-03 00:12:05', 5026, 'amsterdam-orange.png', null, null, 'Rhycero', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (659, 2, 6200, 'amet nulla quisque arcu', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', '2018-07-08 14:06:00', '2018-08-08 14:06:00', '2018-08-23 14:06:00', 20022, 'berlin-blue.png', null, null, 'Browsezoom', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (660, 3, 2555, 'in felis eu sapien cursus', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', '2018-04-02 11:24:39', '2018-05-02 11:24:39', '2018-05-17 11:24:39', 10006, 'amsterdam-orange.png', null, null, 'Podcat', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (661, 1, 5558, 'nunc vestibulum ante ipsum', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.', '2018-03-10 13:32:01', '2018-04-10 13:32:01', '2018-04-25 13:32:01', 40031, 'berlin-blue.png', null, null, 'Innotype', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (662, 4, 4301, 'vel lectus in quam fringilla rhoncus', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', '2018-05-03 07:42:57', '2018-06-03 07:42:57', '2018-06-18 07:42:57', 5029, 'chicago-red.png', null, null, 'Skynoodle', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (663, 4, 801, 'iaculis diam erat fermentum justo nec', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', '2018-07-13 21:21:51', '2018-08-13 21:21:51', '2018-08-28 21:21:51', 5039, 'berlin-blue.png', null, null, 'Zoombox', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (664, 3, 2014, 'eu interdum eu tincidunt in leo', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', '2018-10-12 15:27:51', '2018-11-12 15:27:51', '2018-11-27 15:27:51', 10004, 'amsterdam-orange.png', null, null, 'Kamba', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (665, 4, 5875, 'odio curabitur convallis', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.', '2018-03-16 01:19:24', '2018-04-16 01:19:24', '2018-05-01 01:19:24', 5000, 'berlin-blue.png', null, null, 'Babbleset', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (666, 4, 5216, 'sapien placerat ante', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', '2018-12-13 18:55:41', '2019-01-13 18:55:41', '2019-01-28 18:55:41', 5023, 'berlin-blue.png', null, null, 'Yotz', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (667, 3, 5895, 'in magna bibendum imperdiet nullam orci', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.', '2018-03-14 01:52:27', '2018-04-14 01:52:27', '2018-04-29 01:52:27', 10019, 'amsterdam-orange.png', null, null, 'Thoughtworks', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (668, 2, 7077, 'nulla quisque arcu libero rutrum ac', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', '2018-07-23 18:17:16', '2018-08-23 18:17:16', '2018-09-07 18:17:16', 20049, 'amsterdam-orange.png', null, null, 'Twimbo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (669, 2, 7784, 'amet sem fusce', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.', '2018-01-14 07:47:52', '2018-02-14 07:47:52', '2018-03-01 07:47:52', 20047, 'aberdeen-green.png', null, null, 'Buzzdog', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (670, 3, 1083, 'erat nulla tempus vivamus', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', '2018-05-11 15:20:39', '2018-06-11 15:20:39', '2018-06-26 15:20:39', 10033, 'chicago-red.png', null, null, 'Dynava', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (671, 2, 3434, 'odio curabitur convallis duis', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', '2018-05-21 13:36:14', '2018-06-21 13:36:14', '2018-07-06 13:36:14', 20005, 'berlin-blue.png', null, null, 'Shufflester', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (672, 1, 6571, 'vel lectus in quam fringilla rhoncus', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', '2018-11-06 03:00:14', '2018-12-06 03:00:14', '2018-12-21 03:00:14', 40021, 'amsterdam-orange.png', null, null, 'Kwimbee', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (673, 4, 2790, 'risus praesent lectus vestibulum quam sapien', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', '2018-11-05 08:51:12', '2018-12-05 08:51:12', '2018-12-20 08:51:12', 5008, 'berlin-blue.png', null, null, 'Tazzy', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (674, 3, 1357, 'in quam fringilla rhoncus mauris', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', '2018-04-13 10:10:18', '2018-05-13 10:10:18', '2018-05-28 10:10:18', 10030, 'chicago-red.png', null, null, 'Voonyx', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (675, 3, 3177, 'ligula pellentesque ultrices phasellus id sapien', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', '2018-10-30 06:26:25', '2018-11-30 06:26:25', '2018-12-15 06:26:25', 10030, 'aberdeen-green.png', null, null, 'Twinte', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (676, 4, 12, 'vestibulum rutrum rutrum', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', '2018-09-07 02:13:09', '2018-10-07 02:13:09', '2018-10-22 02:13:09', 5025, 'aberdeen-green.png', null, null, 'Meevee', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (677, 2, 3069, 'arcu sed augue aliquam erat', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.', '2018-09-05 12:32:06', '2018-10-05 12:32:06', '2018-10-20 12:32:06', 20001, 'chicago-red.png', null, null, 'Mybuzz', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (678, 1, 2782, 'morbi a ipsum integer a', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', '2018-10-10 13:26:56', '2018-11-10 13:26:56', '2018-11-25 13:26:56', 40027, 'amsterdam-orange.png', null, null, 'Roombo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (679, 2, 7344, 'id lobortis convallis tortor risus', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', '2018-08-26 02:02:00', '2018-09-26 02:02:00', '2018-10-11 02:02:00', 20045, 'aberdeen-green.png', null, null, 'Flashset', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (680, 1, 4347, 'at velit eu est congue elementum', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', '2018-01-19 01:18:46', '2018-02-19 01:18:46', '2018-03-06 01:18:46', 40034, 'berlin-blue.png', null, null, 'Yakitri', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (681, 2, 7128, 'odio justo sollicitudin ut', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.

Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', '2018-10-17 12:20:05', '2018-11-17 12:20:05', '2018-12-02 12:20:05', 20040, 'berlin-blue.png', null, null, 'Realmix', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (682, 3, 3625, 'in libero ut massa volutpat', 'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', '2018-09-21 05:14:18', '2018-10-21 05:14:18', '2018-11-05 05:14:18', 10018, 'berlin-blue.png', null, null, 'Yodel', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (683, 3, 1951, 'hac habitasse platea dictumst aliquam', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.', '2018-01-23 14:52:53', '2018-02-23 14:52:53', '2018-03-10 14:52:53', 10018, 'chicago-red.png', null, null, 'Jayo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (684, 3, 4451, 'in faucibus orci luctus et ultrices', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.', '2018-11-17 17:57:30', '2018-12-17 17:57:30', '2019-01-01 17:57:30', 10005, 'amsterdam-orange.png', null, null, 'Jamia', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (685, 3, 6094, 'consectetuer adipiscing elit', 'Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', '2018-05-16 15:50:45', '2018-06-16 15:50:45', '2018-07-01 15:50:45', 10012, 'amsterdam-orange.png', null, null, 'Rooxo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (686, 3, 1985, 'ac enim in', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.

Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.

Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', '2018-05-16 22:43:41', '2018-06-16 22:43:41', '2018-07-01 22:43:41', 10050, 'amsterdam-orange.png', null, null, 'Yambee', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (687, 2, 3318, 'cum sociis natoque penatibus', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', '2018-04-24 13:32:29', '2018-05-24 13:32:29', '2018-06-08 13:32:29', 20025, 'chicago-red.png', null, null, 'Riffpath', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (688, 3, 5792, 'montes nascetur ridiculus', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.', '2018-08-12 20:58:35', '2018-09-12 20:58:35', '2018-09-27 20:58:35', 10016, 'berlin-blue.png', null, null, 'Avamm', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (689, 3, 3293, 'ultrices vel augue', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', '2018-06-13 15:51:02', '2018-07-13 15:51:02', '2018-07-28 15:51:02', 10030, 'berlin-blue.png', null, null, 'Blogtag', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (690, 4, 5833, 'id ligula suspendisse', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', '2018-07-19 19:26:01', '2018-08-19 19:26:01', '2018-09-03 19:26:01', 5021, 'amsterdam-orange.png', null, null, 'Skynoodle', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (691, 3, 1491, 'orci nullam molestie nibh in lectus', 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.', '2018-01-08 01:52:09', '2018-02-08 01:52:09', '2018-02-23 01:52:09', 10021, 'aberdeen-green.png', null, null, 'Eabox', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (692, 3, 464, 'venenatis tristique fusce', 'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.', '2018-09-10 13:51:04', '2018-10-10 13:51:04', '2018-10-25 13:51:04', 10040, 'aberdeen-green.png', null, null, 'Gabvine', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (693, 1, 6599, 'in faucibus orci luctus et', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.', '2018-12-09 13:57:39', '2019-01-09 13:57:39', '2019-01-24 13:57:39', 40033, 'aberdeen-green.png', null, null, 'Zoovu', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (694, 1, 2697, 'diam id ornare imperdiet sapien urna', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', '2018-11-13 23:40:03', '2018-12-13 23:40:03', '2018-12-28 23:40:03', 40049, 'chicago-red.png', null, null, 'Avamm', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (695, 4, 4355, 'luctus rutrum nulla', 'Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.', '2018-08-04 04:37:30', '2018-09-04 04:37:30', '2018-09-19 04:37:30', 5019, 'amsterdam-orange.png', null, null, 'Rhynoodle', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (696, 2, 6945, 'tortor sollicitudin mi sit amet lobortis', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', '2018-08-08 15:26:19', '2018-09-08 15:26:19', '2018-09-23 15:26:19', 20029, 'berlin-blue.png', null, null, 'Trilith', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (697, 1, 4058, 'sapien in sapien', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', '2018-07-03 10:13:14', '2018-08-03 10:13:14', '2018-08-18 10:13:14', 40028, 'amsterdam-orange.png', null, null, 'Jabberbean', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (698, 4, 2713, 'risus dapibus augue vel', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', '2018-12-09 04:23:48', '2019-01-09 04:23:48', '2019-01-24 04:23:48', 5004, 'berlin-blue.png', null, null, 'Cogibox', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (699, 1, 155, 'turpis elementum ligula vehicula consequat', 'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', '2018-12-07 04:03:13', '2019-01-07 04:03:13', '2019-01-22 04:03:13', 40022, 'amsterdam-orange.png', null, null, 'Demizz', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (700, 3, 4070, 'adipiscing lorem vitae mattis nibh ligula', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', '2018-09-03 05:02:53', '2018-10-03 05:02:53', '2018-10-18 05:02:53', 10020, 'berlin-blue.png', null, null, 'Zazio', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (701, 3, 2481, 'velit vivamus vel nulla eget eros', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', '2018-07-03 19:04:00', '2018-08-03 19:04:00', '2018-08-18 19:04:00', 10003, 'aberdeen-green.png', null, null, 'Tagopia', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (702, 1, 4471, 'feugiat et eros', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', '2018-03-10 09:51:21', '2018-04-10 09:51:21', '2018-04-25 09:51:21', 40004, 'aberdeen-green.png', null, null, 'Rhynyx', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (703, 3, 6520, 'eros viverra eget', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', '2018-02-11 02:19:06', '2018-03-11 02:19:06', '2018-03-26 02:19:06', 10002, 'chicago-red.png', null, null, 'Avavee', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (704, 1, 323, 'nisi eu orci mauris lacinia sapien', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', '2018-10-27 07:48:56', '2018-11-27 07:48:56', '2018-12-12 07:48:56', 40023, 'berlin-blue.png', null, null, 'Yacero', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (705, 1, 2023, 'turpis integer aliquet massa id', 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', '2018-11-01 02:01:11', '2018-12-01 02:01:11', '2018-12-16 02:01:11', 40005, 'chicago-red.png', null, null, 'Wordpedia', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (706, 1, 5537, 'amet diam in magna bibendum imperdiet', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', '2018-06-02 01:56:07', '2018-07-02 01:56:07', '2018-07-17 01:56:07', 40036, 'amsterdam-orange.png', null, null, 'Twimbo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (707, 4, 2856, 'quis turpis eget elit', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.', '2018-02-04 01:52:21', '2018-03-04 01:52:21', '2018-03-19 01:52:21', 5041, 'aberdeen-green.png', null, null, 'Roomm', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (708, 1, 841, 'ullamcorper augue a suscipit nulla', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', '2018-10-05 12:19:50', '2018-11-05 12:19:50', '2018-11-20 12:19:50', 40025, 'berlin-blue.png', null, null, 'Kwilith', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (709, 1, 5041, 'non lectus aliquam', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', '2018-01-17 07:03:31', '2018-02-17 07:03:31', '2018-03-04 07:03:31', 40000, 'berlin-blue.png', null, null, 'Skimia', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (710, 1, 6407, 'id consequat in consequat ut nulla', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', '2018-11-26 09:35:00', '2018-12-26 09:35:00', '2019-01-10 09:35:00', 40007, 'amsterdam-orange.png', null, null, 'Mydeo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (711, 1, 8018, 'orci luctus et ultrices posuere cubilia', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', '2018-06-30 03:05:33', '2018-07-30 03:05:33', '2018-08-14 03:05:33', 40010, 'amsterdam-orange.png', null, null, 'Edgeblab', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (712, 4, 6737, 'et commodo vulputate', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.

Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.

Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.', '2018-01-04 10:49:07', '2018-02-04 10:49:07', '2018-02-19 10:49:07', 5040, 'chicago-red.png', null, null, 'Blognation', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (713, 4, 7596, 'dictumst aliquam augue', 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', '2018-11-26 03:25:01', '2018-12-26 03:25:01', '2019-01-10 03:25:01', 5009, 'chicago-red.png', null, null, 'Dynazzy', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (714, 1, 6171, 'vel nulla eget eros elementum', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', '2018-07-09 07:53:54', '2018-08-09 07:53:54', '2018-08-24 07:53:54', 40050, 'berlin-blue.png', null, null, 'Gabcube', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (715, 1, 265, 'ante ipsum primis in', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', '2018-03-25 06:10:24', '2018-04-25 06:10:24', '2018-05-10 06:10:24', 40040, 'chicago-red.png', null, null, 'Zoovu', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (716, 1, 6068, 'pretium iaculis diam erat fermentum', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', '2018-02-15 09:14:02', '2018-03-15 09:14:02', '2018-03-30 09:14:02', 40042, 'berlin-blue.png', null, null, 'Quinu', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (717, 4, 2554, 'libero nam dui', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.', '2018-09-02 11:36:20', '2018-10-02 11:36:20', '2018-10-17 11:36:20', 5017, 'aberdeen-green.png', null, null, 'Plajo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (718, 4, 4552, 'parturient montes nascetur ridiculus', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.', '2018-11-01 08:01:27', '2018-12-01 08:01:27', '2018-12-16 08:01:27', 5036, 'berlin-blue.png', null, null, 'Linkbridge', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (719, 3, 2245, 'dui vel nisl duis', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', '2018-09-25 05:08:46', '2018-10-25 05:08:46', '2018-11-09 05:08:46', 10017, 'chicago-red.png', null, null, 'Oyonder', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (720, 4, 5727, 'aenean lectus pellentesque eget nunc donec', 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.', '2018-09-21 20:04:12', '2018-10-21 20:04:12', '2018-11-05 20:04:12', 5046, 'amsterdam-orange.png', null, null, 'Edgeclub', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (721, 2, 731, 'ipsum primis in faucibus orci luctus', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', '2018-07-04 19:54:15', '2018-08-04 19:54:15', '2018-08-19 19:54:15', 20048, 'berlin-blue.png', null, null, 'Fivebridge', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (722, 4, 2119, 'vestibulum ante ipsum primis in faucibus', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', '2018-02-15 07:51:31', '2018-03-15 07:51:31', '2018-03-30 07:51:31', 5019, 'aberdeen-green.png', null, null, 'Centidel', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (723, 3, 2292, 'blandit ultrices enim lorem ipsum dolor', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', '2018-01-11 03:58:39', '2018-02-11 03:58:39', '2018-02-26 03:58:39', 10008, 'chicago-red.png', null, null, 'Realbridge', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (724, 1, 4067, 'sed magna at nunc', 'In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', '2018-07-16 21:19:50', '2018-08-16 21:19:50', '2018-08-31 21:19:50', 40011, 'amsterdam-orange.png', null, null, 'Tavu', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (725, 3, 4153, 'viverra pede ac', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', '2018-05-16 01:51:02', '2018-06-16 01:51:02', '2018-07-01 01:51:02', 10008, 'amsterdam-orange.png', null, null, 'Minyx', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (726, 4, 1330, 'ipsum ac tellus semper', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.', '2018-02-19 10:57:33', '2018-03-19 10:57:33', '2018-04-03 10:57:33', 5023, 'chicago-red.png', null, null, 'Edgeclub', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (727, 4, 7245, 'libero non mattis', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', '2018-02-05 11:29:41', '2018-03-05 11:29:41', '2018-03-20 11:29:41', 5033, 'aberdeen-green.png', null, null, 'Feednation', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (728, 4, 7392, 'quis orci nullam molestie nibh in', 'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', '2018-01-13 18:01:18', '2018-02-13 18:01:18', '2018-02-28 18:01:18', 5050, 'aberdeen-green.png', null, null, 'Twimbo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (729, 1, 3680, 'curabitur at ipsum', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', '2018-05-29 12:49:45', '2018-06-29 12:49:45', '2018-07-14 12:49:45', 40042, 'chicago-red.png', null, null, 'Meejo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (730, 2, 3653, 'tortor id nulla', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', '2018-03-07 11:50:21', '2018-04-07 11:50:21', '2018-04-22 11:50:21', 20047, 'berlin-blue.png', null, null, 'Brainsphere', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (731, 1, 1784, 'tortor quis turpis', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', '2018-11-26 21:10:08', '2018-12-26 21:10:08', '2019-01-10 21:10:08', 40015, 'aberdeen-green.png', null, null, 'Oyoyo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (732, 2, 7831, 'ultrices mattis odio donec vitae nisi', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', '2018-10-22 15:50:42', '2018-11-22 15:50:42', '2018-12-07 15:50:42', 20033, 'berlin-blue.png', null, null, 'Centizu', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (733, 3, 1082, 'varius nulla facilisi cras', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', '2018-07-21 05:27:01', '2018-08-21 05:27:01', '2018-09-05 05:27:01', 10046, 'berlin-blue.png', null, null, 'Divape', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (734, 4, 3050, 'mattis nibh ligula nec sem', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', '2018-11-07 16:10:53', '2018-12-07 16:10:53', '2018-12-22 16:10:53', 5019, 'berlin-blue.png', null, null, 'Vipe', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (735, 4, 2722, 'in quam fringilla', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', '2018-12-28 15:32:00', '2019-01-28 15:32:00', '2019-02-12 15:32:00', 5047, 'berlin-blue.png', null, null, 'Zoonoodle', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (736, 2, 7459, 'ut nulla sed accumsan', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', '2018-03-16 12:56:00', '2018-04-16 12:56:00', '2018-05-01 12:56:00', 20020, 'chicago-red.png', null, null, 'Thoughtmix', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (737, 1, 4087, 'porttitor lorem id', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', '2018-05-06 23:37:29', '2018-06-06 23:37:29', '2018-06-21 23:37:29', 40029, 'berlin-blue.png', null, null, 'Fiveclub', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (738, 2, 189, 'elementum ligula vehicula consequat morbi', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', '2018-04-04 09:31:16', '2018-05-04 09:31:16', '2018-05-19 09:31:16', 20047, 'aberdeen-green.png', null, null, 'Devbug', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (739, 1, 5035, 'nulla eget eros elementum pellentesque quisque', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', '2018-04-15 04:12:41', '2018-05-15 04:12:41', '2018-05-30 04:12:41', 40045, 'chicago-red.png', null, null, 'Linktype', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (740, 1, 7661, 'elit sodales scelerisque mauris sit', 'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.

Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', '2018-12-24 11:59:01', '2019-01-24 11:59:01', '2019-02-08 11:59:01', 40012, 'amsterdam-orange.png', null, null, 'Youspan', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (741, 4, 2263, 'turpis enim blandit', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', '2018-05-07 21:06:49', '2018-06-07 21:06:49', '2018-06-22 21:06:49', 5045, 'berlin-blue.png', null, null, 'Fanoodle', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (742, 4, 4024, 'maecenas pulvinar lobortis est phasellus', 'In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.', '2018-06-05 22:07:39', '2018-07-05 22:07:39', '2018-07-20 22:07:39', 5010, 'aberdeen-green.png', null, null, 'Devshare', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (743, 4, 2877, 'fusce posuere felis', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.', '2018-09-11 20:42:13', '2018-10-11 20:42:13', '2018-10-26 20:42:13', 5001, 'chicago-red.png', null, null, 'Gabtype', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (744, 1, 4339, 'id turpis integer aliquet massa', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', '2018-03-29 17:05:30', '2018-04-29 17:05:30', '2018-05-14 17:05:30', 40014, 'chicago-red.png', null, null, 'Yozio', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (745, 4, 2313, 'morbi a ipsum', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', '2018-07-19 18:07:48', '2018-08-19 18:07:48', '2018-09-03 18:07:48', 5015, 'chicago-red.png', null, null, 'Meevee', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (746, 4, 7212, 'luctus ultricies eu nibh quisque id', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.

Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.

Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', '2018-07-25 10:16:25', '2018-08-25 10:16:25', '2018-09-09 10:16:25', 5028, 'amsterdam-orange.png', null, null, 'Skimia', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (747, 1, 1600, 'quam pede lobortis ligula sit amet', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', '2018-06-06 18:21:02', '2018-07-06 18:21:02', '2018-07-21 18:21:02', 40003, 'aberdeen-green.png', null, null, 'Quire', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (748, 2, 1864, 'enim sit amet nunc', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.', '2018-06-02 11:45:37', '2018-07-02 11:45:37', '2018-07-17 11:45:37', 20026, 'berlin-blue.png', null, null, 'Youtags', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (749, 3, 823, 'ipsum integer a nibh', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', '2018-03-10 07:08:29', '2018-04-10 07:08:29', '2018-04-25 07:08:29', 10032, 'aberdeen-green.png', null, null, 'Tagfeed', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (750, 2, 7448, 'consequat dui nec nisi volutpat eleifend', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.', '2018-06-10 03:56:54', '2018-07-10 03:56:54', '2018-07-25 03:56:54', 20049, 'berlin-blue.png', null, null, 'Tekfly', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (751, 2, 4258, 'viverra pede ac', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', '2018-12-01 22:05:54', '2019-01-01 22:05:54', '2019-01-16 22:05:54', 20016, 'amsterdam-orange.png', null, null, 'Kare', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (752, 4, 7463, 'elementum pellentesque quisque porta', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', '2018-06-01 12:36:19', '2018-07-01 12:36:19', '2018-07-16 12:36:19', 5017, 'aberdeen-green.png', null, null, 'Aimbu', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (753, 1, 3557, 'donec diam neque', 'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.

Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', '2018-10-23 20:00:55', '2018-11-23 20:00:55', '2018-12-08 20:00:55', 40007, 'amsterdam-orange.png', null, null, 'Jazzy', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (754, 3, 7026, 'vestibulum rutrum rutrum neque aenean auctor', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.', '2018-05-03 05:32:18', '2018-06-03 05:32:18', '2018-06-18 05:32:18', 10020, 'aberdeen-green.png', null, null, 'Realbridge', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (755, 2, 6400, 'integer ac neque', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.', '2018-09-15 18:08:09', '2018-10-15 18:08:09', '2018-10-30 18:08:09', 20001, 'amsterdam-orange.png', null, null, 'Linklinks', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (756, 3, 987, 'lorem id ligula suspendisse ornare', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', '2018-12-07 07:21:55', '2019-01-07 07:21:55', '2019-01-22 07:21:55', 10040, 'aberdeen-green.png', null, null, 'Quire', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (757, 3, 6775, 'tempus sit amet sem fusce consequat', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', '2018-09-09 00:05:55', '2018-10-09 00:05:55', '2018-10-24 00:05:55', 10028, 'chicago-red.png', null, null, 'Eare', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (758, 1, 6788, 'erat eros viverra eget congue', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', '2018-08-10 19:43:52', '2018-09-10 19:43:52', '2018-09-25 19:43:52', 40047, 'chicago-red.png', null, null, 'Edgepulse', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (759, 2, 6789, 'non velit donec diam neque', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', '2018-01-14 10:33:02', '2018-02-14 10:33:02', '2018-03-01 10:33:02', 20030, 'chicago-red.png', null, null, 'Yakijo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (760, 1, 5329, 'interdum mauris non ligula pellentesque ultrices', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', '2018-10-09 19:01:17', '2018-11-09 19:01:17', '2018-11-24 19:01:17', 40044, 'chicago-red.png', null, null, 'Oyoyo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (761, 4, 6108, 'felis donec semper sapien a libero', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.

Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.', '2018-06-19 09:36:33', '2018-07-19 09:36:33', '2018-08-03 09:36:33', 5021, 'chicago-red.png', null, null, 'Dabfeed', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (762, 4, 333, 'nam congue risus semper porta volutpat', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', '2018-12-30 04:52:47', '2019-01-30 04:52:47', '2019-02-14 04:52:47', 5008, 'chicago-red.png', null, null, 'Skivee', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (763, 1, 5313, 'sit amet consectetuer', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', '2018-03-17 15:41:06', '2018-04-17 15:41:06', '2018-05-02 15:41:06', 40034, 'berlin-blue.png', null, null, 'Minyx', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (764, 2, 5723, 'in ante vestibulum', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', '2018-07-09 22:24:29', '2018-08-09 22:24:29', '2018-08-24 22:24:29', 20028, 'chicago-red.png', null, null, 'Plajo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (765, 1, 5195, 'semper est quam pharetra magna ac', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.', '2018-03-10 17:07:15', '2018-04-10 17:07:15', '2018-04-25 17:07:15', 40034, 'chicago-red.png', null, null, 'Yodoo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (766, 1, 7666, 'at lorem integer tincidunt ante', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', '2018-09-01 22:13:26', '2018-10-01 22:13:26', '2018-10-16 22:13:26', 40009, 'berlin-blue.png', null, null, 'Abata', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (767, 1, 6484, 'mattis odio donec vitae', 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.

Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', '2018-03-06 18:17:36', '2018-04-06 18:17:36', '2018-04-21 18:17:36', 40000, 'amsterdam-orange.png', null, null, 'Gabtune', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (768, 3, 7461, 'dui maecenas tristique', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', '2018-04-11 06:30:45', '2018-05-11 06:30:45', '2018-05-26 06:30:45', 10008, 'amsterdam-orange.png', null, null, 'Edgewire', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (769, 3, 1564, 'ornare consequat lectus in est', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', '2018-07-31 02:27:35', '2018-08-31 02:27:35', '2018-09-15 02:27:35', 10039, 'chicago-red.png', null, null, 'JumpXS', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (770, 4, 6496, 'morbi odio odio elementum eu', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', '2018-05-09 06:41:18', '2018-06-09 06:41:18', '2018-06-24 06:41:18', 5028, 'berlin-blue.png', null, null, 'Tagpad', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (771, 3, 5898, 'suspendisse potenti nullam porttitor lacus at', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.', '2018-07-31 16:05:58', '2018-08-31 16:05:58', '2018-09-15 16:05:58', 10050, 'chicago-red.png', null, null, 'Tagopia', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (772, 2, 987, 'nisi volutpat eleifend donec', 'Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', '2018-01-26 01:44:41', '2018-02-26 01:44:41', '2018-03-13 01:44:41', 20012, 'amsterdam-orange.png', null, null, 'Gigazoom', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (773, 1, 3211, 'consectetuer adipiscing elit proin interdum mauris', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.', '2018-02-18 15:03:40', '2018-03-18 15:03:40', '2018-04-02 15:03:40', 40044, 'amsterdam-orange.png', null, null, 'Brainlounge', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (774, 4, 4159, 'condimentum curabitur in libero', 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', '2018-04-18 02:54:08', '2018-05-18 02:54:08', '2018-06-02 02:54:08', 5002, 'chicago-red.png', null, null, 'Brainsphere', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (775, 1, 1745, 'nulla eget eros elementum pellentesque', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', '2018-10-09 02:37:44', '2018-11-09 02:37:44', '2018-11-24 02:37:44', 40012, 'amsterdam-orange.png', null, null, 'Leexo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (776, 4, 3553, 'parturient montes nascetur ridiculus mus vivamus', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.', '2018-05-24 14:33:16', '2018-06-24 14:33:16', '2018-07-09 14:33:16', 5024, 'aberdeen-green.png', null, null, 'Skyble', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (777, 4, 1829, 'pellentesque quisque porta volutpat erat', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', '2018-10-28 08:18:38', '2018-11-28 08:18:38', '2018-12-13 08:18:38', 5019, 'chicago-red.png', null, null, 'Devpoint', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (778, 2, 5992, 'lorem id ligula', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', '2018-06-07 18:00:43', '2018-07-07 18:00:43', '2018-07-22 18:00:43', 20035, 'amsterdam-orange.png', null, null, 'Tagchat', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (779, 2, 2551, 'ac enim in tempor', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.', '2018-09-07 05:55:26', '2018-10-07 05:55:26', '2018-10-22 05:55:26', 20042, 'amsterdam-orange.png', null, null, 'Browsetype', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (780, 1, 1359, 'vestibulum vestibulum ante', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', '2018-03-27 05:33:12', '2018-04-27 05:33:12', '2018-05-12 05:33:12', 40038, 'berlin-blue.png', null, null, 'Browsecat', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (781, 1, 6124, 'molestie lorem quisque ut erat', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.', '2018-09-23 08:45:44', '2018-10-23 08:45:44', '2018-11-07 08:45:44', 40029, 'berlin-blue.png', null, null, 'Ailane', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (782, 1, 4736, 'lectus aliquam sit', 'In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', '2018-08-22 14:47:04', '2018-09-22 14:47:04', '2018-10-07 14:47:04', 40026, 'amsterdam-orange.png', null, null, 'Gigashots', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (783, 4, 639, 'adipiscing elit proin', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', '2018-01-09 13:51:06', '2018-02-09 13:51:06', '2018-02-24 13:51:06', 5036, 'berlin-blue.png', null, null, 'Zoomlounge', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (784, 1, 8102, 'lacinia erat vestibulum sed', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.', '2018-04-30 08:04:40', '2018-05-30 08:04:40', '2018-06-14 08:04:40', 40047, 'chicago-red.png', null, null, 'Tazzy', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (785, 4, 748, 'eget tincidunt eget', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', '2018-02-05 20:25:13', '2018-03-05 20:25:13', '2018-03-20 20:25:13', 5014, 'amsterdam-orange.png', null, null, 'Wikizz', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (786, 1, 5081, 'vitae ipsum aliquam non', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.', '2018-12-16 04:05:37', '2019-01-16 04:05:37', '2019-01-31 04:05:37', 40038, 'aberdeen-green.png', null, null, 'Bluezoom', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (787, 3, 5268, 'nec nisi vulputate nonummy maecenas', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', '2018-04-20 15:55:23', '2018-05-20 15:55:23', '2018-06-04 15:55:23', 10000, 'amsterdam-orange.png', null, null, 'Centidel', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (788, 4, 5139, 'felis fusce posuere felis sed lacus', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.', '2018-04-14 06:30:19', '2018-05-14 06:30:19', '2018-05-29 06:30:19', 5027, 'aberdeen-green.png', null, null, 'Twitterbeat', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (789, 1, 3906, 'at feugiat non pretium', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.

Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.', '2018-06-06 06:06:18', '2018-07-06 06:06:18', '2018-07-21 06:06:18', 40019, 'berlin-blue.png', null, null, 'Brightdog', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (790, 3, 2152, 'in hac habitasse', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', '2018-04-07 16:18:12', '2018-05-07 16:18:12', '2018-05-22 16:18:12', 10019, 'chicago-red.png', null, null, 'Linklinks', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (791, 3, 1506, 'congue etiam justo', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.', '2018-02-21 00:11:33', '2018-03-21 00:11:33', '2018-04-05 00:11:33', 10006, 'chicago-red.png', null, null, 'Feedbug', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (792, 4, 7372, 'aliquet ultrices erat tortor sollicitudin mi', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', '2018-01-18 21:38:49', '2018-02-18 21:38:49', '2018-03-05 21:38:49', 5015, 'aberdeen-green.png', null, null, 'Brightbean', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (793, 4, 4878, 'duis aliquam convallis nunc proin at', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', '2018-09-19 05:49:10', '2018-10-19 05:49:10', '2018-11-03 05:49:10', 5040, 'amsterdam-orange.png', null, null, 'Yodo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (794, 2, 7926, 'non mi integer ac', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', '2018-12-24 00:09:09', '2019-01-24 00:09:09', '2019-02-08 00:09:09', 20010, 'chicago-red.png', null, null, 'Voonte', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (795, 2, 4677, 'maecenas rhoncus aliquam lacus morbi', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', '2018-03-08 03:49:06', '2018-04-08 03:49:06', '2018-04-23 03:49:06', 20002, 'chicago-red.png', null, null, 'Twimm', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (796, 3, 1393, 'donec semper sapien a libero nam', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', '2018-09-29 00:38:17', '2018-10-29 00:38:17', '2018-11-13 00:38:17', 10047, 'berlin-blue.png', null, null, 'Topicblab', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (797, 1, 678, 'scelerisque mauris sit amet eros', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', '2018-02-21 01:35:42', '2018-03-21 01:35:42', '2018-04-05 01:35:42', 40038, 'aberdeen-green.png', null, null, 'Babblestorm', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (798, 3, 2000, 'donec ut dolor morbi', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', '2018-09-12 01:08:57', '2018-10-12 01:08:57', '2018-10-27 01:08:57', 10014, 'berlin-blue.png', null, null, 'Aimbo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (799, 2, 5230, 'diam nam tristique tortor', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', '2018-02-22 05:33:54', '2018-03-22 05:33:54', '2018-04-06 05:33:54', 20007, 'aberdeen-green.png', null, null, 'Thoughtsphere', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (800, 4, 78, 'ultrices mattis odio donec', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.', '2018-11-09 22:06:48', '2018-12-09 22:06:48', '2018-12-24 22:06:48', 5003, 'berlin-blue.png', null, null, 'Yambee', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (801, 4, 3822, 'gravida sem praesent id massa id', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', '2018-08-03 21:21:11', '2018-09-03 21:21:11', '2018-09-18 21:21:11', 5029, 'chicago-red.png', null, null, 'Centizu', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (802, 3, 5692, 'aliquet ultrices erat tortor', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', '2018-06-17 23:35:45', '2018-07-17 23:35:45', '2018-08-01 23:35:45', 10038, 'chicago-red.png', null, null, 'Kwinu', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (803, 1, 3909, 'in leo maecenas pulvinar lobortis', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', '2018-06-14 15:10:30', '2018-07-14 15:10:30', '2018-07-29 15:10:30', 40029, 'berlin-blue.png', null, null, 'Edgeify', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (804, 3, 2624, 'quis lectus suspendisse potenti in', 'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', '2018-10-27 12:54:43', '2018-11-27 12:54:43', '2018-12-12 12:54:43', 10032, 'aberdeen-green.png', null, null, 'Cogibox', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (805, 1, 1122, 'nunc commodo placerat praesent', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.', '2018-07-18 08:43:18', '2018-08-18 08:43:18', '2018-09-02 08:43:18', 40015, 'aberdeen-green.png', null, null, 'Blogtag', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (806, 3, 1638, 'quisque erat eros viverra', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', '2018-05-16 22:13:49', '2018-06-16 22:13:49', '2018-07-01 22:13:49', 10009, 'amsterdam-orange.png', null, null, 'Browsetype', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (807, 3, 6496, 'at vulputate vitae nisl aenean lectus', 'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.

Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', '2018-10-14 23:11:13', '2018-11-14 23:11:13', '2018-11-29 23:11:13', 10015, 'amsterdam-orange.png', null, null, 'Myworks', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (808, 3, 3906, 'rutrum nulla tellus', 'Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', '2018-01-27 13:48:48', '2018-02-27 13:48:48', '2018-03-14 13:48:48', 10043, 'aberdeen-green.png', null, null, 'Oba', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (809, 1, 4815, 'curabitur in libero ut massa volutpat', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', '2018-04-17 21:23:26', '2018-05-17 21:23:26', '2018-06-01 21:23:26', 40016, 'amsterdam-orange.png', null, null, 'Riffpedia', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (810, 3, 1341, 'elit proin risus praesent lectus vestibulum', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', '2018-11-30 13:09:57', '2018-12-30 13:09:57', '2019-01-14 13:09:57', 10016, 'berlin-blue.png', null, null, 'Skinix', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (811, 3, 7019, 'interdum mauris non ligula pellentesque ultrices', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', '2018-09-11 01:56:58', '2018-10-11 01:56:58', '2018-10-26 01:56:58', 10038, 'berlin-blue.png', null, null, 'Photofeed', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (812, 1, 1140, 'a suscipit nulla', 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', '2018-12-10 11:21:37', '2019-01-10 11:21:37', '2019-01-25 11:21:37', 40021, 'amsterdam-orange.png', null, null, 'Jaxworks', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (813, 3, 1897, 'vel accumsan tellus nisi', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', '2018-05-13 00:14:25', '2018-06-13 00:14:25', '2018-06-28 00:14:25', 10029, 'berlin-blue.png', null, null, 'Devpoint', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (814, 4, 597, 'justo etiam pretium iaculis justo in', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', '2018-12-08 15:12:25', '2019-01-08 15:12:25', '2019-01-23 15:12:25', 5040, 'amsterdam-orange.png', null, null, 'Brainsphere', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (815, 3, 522, 'posuere metus vitae ipsum', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', '2018-09-09 12:55:27', '2018-10-09 12:55:27', '2018-10-24 12:55:27', 10002, 'chicago-red.png', null, null, 'Meevee', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (816, 4, 5450, 'euismod scelerisque quam', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.', '2018-11-14 17:28:42', '2018-12-14 17:28:42', '2018-12-29 17:28:42', 5045, 'amsterdam-orange.png', null, null, 'Voomm', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (817, 3, 137, 'nulla mollis molestie', 'In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', '2018-03-31 19:37:52', '2018-04-30 19:37:52', '2018-05-15 19:37:52', 10037, 'chicago-red.png', null, null, 'Zoonder', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (818, 1, 1804, 'integer non velit donec diam', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', '2018-12-23 10:05:40', '2019-01-23 10:05:40', '2019-02-07 10:05:40', 40016, 'aberdeen-green.png', null, null, 'Yodel', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (819, 2, 1413, 'ut tellus nulla ut erat id', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', '2018-05-12 13:53:32', '2018-06-12 13:53:32', '2018-06-27 13:53:32', 20041, 'amsterdam-orange.png', null, null, 'Divavu', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (820, 2, 4890, 'convallis duis consequat dui nec nisi', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', '2018-07-04 18:51:11', '2018-08-04 18:51:11', '2018-08-19 18:51:11', 20033, 'amsterdam-orange.png', null, null, 'Innojam', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (821, 3, 4364, 'sapien ut nunc', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.', '2018-12-24 02:29:56', '2019-01-24 02:29:56', '2019-02-08 02:29:56', 10008, 'berlin-blue.png', null, null, 'Quinu', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (822, 3, 298, 'ut mauris eget massa', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', '2018-04-30 16:54:52', '2018-05-30 16:54:52', '2018-06-14 16:54:52', 10029, 'amsterdam-orange.png', null, null, 'Quaxo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (823, 3, 5538, 'felis sed lacus morbi sem', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', '2018-11-30 18:34:33', '2018-12-30 18:34:33', '2019-01-14 18:34:33', 10029, 'berlin-blue.png', null, null, 'Voolith', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (824, 3, 1402, 'odio donec vitae', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', '2018-08-07 12:11:31', '2018-09-07 12:11:31', '2018-09-22 12:11:31', 10042, 'berlin-blue.png', null, null, 'Tazzy', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (825, 2, 3008, 'nulla tellus in', 'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.

Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', '2018-01-15 13:54:35', '2018-02-15 13:54:35', '2018-03-02 13:54:35', 20008, 'berlin-blue.png', null, null, 'Meemm', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (826, 3, 1133, 'auctor sed tristique in tempus sit', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', '2018-08-23 02:09:57', '2018-09-23 02:09:57', '2018-10-08 02:09:57', 10042, 'chicago-red.png', null, null, 'Fadeo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (827, 2, 3639, 'in faucibus orci', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.', '2018-01-19 01:28:21', '2018-02-19 01:28:21', '2018-03-06 01:28:21', 20004, 'aberdeen-green.png', null, null, 'Blogspan', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (828, 4, 1880, 'dui maecenas tristique est', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', '2018-07-11 20:11:04', '2018-08-11 20:11:04', '2018-08-26 20:11:04', 5009, 'chicago-red.png', null, null, 'Rooxo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (829, 1, 7756, 'viverra eget congue', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', '2018-07-29 18:06:08', '2018-08-29 18:06:08', '2018-09-13 18:06:08', 40004, 'amsterdam-orange.png', null, null, 'Flashset', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (830, 2, 2695, 'lacus morbi quis tortor', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', '2018-01-27 16:10:39', '2018-02-27 16:10:39', '2018-03-14 16:10:39', 20037, 'chicago-red.png', null, null, 'Yambee', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (831, 2, 897, 'rutrum at lorem', 'In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', '2018-08-05 17:16:18', '2018-09-05 17:16:18', '2018-09-20 17:16:18', 20002, 'berlin-blue.png', null, null, 'Skibox', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (832, 1, 6299, 'quam sollicitudin vitae', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', '2018-12-27 08:21:18', '2019-01-27 08:21:18', '2019-02-11 08:21:18', 40025, 'amsterdam-orange.png', null, null, 'Meevee', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (833, 2, 7203, 'elit ac nulla', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', '2018-10-14 14:08:21', '2018-11-14 14:08:21', '2018-11-29 14:08:21', 20008, 'aberdeen-green.png', null, null, 'Tazzy', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (834, 3, 5963, 'ipsum primis in faucibus orci luctus', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', '2018-02-26 06:50:32', '2018-03-26 06:50:32', '2018-04-10 06:50:32', 10023, 'berlin-blue.png', null, null, 'Jabbersphere', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (835, 3, 5290, 'cursus vestibulum proin eu', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', '2018-02-19 10:00:19', '2018-03-19 10:00:19', '2018-04-03 10:00:19', 10047, 'chicago-red.png', null, null, 'Youtags', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (836, 4, 560, 'vestibulum aliquet ultrices erat', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.', '2018-09-18 13:47:06', '2018-10-18 13:47:06', '2018-11-02 13:47:06', 5010, 'chicago-red.png', null, null, 'Flashset', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (837, 1, 3059, 'duis ac nibh fusce', 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', '2018-08-19 17:38:19', '2018-09-19 17:38:19', '2018-10-04 17:38:19', 40017, 'aberdeen-green.png', null, null, 'Realblab', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (838, 4, 7596, 'parturient montes nascetur', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', '2018-04-23 15:17:30', '2018-05-23 15:17:30', '2018-06-07 15:17:30', 5004, 'berlin-blue.png', null, null, 'Zazio', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (839, 1, 1458, 'metus aenean fermentum donec ut mauris', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.', '2018-07-08 18:04:14', '2018-08-08 18:04:14', '2018-08-23 18:04:14', 40014, 'aberdeen-green.png', null, null, 'Dazzlesphere', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (840, 3, 6229, 'non interdum in', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.', '2018-11-01 17:55:47', '2018-12-01 17:55:47', '2018-12-16 17:55:47', 10049, 'chicago-red.png', null, null, 'Yacero', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (841, 3, 6178, 'nulla dapibus dolor vel', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.', '2018-05-14 11:38:57', '2018-06-14 11:38:57', '2018-06-29 11:38:57', 10013, 'aberdeen-green.png', null, null, 'Zooxo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (842, 3, 2096, 'quam a odio in hac', 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', '2018-09-08 07:30:18', '2018-10-08 07:30:18', '2018-10-23 07:30:18', 10031, 'aberdeen-green.png', null, null, 'Shufflebeat', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (843, 2, 6983, 'viverra diam vitae', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', '2018-09-08 10:56:40', '2018-10-08 10:56:40', '2018-10-23 10:56:40', 20008, 'chicago-red.png', null, null, 'Eamia', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (844, 3, 7240, 'a feugiat et eros vestibulum ac', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', '2018-06-22 18:44:54', '2018-07-22 18:44:54', '2018-08-06 18:44:54', 10047, 'amsterdam-orange.png', null, null, 'Jamia', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (845, 1, 2260, 'donec diam neque', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', '2018-03-20 05:03:07', '2018-04-20 05:03:07', '2018-05-05 05:03:07', 40023, 'amsterdam-orange.png', null, null, 'Fivechat', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (846, 3, 2225, 'erat vestibulum sed magna', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', '2018-05-21 18:13:34', '2018-06-21 18:13:34', '2018-07-06 18:13:34', 10001, 'chicago-red.png', null, null, 'Podcat', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (847, 4, 5178, 'ligula vehicula consequat', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', '2018-10-19 04:03:16', '2018-11-19 04:03:16', '2018-12-04 04:03:16', 5014, 'berlin-blue.png', null, null, 'Fivechat', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (848, 3, 2708, 'sit amet nulla quisque', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', '2018-10-11 08:03:53', '2018-11-11 08:03:53', '2018-11-26 08:03:53', 10037, 'berlin-blue.png', null, null, 'Topiclounge', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (849, 3, 93, 'erat volutpat in', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', '2018-06-05 06:21:53', '2018-07-05 06:21:53', '2018-07-20 06:21:53', 10029, 'amsterdam-orange.png', null, null, 'Zazio', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (850, 3, 3358, 'porttitor pede justo eu massa', 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.', '2018-10-29 23:32:58', '2018-11-29 23:32:58', '2018-12-14 23:32:58', 10026, 'chicago-red.png', null, null, 'Quamba', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (851, 3, 4738, 'vulputate vitae nisl', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.', '2018-10-20 20:50:19', '2018-11-20 20:50:19', '2018-12-05 20:50:19', 10020, 'chicago-red.png', null, null, 'Voonte', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (852, 4, 380, 'commodo vulputate justo', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', '2018-06-06 00:33:12', '2018-07-06 00:33:12', '2018-07-21 00:33:12', 5014, 'aberdeen-green.png', null, null, 'Flipbug', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (853, 2, 7060, 'sapien sapien non mi', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', '2018-05-11 15:25:50', '2018-06-11 15:25:50', '2018-06-26 15:25:50', 20010, 'amsterdam-orange.png', null, null, 'Oba', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (854, 2, 6780, 'ultrices erat tortor sollicitudin mi sit', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.', '2018-08-25 15:15:04', '2018-09-25 15:15:04', '2018-10-10 15:15:04', 20037, 'chicago-red.png', null, null, 'Mynte', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (855, 1, 2913, 'mattis pulvinar nulla pede ullamcorper', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.', '2018-02-23 12:10:06', '2018-03-23 12:10:06', '2018-04-07 12:10:06', 40019, 'amsterdam-orange.png', null, null, 'Shuffledrive', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (856, 3, 663, 'eleifend quam a odio', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', '2018-12-10 07:08:44', '2019-01-10 07:08:44', '2019-01-25 07:08:44', 10028, 'amsterdam-orange.png', null, null, 'Twitterbridge', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (857, 1, 5350, 'in libero ut massa volutpat convallis', 'Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', '2018-09-17 02:48:54', '2018-10-17 02:48:54', '2018-11-01 02:48:54', 40020, 'chicago-red.png', null, null, 'Yacero', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (858, 2, 7627, 'ante nulla justo aliquam quis turpis', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.', '2018-03-12 09:37:01', '2018-04-12 09:37:01', '2018-04-27 09:37:01', 20019, 'chicago-red.png', null, null, 'Dablist', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (859, 2, 1336, 'sem sed sagittis nam congue', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', '2018-10-26 19:38:57', '2018-11-26 19:38:57', '2018-12-11 19:38:57', 20000, 'aberdeen-green.png', null, null, 'Quimm', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (860, 4, 966, 'donec dapibus duis', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', '2018-10-30 07:11:24', '2018-11-30 07:11:24', '2018-12-15 07:11:24', 5041, 'berlin-blue.png', null, null, 'Yata', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (861, 3, 3507, 'morbi vestibulum velit id', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', '2018-01-15 03:16:45', '2018-02-15 03:16:45', '2018-03-02 03:16:45', 10010, 'chicago-red.png', null, null, 'Linkbridge', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (862, 3, 4433, 'ultrices mattis odio donec vitae', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', '2018-05-11 20:28:37', '2018-06-11 20:28:37', '2018-06-26 20:28:37', 10008, 'chicago-red.png', null, null, 'Voolia', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (863, 4, 1194, 'mauris lacinia sapien quis libero', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', '2018-07-26 04:15:44', '2018-08-26 04:15:44', '2018-09-10 04:15:44', 5011, 'aberdeen-green.png', null, null, 'Twimbo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (864, 3, 937, 'ac nulla sed vel', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', '2018-05-19 06:01:07', '2018-06-19 06:01:07', '2018-07-04 06:01:07', 10045, 'berlin-blue.png', null, null, 'Wikivu', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (865, 4, 1691, 'duis mattis egestas', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', '2018-09-26 11:07:06', '2018-10-26 11:07:06', '2018-11-10 11:07:06', 5039, 'amsterdam-orange.png', null, null, 'Latz', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (866, 2, 7741, 'praesent id massa id nisl venenatis', 'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', '2018-02-12 21:55:05', '2018-03-12 21:55:05', '2018-03-27 21:55:05', 20022, 'chicago-red.png', null, null, 'Eayo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (867, 4, 4858, 'turpis a pede', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', '2018-08-18 18:34:30', '2018-09-18 18:34:30', '2018-10-03 18:34:30', 5044, 'aberdeen-green.png', null, null, 'Twitterlist', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (868, 1, 606, 'eu nibh quisque id justo sit', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', '2018-10-06 18:16:42', '2018-11-06 18:16:42', '2018-11-21 18:16:42', 40004, 'chicago-red.png', null, null, 'Layo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (869, 1, 2248, 'in faucibus orci', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', '2018-05-27 00:45:14', '2018-06-27 00:45:14', '2018-07-12 00:45:14', 40022, 'chicago-red.png', null, null, 'Bubblemix', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (870, 4, 5329, 'platea dictumst maecenas ut massa quis', 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', '2018-10-05 23:05:28', '2018-11-05 23:05:28', '2018-11-20 23:05:28', 5049, 'chicago-red.png', null, null, 'Jazzy', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (871, 2, 3127, 'dapibus duis at velit eu est', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', '2018-02-02 22:39:43', '2018-03-02 22:39:43', '2018-03-17 22:39:43', 20030, 'berlin-blue.png', null, null, 'Podcat', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (872, 4, 719, 'hendrerit at vulputate', 'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', '2018-04-17 00:47:20', '2018-05-17 00:47:20', '2018-06-01 00:47:20', 5046, 'chicago-red.png', null, null, 'Leenti', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (873, 2, 6464, 'et tempus semper est', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', '2018-11-15 20:35:36', '2018-12-15 20:35:36', '2018-12-30 20:35:36', 20047, 'aberdeen-green.png', null, null, 'Edgewire', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (874, 1, 7612, 'vehicula condimentum curabitur in libero ut', 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', '2018-05-14 19:39:38', '2018-06-14 19:39:38', '2018-06-29 19:39:38', 40038, 'aberdeen-green.png', null, null, 'Devshare', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (875, 4, 5679, 'duis consequat dui nec nisi volutpat', 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.', '2018-09-07 19:56:39', '2018-10-07 19:56:39', '2018-10-22 19:56:39', 5050, 'amsterdam-orange.png', null, null, 'Voolith', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (876, 4, 5286, 'nunc viverra dapibus nulla suscipit ligula', 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.', '2018-11-15 19:57:16', '2018-12-15 19:57:16', '2018-12-30 19:57:16', 5049, 'aberdeen-green.png', null, null, 'Voolith', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (877, 4, 3188, 'felis ut at', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.', '2018-07-30 07:22:56', '2018-08-30 07:22:56', '2018-09-14 07:22:56', 5040, 'chicago-red.png', null, null, 'Yodo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (878, 4, 272, 'rutrum nulla nunc purus phasellus in', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', '2018-02-15 09:37:20', '2018-03-15 09:37:20', '2018-03-30 09:37:20', 5026, 'chicago-red.png', null, null, 'Quinu', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (879, 4, 116, 'ante ipsum primis in faucibus orci', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', '2018-06-24 23:44:12', '2018-07-24 23:44:12', '2018-08-08 23:44:12', 5041, 'chicago-red.png', null, null, 'Topiczoom', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (880, 1, 2244, 'vitae nisl aenean', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', '2018-12-20 01:09:37', '2019-01-20 01:09:37', '2019-02-04 01:09:37', 40019, 'amsterdam-orange.png', null, null, 'Kwideo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (881, 2, 4960, 'et tempus semper est quam', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.', '2018-12-12 02:55:26', '2019-01-12 02:55:26', '2019-01-27 02:55:26', 20017, 'berlin-blue.png', null, null, 'Bubblemix', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (882, 1, 3234, 'consectetuer adipiscing elit proin interdum mauris', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', '2018-04-02 07:37:20', '2018-05-02 07:37:20', '2018-05-17 07:37:20', 40038, 'chicago-red.png', null, null, 'Eadel', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (883, 4, 7621, 'a feugiat et eros vestibulum', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', '2018-07-01 10:24:53', '2018-08-01 10:24:53', '2018-08-16 10:24:53', 5039, 'berlin-blue.png', null, null, 'Flashset', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (884, 2, 1016, 'mattis pulvinar nulla pede ullamcorper augue', 'Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', '2018-12-04 07:07:06', '2019-01-04 07:07:06', '2019-01-19 07:07:06', 20042, 'berlin-blue.png', null, null, 'Voolia', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (885, 1, 2618, 'nisl ut volutpat', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', '2018-04-22 22:42:09', '2018-05-22 22:42:09', '2018-06-06 22:42:09', 40027, 'aberdeen-green.png', null, null, 'Skipfire', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (886, 1, 7476, 'sem mauris laoreet ut', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', '2018-08-12 23:04:18', '2018-09-12 23:04:18', '2018-09-27 23:04:18', 40045, 'berlin-blue.png', null, null, 'Layo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (887, 1, 7197, 'duis at velit eu est', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', '2018-08-10 19:41:26', '2018-09-10 19:41:26', '2018-09-25 19:41:26', 40032, 'chicago-red.png', null, null, 'Quire', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (888, 2, 2864, 'amet consectetuer adipiscing elit proin interdum', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', '2018-07-18 06:14:36', '2018-08-18 06:14:36', '2018-09-02 06:14:36', 20013, 'berlin-blue.png', null, null, 'Riffpath', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (889, 1, 3670, 'orci vehicula condimentum curabitur', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.

Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', '2018-05-27 07:23:10', '2018-06-27 07:23:10', '2018-07-12 07:23:10', 40044, 'berlin-blue.png', null, null, 'Photospace', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (890, 2, 1417, 'ipsum integer a nibh', 'In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', '2018-10-28 18:11:59', '2018-11-28 18:11:59', '2018-12-13 18:11:59', 20024, 'chicago-red.png', null, null, 'Avavee', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (891, 1, 6838, 'nulla quisque arcu libero', 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.

Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', '2018-07-13 18:47:26', '2018-08-13 18:47:26', '2018-08-28 18:47:26', 40016, 'chicago-red.png', null, null, 'Photofeed', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (892, 2, 5068, 'purus aliquet at feugiat non pretium', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', '2018-09-29 04:12:14', '2018-10-29 04:12:14', '2018-11-13 04:12:14', 20004, 'chicago-red.png', null, null, 'Zooveo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (893, 2, 6094, 'odio odio elementum eu', 'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', '2018-12-12 04:01:40', '2019-01-12 04:01:40', '2019-01-27 04:01:40', 20039, 'amsterdam-orange.png', null, null, 'Pixonyx', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (894, 3, 1450, 'est quam pharetra', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', '2018-08-18 11:38:53', '2018-09-18 11:38:53', '2018-10-03 11:38:53', 10032, 'aberdeen-green.png', null, null, 'Eire', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (895, 3, 547, 'ipsum praesent blandit lacinia', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', '2018-01-27 04:36:59', '2018-02-27 04:36:59', '2018-03-14 04:36:59', 10025, 'chicago-red.png', null, null, 'Wordware', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (896, 3, 1587, 'ligula suspendisse ornare consequat', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', '2018-04-16 17:49:42', '2018-05-16 17:49:42', '2018-05-31 17:49:42', 10004, 'aberdeen-green.png', null, null, 'Rhynoodle', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (897, 4, 2482, 'quis lectus suspendisse potenti', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.', '2018-10-11 23:41:24', '2018-11-11 23:41:24', '2018-11-26 23:41:24', 5005, 'berlin-blue.png', null, null, 'Brainverse', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (898, 1, 3271, 'scelerisque mauris sit amet eros', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', '2018-02-01 04:25:37', '2018-03-01 04:25:37', '2018-03-16 04:25:37', 40031, 'aberdeen-green.png', null, null, 'Demizz', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (899, 1, 3094, 'nulla facilisi cras non velit', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', '2018-11-09 07:42:50', '2018-12-09 07:42:50', '2018-12-24 07:42:50', 40027, 'berlin-blue.png', null, null, 'Leenti', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (900, 1, 3098, 'at ipsum ac', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', '2018-04-02 10:17:34', '2018-05-02 10:17:34', '2018-05-17 10:17:34', 40011, 'berlin-blue.png', null, null, 'Photobean', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (901, 1, 5532, 'in hac habitasse platea dictumst', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', '2018-06-06 23:58:48', '2018-07-06 23:58:48', '2018-07-21 23:58:48', 40038, 'amsterdam-orange.png', null, null, 'Flashspan', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (902, 3, 4880, 'eget semper rutrum nulla nunc', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', '2018-04-26 12:22:10', '2018-05-26 12:22:10', '2018-06-10 12:22:10', 10005, 'berlin-blue.png', null, null, 'Mycat', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (903, 3, 5817, 'congue elementum in hac', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', '2018-03-16 13:13:06', '2018-04-16 13:13:06', '2018-05-01 13:13:06', 10042, 'berlin-blue.png', null, null, 'Dabshots', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (904, 4, 8027, 'vivamus vestibulum sagittis', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', '2018-02-09 01:58:45', '2018-03-09 01:58:45', '2018-03-24 01:58:45', 5011, 'berlin-blue.png', null, null, 'Livetube', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (905, 3, 7729, 'posuere cubilia curae', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', '2018-09-06 22:30:05', '2018-10-06 22:30:05', '2018-10-21 22:30:05', 10042, 'berlin-blue.png', null, null, 'Dynava', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (906, 3, 2508, 'mollis molestie lorem quisque', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.', '2018-10-27 17:39:35', '2018-11-27 17:39:35', '2018-12-12 17:39:35', 10025, 'aberdeen-green.png', null, null, 'Feedspan', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (907, 4, 3933, 'suscipit a feugiat et', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', '2018-11-22 16:57:06', '2018-12-22 16:57:06', '2019-01-06 16:57:06', 5033, 'chicago-red.png', null, null, 'Voonder', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (908, 3, 5740, 'sit amet justo morbi ut', 'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', '2018-12-13 10:05:29', '2019-01-13 10:05:29', '2019-01-28 10:05:29', 10049, 'aberdeen-green.png', null, null, 'Abatz', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (909, 1, 5744, 'congue eget semper rutrum nulla', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', '2018-01-31 18:34:20', '2018-02-28 18:34:20', '2018-03-15 18:34:20', 40045, 'berlin-blue.png', null, null, 'Gigaclub', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (910, 2, 2509, 'congue etiam justo etiam pretium', 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', '2018-08-22 01:12:27', '2018-09-22 01:12:27', '2018-10-07 01:12:27', 20049, 'berlin-blue.png', null, null, 'Skinte', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (911, 2, 523, 'mollis molestie lorem quisque ut erat', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.', '2018-10-16 20:49:19', '2018-11-16 20:49:19', '2018-12-01 20:49:19', 20001, 'aberdeen-green.png', null, null, 'Vinte', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (912, 1, 4242, 'quam pharetra magna', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', '2018-10-28 04:14:40', '2018-11-28 04:14:40', '2018-12-13 04:14:40', 40045, 'aberdeen-green.png', null, null, 'Gabvine', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (913, 2, 3977, 'et ultrices posuere cubilia curae', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.', '2018-01-05 15:28:10', '2018-02-05 15:28:10', '2018-02-20 15:28:10', 20030, 'aberdeen-green.png', null, null, 'Oyoloo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (914, 1, 7392, 'massa quis augue', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.', '2018-01-06 11:21:50', '2018-02-06 11:21:50', '2018-02-21 11:21:50', 40045, 'aberdeen-green.png', null, null, 'Youbridge', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (915, 2, 49, 'praesent blandit nam nulla integer pede', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', '2018-05-02 13:16:28', '2018-06-02 13:16:28', '2018-06-17 13:16:28', 20048, 'berlin-blue.png', null, null, 'Vitz', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (916, 4, 6052, 'sollicitudin mi sit amet', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', '2018-07-16 14:12:53', '2018-08-16 14:12:53', '2018-08-31 14:12:53', 5048, 'chicago-red.png', null, null, 'Thoughtbeat', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (917, 2, 98, 'suscipit nulla elit ac nulla sed', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', '2018-03-26 18:32:39', '2018-04-26 18:32:39', '2018-05-11 18:32:39', 20006, 'amsterdam-orange.png', null, null, 'Flashdog', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (918, 2, 7051, 'nulla mollis molestie lorem quisque ut', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', '2018-01-02 04:36:51', '2018-02-02 04:36:51', '2018-02-17 04:36:51', 20023, 'berlin-blue.png', null, null, 'Zooxo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (919, 2, 362, 'consequat dui nec nisi volutpat', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', '2018-04-12 09:02:05', '2018-05-12 09:02:05', '2018-05-27 09:02:05', 20021, 'amsterdam-orange.png', null, null, 'Voonix', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (920, 4, 363, 'congue vivamus metus arcu adipiscing', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', '2018-09-08 05:03:53', '2018-10-08 05:03:53', '2018-10-23 05:03:53', 5009, 'berlin-blue.png', null, null, 'Agivu', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (921, 2, 5875, 'id nulla ultrices aliquet maecenas', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', '2018-02-07 21:44:29', '2018-03-07 21:44:29', '2018-03-22 21:44:29', 20027, 'berlin-blue.png', null, null, 'Avaveo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (922, 1, 5607, 'aliquam quis turpis eget elit', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.', '2018-07-27 08:30:45', '2018-08-27 08:30:45', '2018-09-11 08:30:45', 40014, 'berlin-blue.png', null, null, 'BlogXS', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (923, 4, 7951, 'dictumst morbi vestibulum velit id pretium', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.', '2018-12-24 12:29:04', '2019-01-24 12:29:04', '2019-02-08 12:29:04', 5032, 'chicago-red.png', null, null, 'Kwinu', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (924, 2, 4460, 'neque vestibulum eget vulputate', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', '2018-01-17 00:11:21', '2018-02-17 00:11:21', '2018-03-04 00:11:21', 20036, 'berlin-blue.png', null, null, 'Muxo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (925, 2, 2260, 'in hac habitasse platea dictumst maecenas', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.', '2018-08-08 00:15:26', '2018-09-08 00:15:26', '2018-09-23 00:15:26', 20044, 'chicago-red.png', null, null, 'Twitternation', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (926, 1, 383, 'habitasse platea dictumst etiam faucibus', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', '2018-01-25 02:45:55', '2018-02-25 02:45:55', '2018-03-12 02:45:55', 40039, 'chicago-red.png', null, null, 'Pixonyx', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (927, 2, 5573, 'ut erat id mauris vulputate', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', '2018-10-26 11:13:58', '2018-11-26 11:13:58', '2018-12-11 11:13:58', 20027, 'chicago-red.png', null, null, 'Feedmix', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (928, 3, 5161, 'duis bibendum felis', 'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', '2018-11-10 18:03:16', '2018-12-10 18:03:16', '2018-12-25 18:03:16', 10034, 'aberdeen-green.png', null, null, 'Thoughtmix', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (929, 1, 4456, 'in felis donec', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', '2018-05-04 13:51:30', '2018-06-04 13:51:30', '2018-06-19 13:51:30', 40007, 'amsterdam-orange.png', null, null, 'Thoughtbeat', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (930, 2, 6462, 'pulvinar nulla pede ullamcorper', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', '2018-05-16 22:26:03', '2018-06-16 22:26:03', '2018-07-01 22:26:03', 20014, 'berlin-blue.png', null, null, 'Livefish', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (931, 2, 7122, 'tortor risus dapibus augue vel', 'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', '2018-06-03 03:33:32', '2018-07-03 03:33:32', '2018-07-18 03:33:32', 20013, 'aberdeen-green.png', null, null, 'Gabtune', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (932, 2, 5514, 'velit id pretium', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', '2018-10-28 03:00:55', '2018-11-28 03:00:55', '2018-12-13 03:00:55', 20006, 'aberdeen-green.png', null, null, 'Edgewire', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (933, 1, 3275, 'massa donec dapibus', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.', '2018-05-08 20:52:12', '2018-06-08 20:52:12', '2018-06-23 20:52:12', 40042, 'aberdeen-green.png', null, null, 'Divape', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (934, 3, 6587, 'sed magna at nunc', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.', '2018-04-16 02:42:24', '2018-05-16 02:42:24', '2018-05-31 02:42:24', 10022, 'amsterdam-orange.png', null, null, 'Twimm', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (935, 4, 4873, 'at ipsum ac', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', '2018-03-04 01:43:29', '2018-04-04 01:43:29', '2018-04-19 01:43:29', 5014, 'chicago-red.png', null, null, 'Agimba', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (936, 2, 1030, 'sem praesent id massa id nisl', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', '2018-10-20 17:22:38', '2018-11-20 17:22:38', '2018-12-05 17:22:38', 20019, 'chicago-red.png', null, null, 'Dabvine', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (937, 2, 6644, 'pede justo lacinia', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', '2018-04-29 13:32:15', '2018-05-29 13:32:15', '2018-06-13 13:32:15', 20022, 'berlin-blue.png', null, null, 'Meedoo', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (938, 1, 2532, 'pede lobortis ligula sit amet eleifend', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', '2018-11-25 14:51:06', '2018-12-25 14:51:06', '2019-01-09 14:51:06', 40040, 'berlin-blue.png', null, null, 'Skinix', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (939, 1, 1385, 'iaculis congue vivamus metus', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', '2018-05-14 16:36:15', '2018-06-14 16:36:15', '2018-06-29 16:36:15', 40006, 'chicago-red.png', null, null, 'Abata', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (940, 3, 6770, 'nec molestie sed', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', '2018-06-21 23:10:37', '2018-07-21 23:10:37', '2018-08-05 23:10:37', 10033, 'amsterdam-orange.png', null, null, 'Zoomcast', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (941, 2, 7516, 'habitasse platea dictumst', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', '2018-06-06 10:22:55', '2018-07-06 10:22:55', '2018-07-21 10:22:55', 20019, 'amsterdam-orange.png', null, null, 'Einti', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (942, 4, 5644, 'leo maecenas pulvinar lobortis', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', '2018-12-09 13:25:05', '2019-01-09 13:25:05', '2019-01-24 13:25:05', 5041, 'berlin-blue.png', null, null, 'Brightdog', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (943, 3, 2378, 'donec dapibus duis at', 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', '2018-02-06 19:10:45', '2018-03-06 19:10:45', '2018-03-21 19:10:45', 10047, 'berlin-blue.png', null, null, 'Trunyx', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (944, 1, 4251, 'cras in purus eu', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', '2018-04-27 16:28:45', '2018-05-27 16:28:45', '2018-06-11 16:28:45', 40010, 'amsterdam-orange.png', null, null, 'Realpoint', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (945, 2, 7872, 'volutpat quam pede lobortis ligula', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', '2018-02-03 06:44:08', '2018-03-03 06:44:08', '2018-03-18 06:44:08', 20044, 'amsterdam-orange.png', null, null, 'Zoozzy', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (946, 4, 5078, 'dapibus dolor vel est donec', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.', '2018-02-10 20:43:06', '2018-03-10 20:43:06', '2018-03-25 20:43:06', 5045, 'aberdeen-green.png', null, null, 'Jaxnation', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (947, 1, 5837, 'et commodo vulputate justo', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.', '2018-10-18 10:00:10', '2018-11-18 10:00:10', '2018-12-03 10:00:10', 40045, 'amsterdam-orange.png', null, null, 'Gabvine', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (948, 1, 3413, 'semper est quam pharetra', 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.

Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', '2018-08-15 02:33:28', '2018-09-15 02:33:28', '2018-09-30 02:33:28', 40033, 'amsterdam-orange.png', null, null, 'Youtags', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (949, 4, 4515, 'libero non mattis pulvinar nulla', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', '2018-12-26 19:33:13', '2019-01-26 19:33:13', '2019-02-10 19:33:13', 5033, 'amsterdam-orange.png', null, null, 'Brainbox', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (950, 4, 8079, 'ut massa quis augue luctus tincidunt', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', '2018-03-03 18:30:56', '2018-04-03 18:30:56', '2018-04-18 18:30:56', 5032, 'aberdeen-green.png', null, null, 'Avamm', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (951, 2, 5436, 'maecenas rhoncus aliquam lacus morbi quis', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.

Fusce consequat. Nulla nisl. Nunc nisl.', '2018-03-09 15:31:09', '2018-04-09 15:31:09', '2018-04-24 15:31:09', 20013, 'berlin-blue.png', null, null, 'Skidoo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (952, 3, 2384, 'sapien ut nunc vestibulum ante', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', '2018-04-21 01:45:59', '2018-05-21 01:45:59', '2018-06-05 01:45:59', 10027, 'berlin-blue.png', null, null, 'Edgeify', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (953, 1, 5029, 'et ultrices posuere', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', '2018-06-04 18:03:22', '2018-07-04 18:03:22', '2018-07-19 18:03:22', 40045, 'amsterdam-orange.png', null, null, 'Flipopia', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (954, 2, 2383, 'mauris lacinia sapien quis libero nullam', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', '2018-03-14 05:17:35', '2018-04-14 05:17:35', '2018-04-29 05:17:35', 20012, 'berlin-blue.png', null, null, 'Linktype', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (955, 3, 7277, 'aenean lectus pellentesque eget nunc donec', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', '2018-04-04 12:07:50', '2018-05-04 12:07:50', '2018-05-19 12:07:50', 10002, 'berlin-blue.png', null, null, 'Buzzbean', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (956, 4, 3537, 'dapibus duis at', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', '2018-02-01 05:41:30', '2018-03-01 05:41:30', '2018-03-16 05:41:30', 5028, 'chicago-red.png', null, null, 'Divanoodle', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (957, 4, 2797, 'interdum mauris ullamcorper', 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.

Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', '2018-07-23 07:48:30', '2018-08-23 07:48:30', '2018-09-07 07:48:30', 5041, 'aberdeen-green.png', null, null, 'Latz', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (958, 2, 6089, 'donec vitae nisi nam', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', '2018-10-20 17:28:00', '2018-11-20 17:28:00', '2018-12-05 17:28:00', 20037, 'amsterdam-orange.png', null, null, 'Eazzy', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (959, 1, 1263, 'duis consequat dui', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', '2018-04-06 22:43:32', '2018-05-06 22:43:32', '2018-05-21 22:43:32', 40017, 'aberdeen-green.png', null, null, 'BlogXS', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (960, 3, 5516, 'ante ipsum primis in faucibus', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', '2018-11-26 21:40:07', '2018-12-26 21:40:07', '2019-01-10 21:40:07', 10009, 'aberdeen-green.png', null, null, 'Photojam', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (961, 4, 2665, 'pellentesque volutpat dui', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.', '2018-04-05 20:58:52', '2018-05-05 20:58:52', '2018-05-20 20:58:52', 5017, 'berlin-blue.png', null, null, 'Tekfly', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (962, 2, 5983, 'leo maecenas pulvinar', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', '2018-11-12 12:44:24', '2018-12-12 12:44:24', '2018-12-27 12:44:24', 20036, 'chicago-red.png', null, null, 'Vinder', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (963, 1, 1557, 'cubilia curae donec pharetra magna', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', '2018-01-05 22:02:35', '2018-02-05 22:02:35', '2018-02-20 22:02:35', 40038, 'chicago-red.png', null, null, 'Jayo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (964, 1, 2397, 'turpis eget elit sodales', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.', '2018-04-24 21:20:13', '2018-05-24 21:20:13', '2018-06-08 21:20:13', 40044, 'berlin-blue.png', null, null, 'Vipe', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (965, 2, 7401, 'aenean lectus pellentesque eget nunc', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', '2018-07-22 06:59:46', '2018-08-22 06:59:46', '2018-09-06 06:59:46', 20010, 'chicago-red.png', null, null, 'Gabcube', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (966, 1, 6782, 'nulla eget eros elementum pellentesque quisque', 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', '2018-06-19 11:57:29', '2018-07-19 11:57:29', '2018-08-03 11:57:29', 40044, 'chicago-red.png', null, null, 'Fivebridge', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (967, 2, 103, 'sit amet cursus', 'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', '2018-09-27 07:16:22', '2018-10-27 07:16:22', '2018-11-11 07:16:22', 20000, 'chicago-red.png', null, null, 'Quinu', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (968, 2, 2960, 'suspendisse ornare consequat lectus in', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', '2018-03-20 02:38:55', '2018-04-20 02:38:55', '2018-05-05 02:38:55', 20046, 'chicago-red.png', null, null, 'Avamba', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (969, 1, 6448, 'augue vel accumsan tellus nisi', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', '2018-07-16 12:07:46', '2018-08-16 12:07:46', '2018-08-31 12:07:46', 40046, 'amsterdam-orange.png', null, null, 'Npath', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (970, 3, 7059, 'ornare imperdiet sapien urna pretium', 'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.

Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', '2018-02-03 20:52:24', '2018-03-03 20:52:24', '2018-03-18 20:52:24', 10001, 'berlin-blue.png', null, null, 'Linklinks', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (971, 4, 3673, 'nunc nisl duis', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', '2018-12-25 01:04:02', '2019-01-25 01:04:02', '2019-02-09 01:04:02', 5000, 'amsterdam-orange.png', null, null, 'Oyope', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (972, 4, 266, 'metus vitae ipsum aliquam non', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.

Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.', '2018-11-15 00:28:29', '2018-12-15 00:28:29', '2018-12-30 00:28:29', 5032, 'amsterdam-orange.png', null, null, 'Zooxo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (973, 3, 922, 'vestibulum ante ipsum primis in', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', '2018-12-18 06:59:51', '2019-01-18 06:59:51', '2019-02-02 06:59:51', 10050, 'berlin-blue.png', null, null, 'Skaboo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (974, 1, 6314, 'sapien in sapien iaculis congue', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', '2018-05-12 21:50:12', '2018-06-12 21:50:12', '2018-06-27 21:50:12', 40024, 'amsterdam-orange.png', null, null, 'Talane', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (975, 3, 3714, 'sit amet nulla quisque arcu libero', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', '2018-12-11 18:37:22', '2019-01-11 18:37:22', '2019-01-26 18:37:22', 10022, 'amsterdam-orange.png', null, null, 'Avamm', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (976, 3, 4203, 'ut blandit non interdum in', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', '2018-05-02 14:27:56', '2018-06-02 14:27:56', '2018-06-17 14:27:56', 10030, 'berlin-blue.png', null, null, 'Tagfeed', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (977, 1, 6605, 'iaculis justo in hac habitasse', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.

Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.

Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.', '2018-08-13 21:39:25', '2018-09-13 21:39:25', '2018-09-28 21:39:25', 40043, 'berlin-blue.png', null, null, 'Eadel', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (978, 2, 3152, 'dictumst etiam faucibus cursus urna', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', '2018-02-18 01:51:38', '2018-03-18 01:51:38', '2018-04-02 01:51:38', 20026, 'aberdeen-green.png', null, null, 'Topiclounge', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (979, 4, 1902, 'maecenas ut massa quis', 'Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', '2018-07-26 20:27:15', '2018-08-26 20:27:15', '2018-09-10 20:27:15', 5047, 'aberdeen-green.png', null, null, 'Ainyx', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (980, 4, 5690, 'turpis enim blandit mi in porttitor', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.', '2018-07-14 15:15:44', '2018-08-14 15:15:44', '2018-08-29 15:15:44', 5047, 'berlin-blue.png', null, null, 'Meetz', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (981, 2, 4693, 'hac habitasse platea dictumst aliquam', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', '2018-03-13 19:01:33', '2018-04-13 19:01:33', '2018-04-28 19:01:33', 20040, 'amsterdam-orange.png', null, null, 'Youopia', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (982, 1, 1039, 'sagittis dui vel nisl duis', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', '2018-03-13 12:59:28', '2018-04-13 12:59:28', '2018-04-28 12:59:28', 40020, 'berlin-blue.png', null, null, 'Fanoodle', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (983, 1, 3728, 'donec posuere metus vitae ipsum', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', '2018-11-01 18:08:43', '2018-12-01 18:08:43', '2018-12-16 18:08:43', 40004, 'amsterdam-orange.png', null, null, 'Youbridge', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (984, 3, 976, 'pede libero quis', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', '2018-05-18 15:06:39', '2018-06-18 15:06:39', '2018-07-03 15:06:39', 10003, 'chicago-red.png', null, null, 'Edgepulse', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (985, 3, 1091, 'pellentesque quisque porta volutpat', 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.', '2018-05-20 10:59:35', '2018-06-20 10:59:35', '2018-07-05 10:59:35', 10044, 'berlin-blue.png', null, null, 'Skiba', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (986, 4, 606, 'nullam molestie nibh in', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', '2018-02-12 18:13:27', '2018-03-12 18:13:27', '2018-03-27 18:13:27', 5028, 'amsterdam-orange.png', null, null, 'Twiyo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (987, 2, 2053, 'curae nulla dapibus dolor vel est', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', '2018-12-21 12:37:21', '2019-01-21 12:37:21', '2019-02-05 12:37:21', 20009, 'berlin-blue.png', null, null, 'Thoughtbeat', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (988, 2, 4428, 'vestibulum rutrum rutrum neque aenean auctor', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', '2018-08-05 10:49:23', '2018-09-05 10:49:23', '2018-09-20 10:49:23', 20048, 'aberdeen-green.png', null, null, 'Yakitri', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (989, 1, 1118, 'dui maecenas tristique', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.', '2018-07-13 16:31:47', '2018-08-13 16:31:47', '2018-08-28 16:31:47', 40024, 'amsterdam-orange.png', null, null, 'Flipopia', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (990, 1, 4132, 'cursus id turpis', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', '2018-05-19 02:49:22', '2018-06-19 02:49:22', '2018-07-04 02:49:22', 40001, 'berlin-blue.png', null, null, 'Zoomzone', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (991, 2, 2974, 'nibh in lectus pellentesque at', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', '2018-06-01 20:03:33', '2018-07-01 20:03:33', '2018-07-16 20:03:33', 20049, 'amsterdam-orange.png', null, null, 'Muxo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (992, 4, 1409, 'turpis donec posuere metus vitae', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', '2018-11-01 09:40:48', '2018-12-01 09:40:48', '2018-12-16 09:40:48', 5037, 'berlin-blue.png', null, null, 'Devbug', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (993, 1, 5551, 'amet consectetuer adipiscing elit', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', '2018-05-10 23:38:25', '2018-06-10 23:38:25', '2018-06-25 23:38:25', 40020, 'chicago-red.png', null, null, 'Nlounge', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (994, 3, 5411, 'nisl duis bibendum', 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', '2018-06-22 00:42:48', '2018-07-22 00:42:48', '2018-08-06 00:42:48', 10001, 'amsterdam-orange.png', null, null, 'Blogtag', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (995, 2, 6531, 'non quam nec dui luctus rutrum', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.', '2018-10-07 00:59:25', '2018-11-07 00:59:25', '2018-11-22 00:59:25', 20041, 'chicago-red.png', null, null, 'Browsebug', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (996, 3, 6369, 'odio elementum eu interdum', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', '2018-06-25 16:08:04', '2018-07-25 16:08:04', '2018-08-09 16:08:04', 10016, 'amsterdam-orange.png', null, null, 'Flashpoint', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (997, 2, 5684, 'arcu libero rutrum ac lobortis vel', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', '2018-12-25 00:15:28', '2019-01-25 00:15:28', '2019-02-09 00:15:28', 20031, 'amsterdam-orange.png', null, null, 'Tavu', true, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (998, 2, 1870, 'sed nisl nunc rhoncus dui', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.', '2018-11-08 06:59:33', '2018-12-08 06:59:33', '2018-12-23 06:59:33', 20041, 'berlin-blue.png', null, null, 'Dabfeed', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (999, 2, 3165, 'ut nulla sed', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', '2018-08-17 05:52:28', '2018-09-17 05:52:28', '2018-10-02 05:52:28', 20020, 'chicago-red.png', null, null, 'Jayo', false, null);
insert into races (race_id, category_id, city, race_name, description, inscription_start_date, inscription_finish_date, date, distance, poster, google_map_url, google_map_iframe, organizer, is_active, rules) values (1000, 3, 2559, 'tellus nisi eu orci', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', '2018-04-12 22:31:07', '2018-05-12 22:31:07', '2018-05-27 22:31:07', 10002, 'aberdeen-green.png', null, null, 'Talane', false, null);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;